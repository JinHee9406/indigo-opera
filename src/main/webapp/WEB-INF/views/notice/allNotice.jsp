<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
   <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <link rel="stylesheet" href="/opera/resources/css/noticeStyle.css">
<title>Opera GroupWare</title>
<style type="text/css">
	#writeBtn:hover{
		cursor: pointer;
	}
	td:hover{
		cursor: pointer;
	}
	.pagingBtn:hover{
		cursor: pointer;
	}
	.pagingSide:hover{
		cursor: pointer;
	}
</style>
</head>
<body>
   <jsp:include page="../common/operaNav.jsp"/>   
   <jsp:include page="../common/noticeAside.jsp">
   	<jsp:param value="${Typelist}" name="Typelist"/>
   </jsp:include>   
   <section >
      <div id="mainDiv" style="width:950px;" >
         <div id="titleDiv">
            <a id="titleName" style="color: #57585E;">공지사항</a>
         </div>
         <hr style="margin-top: 1%; margin-bottom: 2%;">
        <!--  <div style="border: 2px solid #C4C4C4; margin: 10px; width: 450px; height: 35px; background: #E5E5E5;">
            <a style="float:left; padding: 10px; font-weight: 500;">지식검색</a>
            <div style="float:left; margin-right: 10px; border: 2px solid #C4C4C4; width:0.5px; height: 33px;"></div>
            <div style="float:left; padding: 8px;">
               <select style="background: #27334C; color: white; font-size: 15px;">
                  <option>제목</option>
                  <option>내용</option>
                  <option>작성자</option>
               </select>
            </div> -->
            <!-- <div style="float:left; padding: 8px; font-size: 15px;">
               <input type="text" placeholder="검색어를 입력해주세요">
            </div>
            <div style="float:left; padding: 5px; font-size: 14px; margin-left: 10px; margin-top: 6px; width: 55px; height: 15px; background: #27334C; color: white; ">
               검색하기
            </div> -->
            <div style="width: 1150px; clear: both; text-align: center; ">
               <table id="listTable">
                  <tr class="head" style="background-color:#27334C; color:white;">
                     <th width="60px;" style="border: 2px; border-style:solid;  border-color: lightgray;">No.</th>
                     <th width="660px;" style="border: 2px; border-style:solid;  border-color: lightgray;">글제목</th>
                     <th width="120px;" style="border: 2px; border-style:solid;  border-color: lightgray;">작성자</th>
                     <th width="160px;" style="border: 2px; border-style:solid;  border-color: lightgray;">작성일</th>
                     <th width="60px;" style="border: 2px; border-style:solid;  border-color: lightgray;">조회수</th>
                     <th width="60px;" style="border: 2px; border-style:solid;  border-color: lightgray;">파일</th>
                  </tr>
                  <c:forEach var="note" items="${notice}" varStatus="i">
                  <tr class="row" onclick="location.href='allDetail.not?boardNo=${note.boardNo}'">
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">${(pi.listCount - (pi.limit * (pi.currentPage - 1))) - i.index}</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;  text-align: left; padding-left: 10px;">${note.boardTitle }</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">${note.empName }</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">${note.boardDate }</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">${note.boardCnt }</td>
                     <c:set var="asd" value="${fn:split(note.fileCode,',')}"/>
                     <c:set var="counts" value="0"/>
                     <c:forEach var="count" items="${asd}">
                     	<c:set var="counts" value="${counts + 1}"/>
                     </c:forEach>
                     <c:if test="${note.fileCode != null}">
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">${counts} </td>
                     </c:if>
                     <c:if test="${note.fileCode == null}">
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">0</td>
                     </c:if>
                  </tr>
                  </c:forEach>
               </table>
               
               <c:if test="${loginUser.empJobCode eq 'JOB1' || loginUser.empJobCode eq 'JOB2' || loginUser.empJobCode eq 'JOB3' || loginUser.empJobCode eq 'JOB4'   }">
                  <div id="writeBtn" style="float:right; padding: 5px; font-size: 14px; margin-right: 5px; 
                  margin-top: 6px; width: 55px; height: 15px; background: #27334C; color: white;" onclick="location.href='allWrite.not'">
             글 작성
            </div>
            </c:if> 
               <br><br>
                 <div id="pagingArea" align="center" style="">
				<c:if test="${pi.currentPage <= 1 }">
					[이전] &nbsp;
				
				</c:if>
				<c:if test="${pi.currentPage > 1 }">
					<c:url var="blistBack" value="allNotice.not?pageNo=${pi.currentPage - 1 }">
						<c:param name="pageNum" value="${pi.currentPage -1 }"/>
					</c:url>
					<a href="${blistBack }">[이전]</a> &nbsp;
				</c:if>
				<c:forEach var="p" begin="${pi.startPage }" end="${pi.endPage }">
					<c:if test="${p eq pi.currentPage }">
						<font color="red" size="4"><b>[${p}]</b></font>
					</c:if>
					<c:if test="${p ne pi.currentPage }">
						<c:url var="blistCheck" value="allNotice.not?pageNo=${p}">
							<c:param name="pageNum" value="${p }"/>
						</c:url>				
						<a href="${blistCheck }">${p}</a>
					</c:if>
				</c:forEach>
				<c:if test="${pi.currentPage >= pi.maxPage }">
					&nbsp;[다음]
				</c:if>
				<c:if test="${pi.currentPage < pi.maxPage }">
					<c:url var="blistEnd" value="allNotice.not?pageNo=${pi.currentPage + 1 }">
						<c:param name="pageNum" value="${pi.currentPage + 1 }"/>
					</c:url>
					&nbsp; <a href="${blistEnd}">[다음]</a>
				</c:if>
			</div>
      </div>
   </section>
   <script>
      /* function openAside(){

      }
      $("#listTable tr").click(function(){
         location.href="commonDetail.not";
      }) */
   </script>
</body>
</html>