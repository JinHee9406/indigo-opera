<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
   <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <script type="text/javascript" src="/opera/resources/se2/js/service/HuskyEZCreator.js" charset="utf-8"></script>
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
   <link rel="stylesheet" href="/opera/resources/css/noticeStyle.css">
<title>Opera GroupWare</title>
<style type="text/css">
   #writeBtn:hover{
      cursor: pointer;
   }
   td:hover{
      cursor: pointer;
   }
   .pagingBtn:hover{
      cursor: pointer;
   }
   .pagingSide:hover{
      cursor: pointer;
   }
   #searchBtn:hover{
      cursor:pointer;
   }
</style>
</head>
<body>
   <jsp:include page="../common/operaNav.jsp"/>   
   <jsp:include page="../common/noticeAside.jsp"/>   
   <section >
      <div id="mainDiv" style="width:950px;" >
         <div id="titleDiv">
            <a id="titleName" style="color: #57585E;">부서 게시판</a>
         </div>
         <hr style="margin-top: 1%; margin-bottom: 2%;">
         <!-- <div style="border: 0.5px solid #C4C4C4; margin: 10px; width: 950px; height: 35px; background: white;">
            <a style="float:left; padding: 10px; font-weight: 500; margin-left: 5px;">출근통계</a>
            <div style="float:left; margin-left: 4px; margin-right: 5px; border: 2px solid #C4C4C4; width:0.05px; height: 32px; "></div>
            <div style=" float: left; background: #C4C4C4; width:60px; padding:3px; margin-top: 4px; border-radius: 3px; text-align: center; border: 0.5px solid gray; margin-left: 5px;">당일</div>
            <div style=" float: left; background: #C4C4C4; width:60px; padding:3px; margin-top: 4px; border-radius: 3px; text-align: center; border: 0.5px solid gray;margin-left: 5px;">3일</div>
            <div style=" float: left; background: #C4C4C4; width:60px; padding:3px; margin-top: 4px; border-radius: 3px; text-align: center; border: 0.5px solid gray;margin-left: 5px;">7일</div>
            <div style=" float: left; background: #C4C4C4; width:60px; padding:3px; margin-top: 4px; border-radius: 3px; text-align: center; border: 0.5px solid gray;margin-left: 5px;">30일</div>
            <div style="float:left; padding: 8px;">
               <select style="background: white; color: gray; font-size: 15px; width: 120px;">
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
               </select>
               ~
            </div>
            
            <div style="float:left; padding: 8px;">
               <select style="background: white; color: gray; font-size: 15px; width: 120px;">
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
               </select>
            </div>
            <div style="float:left; padding: 5px; text-align:center; font-size: 14px; margin-left: 10px; margin-top: 6px; width: 55px; height: 15px; background: #27334C; color: white; "
            id="searchBtn">
               검색
            </div> -->
            <!-- <br>
            <br>
            <br> -->
            <form method="post" action="insertDept.not?empDept=${loginUser.empDept}" id="insertform" enctype="multipart/form-data">
            <div style="width: 1150px; clear: both; text-align: center; ">
               <table id="listTable" style="margin-left:0;">
                  <tr class="head" style="background-color:#27334C; color:white;">
                     <th width="143px" style="border: 2px; border-style:solid;  border-color: lightgray; ">작성자</th>
                     <th  width="429px" style="border: 2px; border-style:solid;  border-color: lightgray; text-align:left; margin-left: 10px; background: white; color:#57585E;" colspan="3">${notice.empName }</th>
                     <th width="143px" style="border: 2px; border-style:solid;  border-color: lightgray;">작성일</th>
                     <th width="429px" style="border: 2px; border-style:solid;  border-color: lightgray; text-align:left; margin-left: 10px; background: white; color:#57585E;" colspan="3">${notice.boardDate }</th>
                  </tr>
                  <tr class="head" style="background-color:#27334C; color:white;">
                     <th width="143px" style="border: 2px; border-style:solid;  border-color: lightgray; ">제목</th>
                     <th width="1007px" style="border: 2px; border-style:solid;  border-color: lightgray; text-align:left; margin-left: 10px; background: white; color:#57585E;" colspan="7"><input type="text" name="boardTitle" id="titleArea" size="50"></th>
                  </tr>
                  <tr >
                     <th width="1150px" height="400px" style="border: 2px; border-style:solid;  border-color: lightgray; text-align:left; margin-left: 10px; background: white; color:#57585E;" colspan="8" rowspan="7">
                     <div>
                     <textarea name="ir1" id="ir1" rows="10" cols="100" style="width:1105px; height: 300px;"></textarea>
                     <input type="hidden" id="boardContent" name="boardContent">
                     
                     </div>
                     </th>
                  </tr>
                 
               </table>
              <div style="width:1142px; height:200px; border:1.5px solid lightgray;">
              <div>
                    <div 
                    style="width:900px; height:40px; border:1.5px solid lightgray; 
                    border-left:0; float: left; background-color:#27334C; color:white;">
                    <p style="margin-top: 10px;">파일명</p>
                    </div>
                    <div 
                    style="width:239.5px; height:40px; border:1.5px solid lightgray; 
                    border-left:0; border-right:1px solid lightgray; float: left; 
                    background-color:#27334C; color:white;"><p style="margin-top: 10px;">크기</p>
                    </div>
                    <div style="width:900px; height:160px; border:1.5px solid lightgray; 
                    border-left:0; float: left; background-color:white; color:#57585E;" id="filenamebox">
                    </div>
                    <div 
                    style="width:239.5px; height:160px; border:1.5px solid lightgray; 
                    border-left:0; border-right:1px solid lightgray; float: left; 
                    background-color:white; color:#57585E;" id="filesizebox">
                    </div>
                    
              </div>
              </div>
              <div align="left">
                    <input type="file"  placeholder="파일 선택" id="filebox" style="width:75px;"/><br/>
              </div>
              
              <div id="filediv">
                 
              </div>
              </form>
              <div style="width: 100px; height:30px; border:1.5px solid lightgray; background: #27334C; color:red; float: right; margin-right: 10px; margin-top: 10px;">
                 <p style="margin-top: 6px;" onclick="submit();">글 작성</p>
              </div> 
              <div style="width: 100px; height:30px; border:1.5px solid lightgray; background: #27334C; color:white; float: right; margin-right: 10px; margin-top: 10px;">
                 <p style="margin-top: 6px;">목록으로</p>
              </div>    
               <br><br>    
               <br><br>
                 
         </div>
      </div>
   </section>
   <script type="text/javascript">
           var oEditors = [];
           nhn.husky.EZCreator.createInIFrame({
            oAppRef: oEditors,
            elPlaceHolder: "ir1",
            sSkinURI: "/opera/resources/se2/SmartEditor2Skin.html",
            //se2폴더 경로(프로젝트안에 넣어놓는게 좋음)
            fCreator: "createSEditor2"
           });
    </script>
    <script>
       function submitContents(elClickedObj) {
          // 에디터의 내용이 textarea에 적용된다.
          oEditors.getById["ir1"].exec("UPDATE_CONTENTS_FIELD", []);
          
          // 에디터의 내용에 대한 값 검증은 이곳에서
          // document.getElementById("ir1").value를 이용해서 처리한다.
          
          try {
               elClickedObj.form.submit();
               
          } catch(e) {
             
            }
       }
   </script>
   
   <script type="text/javascript">
      function submit() {
         var title = $("#titleArea").val();
            oEditors.getById["ir1"].exec("UPDATE_CONTENTS_FIELD", []);
            
            $("#boardContent").val(document.getElementById("ir1").value);
         
         $("#insertform").submit();
      }
   </script>
   <script type="text/javascript">
        var i = 1;
     $(document).on("change", "#filebox", function(){
        
        
        if(i < 3){
           
        
        var file = $(this).clone();
        
        file.attr("hidden","hidden");
        
        file.attr("name","uploadfiles");
        
        $("#filediv").append(file);
        
        console.log($("#filediv"));
        
            
            $("#filenamebox").append('<div style="height: 20px;">' + $("input[type=file]")[0].files[0].name + '</div>');
            $("#filesizebox").append('<div style="height: 20px;">' + ($("input[type=file]")[0].files[0].size / 1000) + 'KB</div>');
            
            i++;
        }else{
           var file = $(this).clone();
           
           file.attr("hidden","hidden");
           
           file.attr("name","uploadfiles");
           
           $("#filediv").append(file);
           
           console.log($("#filediv"));
           
               
               $("#filenamebox").append('<div style="height: 30px;">' + $("input[type=file]")[0].files[0].name + '</div>');
               $("#filesizebox").append('<div style="height: 30px;">' + ($("input[type=file]")[0].files[0].size / 1000) + 'KB</div>');
               
               i++;
           
           $("#filebox").hide();
        }
       // $('input[type=file]')[0].files[0].name;
       // $("#imgUpload")[0].files[0].type;
       // $("#imgUpload")[0].files[0].size;
      });
   
   </script>
   
   
   <!-- <script>
      function openAside(){

      }
      $("#listTable tr").click(function(){
         location.href="knowledgeDetail.me";
      })
   </script> -->
</body>
</html>