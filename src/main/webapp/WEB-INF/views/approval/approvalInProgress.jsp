<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<style>
	body {
		font-family: Gothic A1;
	}
	
	.mainbutton {
		width: 200px;
		height: 49px;
		border-radius: 5px;
		color: white;
		background-color: #27334C;
		font-size: 22px;
	}
	
	.mainfont {
		font-size: 20px;
	}
	
	.mainbarfont {
		display: inline-block;
		width: 45%;
		font-size: 22px;
		font-weight: 600;
		padding-bottom: 5px;
	}
	
	.mainbarfont2 {
		display: inline-block;
		width: 45%;
		font-size: 14px;
		font-weight: 600;
	}
	
	.mainbarfont3 {
		display: inline-block;
		font-size: 18px;
		width: 60%;
	}
	
	.ddtag1 {
	padding-top: 10px;
	padding-left: 25px;
	color: #27334C;
	font-weight: 300;
	font-size: 16px;
	cursor: pointer;
	width: 40px;
}
	button {
		border: 0;
		outline: 0;
	}
	.btn1{
		font-size: 10px;
		width: 50px;
		height: 23px;
		margin-left: 5px;
		color: white;
		background-color: #27334C;
		border-radius: 1px;
	}
	#list tr:hover{
		color: white;
		background-color: #27334C;
	}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/views/common/operaNav.jsp" />
	<jsp:include page="/WEB-INF/views/approval/cashNav.jsp" />
	
	<div style="padding-top: 70px; padding-left: 350px;">
		<img src="/opera/resources/icons/approvalImages/docu.png"><span
			style="font-weight: 600; font-size: 20px; padding-left: 10px;">진행중인
			문서</span><br><br>
			
			<table style="border: 1px solid #C4C4C4; border-collapse: collapse;">
			<tr style="height: 40px; border-bottom:1px solid gray;">
				<td style=" padding-left:20px; padding-right:10px;border-right:1px solid #C4C4C4; font-size:12px; width:60px;">작성일자</td>
				<td style="width:400px; padding-left:20px;"> 
				<button class="btn1"><label class="ddtag1" style="color:white; font-size: 10px; padding:0px">당일</label></button>
				<button class="btn1"><label class="ddtag1" style="color:white; font-size: 10px; padding:0px">1주일</label></button> 
				<button class="btn1"><label class="ddtag1" style="color:white; font-size: 10px; padding:0px">2주일</label></button> 
				<button class="btn1"><label class="ddtag1" style="color:white; font-size: 10px; padding:0px">3주일</label></button> 
				<button class="btn1"><label class="ddtag1" style="color:white; font-size: 10px; padding:0px">1개월</label></button>
				</td>
			</tr>
			
			<tr style="height: 40px;">
				<td style=" padding-left:20px; padding-right:10px;border-right:1px solid #C4C4C4; font-size:12px; width:60px;">문서검색</td>
				<td style="width:400px; padding-left:20px;"> 
				<select id="stype" style=" padding-left:5px;background-color: #27334C; border-radius: 1px; color:white;width:80px; height:23px; font-size: 10px;">
					<option>제목</option>
					<option>기안자</option>
				</select>
				<input type="text" placeholder="검색명" style="padding-left:6px; height:19px;" id="scontent" onchange="print()"> 
				<button class="btn1"><label class="ddtag1" style="color:white; font-size: 10px; padding:0px">검색</label></button>
				</td>
			</tr>
		</table>
		<br>
		<table id="list" style="border: 1px solid #C4C4C4; border-collapse: collapse; font-size: 11px;">
			<thead>
			<tr style="height: 35px; background-color: #27334C; color:white;">
				<th style="width:50px;">번호</th>
				<th style="width:300px;">제목</th>
				<th style="width:100px;">문서분류</th>
				<th style="width:100px;">문서번호</th>
				<th style="width:100px;">상태</th>
				<th style="width:200px;">기안일</th>
				<th style="width:100px;">수신현황</th>
				<th style="width:100px;">기안자</th>
			</tr>
			<thead>
			<tbody>
 			<c:set var="DraftListSize" value="${fn:length(approvalList)}"/>
			<c:forEach var="DraftList" items="${approvalList}" varStatus="status">
				<tr align="center" style="height: 27px;">
					<td>${(pi.listCount - (pi.limit * (pi.currentPage - 1))) - status.index}</td>
					<td>${DraftList.drTitle}</td>
					<td>${DraftList.doName}</td>
					<td>${DraftList.drNo}</td>
					<td>
						<c:if test="${DraftList.drStatus eq '승인완료'}"><label style="color: blue;">승인완료</label></c:if>
						<c:if test="${DraftList.drStatus eq '승인반려'}"><label style="color: red;">승인반려</label></c:if>
						<c:if test="${DraftList.drStatus eq '선결완료'}"><label style="color: #009D9D;">선결승인</label></c:if>
						<c:if test="${DraftList.drStatus eq '진행중'}"><label style="color: black;">진행중</label></c:if>
					</td>
					<td><fmt:formatDate value="${DraftList.drDate}" pattern="yyyy-MM-dd HH:mm"/></td>
					<td></td>
					<td>${DraftList.empName}</td>
				</tr>
			</c:forEach>
			<!--<c:out value="${fn:length(approvalList)} }"/>;-->
			</tbody>
		</table>
		<div id="pagingArea" align="center" style="margin-top: 10px;">
				<c:if test="${pi.currentPage <= 1 }">
					[이전] &nbsp;
				</c:if>
				<c:if test="${pi.currentPage > 1 }">
					<c:url var="blistBack" value="approvalInProgress.app">
						<c:param name="pageNum" value="${pi.currentPage -1 }"/>
					</c:url>
					<a href="${blistBack }">[이전]</a> &nbsp;
				</c:if>
				<c:forEach var="p" begin="${pi.startPage }" end="${pi.endPage }">
					<c:if test="${p eq pi.currentPage }">
						<font color="red" size="4"><b>[${p}]</b></font>
					</c:if>
					<c:if test="${p ne pi.currentPage }">
						<c:url var="blistCheck" value="approvalInProgress.app">
							<c:param name="pageNum" value="${p }"/>
						</c:url>				
						<a href="${blistCheck }">${p}</a>
					</c:if>
				</c:forEach>
				<c:if test="${pi.currentPage >= pi.maxPage }">
					&nbsp;[다음]
				</c:if>
				<c:if test="${pi.currentPage < pi.maxPage }">
					<c:url var="blistEnd" value="approvalInProgress.app">
						<c:param name="pageNum" value="${pi.currentPage + 1 }"/>
					</c:url>
					&nbsp; <a href="${blistEnd}">[다음]</a>
				</c:if>
			</div>
			<br><br><br><br>
		<form method="post" action="selectApprovalOne.app" id="selectApprovalOne">
			<input type="hidden" id="oneDocu" name="oneDocu">
		</form>
		
	</div>

	<script>
		$(document).ready(function() {
			var flag = true;
			$("#spread").click(function() {
				if (!flag) {
					flag = true;
					$("#img1").css("transform", "rotate(0deg)");
					$("#ddta1").slideDown(500);
				} else {
					flag = false;
					$("#img1").css("transform", "rotate(270deg)");
					$("#ddta1").slideUp(500);

				}
			});

			var flag2 = true;
			$("#spread2").click(function() {
				if (!flag2) {
					flag2 = true;
					$("#img2").css("transform", "rotate(0deg)");
					$("#ddta2").slideDown(500);
				} else {
					flag2 = false;
					$("#img2").css("transform", "rotate(270deg)");
					$("#ddta2").slideUp(500);

				}
			});
		});
		
		function approvalDocuList(){
			location.href="selectDocuList.app";
		}
		
		$("#list tbody tr").click(function(){
			$("#oneDocu").val($(this).find(":nth-child(4)").text());
			$("#selectApprovalOne").submit();
		})
		
	</script>
</body>
</html>