<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<head>
<meta charset="UTF-8">
<link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="/opera/resources/se2/js/service/HuskyEZCreator.js" charset="utf-8"></script>
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<title>Insert title here</title>

<style>
body {
	font-family: Gothic A1;
}

.mainbutton {
	width: 200px;
	height: 49px;
	border-radius: 5px;
	color: white;
	background-color: #27334C;
	font-size: 22px;
}

.mainfont {
	font-size: 20px;
}

.mainbarfont {
	display: inline-block;
	width: 45%;
	font-size: 22px;
	font-weight: 600;
	padding-bottom: 5px;
}

.mainbarfont2 {
	display: inline-block;
	width: 45%;
	font-size: 14px;
	font-weight: 600;
}

.mainbarfont3 {
	display: inline-block;
	font-size: 18px;
	width: 60%;
}

.ddtag1 {
	padding-top: 10px;
	padding-left: 25px;
	color: #27334C;
	font-weight: 300;
	font-size: 16px;
	cursor: pointer;
	width: 40px;
}

button {
	border: 0;
	outline: 0;
}

.btn1 {
	font-size: 10px;
	width: 50px;
	height: 23px;
	margin-left: 5px;
	color: white;
	background-color: #27334C;
	border-radius: 1px;
}

#apporjoin td, th {
	border: 1px solid black;
	font-size: 13px;
	border: 2px solid #81858B;
}

#apporjoin th {
	background-color: #BEC2C9;
	font-weight: bold;
}

#apporjoin td {
	background-color: #EEEFF1;
	font-weight: 500;
}

.addprobtn {
	border: 1px solid #81848A;
	font-size: 10px;
	height: 20px;
	width: 70px;
	margin-left: 5px;
	text-align: center;
	vertical-align: middle;
}

#font .addtext {
	text-align: center;
	width: 100px;
	height: 26px;
	background-color: #B9BDC4;
	border-right: 1px solid #81848A;
	font-weight: 600;
}

#font .addtext2 {
	text-align: center;
	width: 80px;
	height: 26px;
	background-color: #E8E8E8;
	border: 1px solid #81848A;
	font-weight: 600;
}

#font .adddtext2 {
	text-align: center;
	width: 140px;
	height: 26px;
	background-color: #E8E8E8;
	border: 1px solid #81848A;
	font-weight: 600;
}

#font .addtext3 {
	text-align: center;
	width: 100px;
	height: 80px;
	background-color: #B9BDC4;
	border: 1px solid #81848A;
	font-weight: 600;
}

#font .addtext4 {
	text-align: center;
	width: 100px;
	height: 300px;
	background-color: #B9BDC4;
	border: 1px solid #81848A;
	font-weight: 600;
}

#font {
	font-size: 10px;
}

#font tr {
	border: 1px solid #81848A;
}

.radiobtn {
	vertical-align: middle;
	margin-left: 10px;
	margin-right: 3px;
	width: 13px;
}
/* The Modal (background) */
.modal {
	display: none; /* Hidden by default */
	position: fixed; /* Stay in place */
	z-index: 1; /* Sit on top */
	left: 0;
	top: 0;
	width: 100%; /* Full width */
	height: 100%; /* Full height */
	overflow: auto; /* Enable scroll if needed */
	background-color: rgb(0, 0, 0); /* Fallback color */
	background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
	background-color: #fefefe;
	margin: 15% auto; /* 15% from the top and centered */
	padding: 20px;
	border: 1px solid #888;
	width: 70%; /* Could be more or less, depending on screen size */
	height: 55%
}
.modal-content2 {
	background-color: #fefefe;
	margin: 15% auto; /* 15% from the top and centered */
	padding: 20px;
	border: 1px solid #888;
	width: 50%; /* Could be more or less, depending on screen size */
	height: 55%
}
.modal-content3 {
	background-color: #fefefe;
	margin: 15% auto; /* 15% from the top and centered */
	padding: 20px;
	border: 1px solid #888;
	width: 30%; /* Could be more or less, depending on screen size */
	height: 20%
}
.modal-content4 {
	background-color: #fefefe;
	margin: 15% auto; /* 15% from the top and centered */
	padding: 20px;
	border: 1px solid #888;
	width: 33%; /* Could be more or less, depending on screen size */
	height: 50%
}
.adddocubtn {
	background-color: #27334C;
	color: white;
	font-size: 12px;
	height: 20px;
	width: 80px;
	border-radius: 2px;
}
.che2:hover{
		color: white;
		background-color: #27334C;
}
.che{
	color:black; background-color: #B9BDC4;
}

.che2{
	color:black; background-color: white; border: 1px solid #81848A;"
}
.ccc{
	color:black; background-color: white; border: 1px solid #81848A;"
}
.ccc:visit{
	color: white;
	background-color: #27334C;
}
.prolist{
	 border-radius:3px; 
	 vertical-align: middle; 
	 margin-left:5px; 
	 border:1px solid #81848A; 
	 width:60px; height:20px; color:white; background-color: #27334C; padding-left:5px;padding-right:5px;
	 display: inline-block;
}
.removebtn{
	 border:0px; color:red; background-color: #27334C; height: 15px; font-size:10px; margin-left:5px; margin-top:3px;
}

.addclientlisttd{
	vertical-align:middle; 
	width: 100px; 
	background-color: #C4C4C4; 
	border: 1px solid #81848A; 
	font-size: 10px; 
	padding-left:10px; 
	height:20px; 
	font-weight: 600;
}
.deptList:hover{
	font-size:14px;
	color: #E61358;
	cursor: pointer;
}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/views/common/operaNav.jsp" />
	<jsp:include page="/WEB-INF/views/approval/cashNav.jsp" />
	
	<form method="post" action="insertCashDis.app" id="insertform" enctype="multipart/form-data">
		<input type="hidden" id="approvers" name="approvers"> <!-- 결재자들 정보 -->
		<input type="hidden" id="cooperators" name="cooperators"> <!-- 협조자들 정보 -->
		<input type="hidden" id="totals" name="totals"> <!-- 결재 로그 만들때 사용할것 -->
		<input type="hidden" id="receiver" name="receiver"> <!-- 수신자들 정보 -->
		<input type="hidden" id="expendform" name="expendform"> <!-- 지출형식 정보 -->
		<input type="hidden" id="realtitle" name="title"> <!-- 제목 정보 -->
		<input type="hidden" id="paytype" name="paytype"> <!-- 지급유형 정보 -->
		<input type="hidden" id="paynumber" name="paynumber"> <!-- 지급번호 정보 -->
		<input type="hidden" id="paydate" name="paydate"> <!-- 년월일 정보 -->
		<input type="hidden" id="paycontent" name="paycontent"> <!-- 내용 정보 -->
		<input type="hidden" id="payprice" name="payprice"> <!-- 금액 정보 -->
		<input type="hidden" id="paycompany" name="paycompany"> <!-- 업체명 정보 -->
		<input type="hidden" id="paybank" name="paybank"> <!-- 지급은행정보 -->
		<input type="hidden" id="banknumber" name="banknumber"> <!-- 계좌번호 정보 -->
		<input type="hidden" id="bankholder" name="bankholder"> <!-- 예금주 정보 -->
		<input type="hidden" id="remark" name="remark"> <!-- 비고정보 -->
		<input type="hidden" id="deposit" name="deposit"> <!-- 합계 금액 정보 -->
		<input type="hidden" id="share" name="share"> <!-- 부서 공유 정보 -->
		<input type="hidden" id="summary" name="summary"> <!-- 내용 요약 정보 -->
		<input type="hidden" id="details" name="details"> <!-- 내용 요약 정보 -->
		<input type="hidden" name="empNo" value="${loginUser.empNo}">
	
	
	<div style="padding-top: 70px; padding-left: 350px;">
		<div style="border: 1.5px solid #81848A;">
			<div align="center"
				style="margin-top: 50px; font-size: 30px; font-weight: 500;">
				지출결의서
				<hr>
			</div>
			<br>

			<!-- 결재나 협조 공간 -->
			<div id="apporjoin" style="float: right; display: block;">
				<div>
					<table id="appline"
						style="border-collapse: collapse; border: 1px solid #81848A; text-align: center; margin-right: 20px; margin-bottom: 10px;">
						<tr>
							<th style="width: 60px;" rowspan="2">결재</th>
							<th style="width: 70px; height: 20px;">작성자</th>
							<th style="width: 70px;"></th>
							<th style="width: 70px;"></th>
							<th style="width: 70px;"></th>
							<th style="width: 70px;"></th>
							<th style="width: 70px;"></th>
						</tr>
						<tr>
							<td style="width: 70px; height: 40px;">${loginUser.empName }</td>
							<td style="width: 70px;"></td>
							<td style="width: 70px;"></td>
							<td style="width: 70px;"></td>
							<td style="width: 70px;"></td>
							<td style="width: 70px;"></td>
						</tr>
					</table>
				</div>
				<div style="float: right;">
					<table id="appline2"
						style="border-collapse: collapse; border: 1px solid black; text-align: center; margin-right: 20px;">
						<tr>
							<th style="width: 60px;" rowspan="2">협조</th>
							<th style="width: 70px; height: 20px;"></th>
							<th style="width: 70px;"></th>
							<th style="width: 70px;"></th>
							<th style="width: 70px;"></th>
							<th style="width: 70px;"></th>
						</tr>
						<tr>
							<td style="width: 70px; height: 40px;"></td>
							<td style="width: 70px;"></td>
							<td style="width: 70px;"></td>
							<td style="width: 70px;"></td>
							<td style="width: 70px;"></td>
						</tr>
						
					</table>
					<div style="float: right; margin-right: 18px; margin-top: 10px;">
						<table>
							<tr>
								<td style="border: 1px solid #81858B; background-color: white;">
									<button type="button" style="padding-right: 10px; height: 20px; font-size: 10px;" onclick="open_modal1();">
										<img style="margin-left: 5px; margin-right: 5px; width:12px;"
											src="/opera/resources/icons/approvalImages/Pluss.png">결재라인
										추가
									</button>
								</td>
								<td style="border: 0px; background-color: white;">&nbsp;</td>
								<td style="border: 1px solid #81858B; background-color: white;">
									<button type="button" style="padding-right: 10px; height: 20px; font-size: 10px;" onclick="open_modal10();" >
										<img style="margin-left: 5px; margin-right: 5px; width:12px;"
											src="/opera/resources/icons/approvalImages/Pluss.png">개인결재선
									</button>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div id="font" style="margin-top: 200px; width: 100%">
				<table id="asd"
					style="width: 100%; border: 1px solid #81848A; border-collapse: collapse;">
					<tr>
						<td class="addtext">
							<div>수신참조</div>
						</td>
						<td style="background-color: #FAFAFA;" colspan="10">
							<button type="button" class="addprobtn" onclick="open_modal2();">수신자등록</button>
							<div style="display: inline-block;" id="listpro">
								<!-- <div class="prolist">
									<label>정용탁상무</label>
									<button class="removebtn">X</button>
								</div>  -->
							</div>
						</td>
					</tr>
					<tr>
						<td class="addtext">
							<div>지출형식</div>
						</td>
						<td style="background-color: #FAFAFA;" colspan="10"><input
							type="radio" class="radiobtn" name="expendfor" checked="" value="법인카드 사용"><label>법인카드
								사용</label> <input type="radio" class="radiobtn" name="expendfor" value="송금"><label>송금</label>
							<input type="radio" class="radiobtn" name="expendfor" value="현금가지급정산"><label>현금가지급정산</label>
						</td>
					</tr>
					<tr>
						<td class="addtext">
							<div>기안일</div>
						</td>
						<td style="background-color: #FAFAFA;" colspan="2">
							<div
								style="width: 200px; display: inline-block; text-align: center;"><span id="today"></span></div>
						</td>
						<td class="addtext" colspan="1">부서</td>
						<td style="background-color: #FAFAFA;" colspan="3">
							<div
								style="width: 200px; display: inline-block; text-align: center;">${empDeptName }</div>
						</td>
						<td class="addtext" colspan="1">이름</td>
						<td style="background-color: #FAFAFA;" colspan="3">
							<div
								style="width: 200px; display: inline-block; text-align: center;">${loginUser.empName }</div>
						</td>
					</tr>
					<tr>
						<td class="addtext">
							<div>제목</div>
						</td>
						<td style="background-color: #FAFAFA;" colspan="10"><input id="title"
							type="text" style="margin-left: 5px; width: 98%;"></td>
					</tr>
					<tr>
						<td class="addtext">
							<div>지급유형</div>
						</td>
						<td style="background-color: #FAFAFA;" colspan="4">
							<div
								style="width: 200px; display: inline-block; text-align: center;">
								<select id="fakepaytype"
									style="width: 300px; margin-left: 5px; height: 20px; font-size: 10px;">
									<option>법인카드</option>
									<option>체크카드</option>
									<option>복지카드</option>
								</select>
							</div>
						</td>
						<td class="addtext" colspan="1">지급번호</td>
						<td style="background-color: #FAFAFA;" colspan="5">
							<div
								style="width: 330px; display: inline-block; text-align: center;">
								<select id="fakepaynumber"
									style="width: 250px; margin-left: 5px; height: 20px; font-size: 10px;">
									
								</select>
								<button  type="button" style=" border-radius:3px; width: 50px; height: 20px; font-size: 10px; color: white; background-color: #27334C;" onclick="addpaynumbermodal()">등록</button>
							</div>
						</td>
					</tr>






					<tr>
						<td class="addtext">
							<div>선택</div>
						</td>
						<td class="addtext" colspan="2">
							<div>년월일</div>
						</td>
						<td class="addtext">
							<div>내용</div>
						</td>
						<td class="addtext">
							<div>금액</div>
						</td>
						<td class="addtext">
							<div>업체명</div>
						</td>
						<td class="addtext">
							<div>지급은행</div>
						</td>
						<td class="addtext" colspan="1">
							<div>계좌번호</div>
						</td>
						<td class="addtext">
							<div>예금주</div>
						</td>
						<td class="addtext">
							<div>비고</div>
						</td>

					</tr>


					<tr class="last">
						<td class="addtext2">
							<div></div>
						</td>
						<td class="addtext2" colspan="2">
							<div>
								<input class="fakedate" type="date"
									style="width: 87%; padding-left: 10px; font-size: 10px;" >
							</div>
						</td>
						<td class="addtext2"><select class="fakepaycontent"
							style="font-size: 10px; width: 95%; height: 20px;">
								<option>선택</option>
								<option>교통비</option>
								<option>도서구입</option>
								<option>식대</option>
								<option>주유비</option>
								<option>지급수수료</option>
								<option>복리후생비</option>
						</select></td>
						<td class="addtext2">
							<div>
								<input class="fakepayprice" type="text" style="padding-left:2px; width: 80px; font-size: 10px;" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');"> 
							</div>
						</td>
						<td class="adddtext2">
							<div>
								<input type="text" class="fakepaycompany"
									style="padding-left:2px; width: 95px; vertical-align: bottom; font-size: 10px;">
								<button type="button" onclick="addclientmodal(this);"
									style="width: 25px; border: 1px solid #27334C; font-size: 10px; height: 20px;">찾기</button>
							</div>
						</td>
						<td class="addtext2"><input type="text" class="fakepaybank"
							style="padding-left:2px; width: 85px; vertical-align: bottom; font-size: 10px;"></td>
						<td class="addtext2"><input type="text" class="fakebanknumber"
							style="padding-left:2px; width: 85px; vertical-align: bottom; font-size: 10px;"></td>
						<td class="addtext2"><input type="text" class="fakebankholder"
							style="padding-left:2px; width: 85px; vertical-align: bottom; font-size: 10px;"></td>
						<td class="addtext2"><input type="text" class="fakeremark"
							style="padding-left:2px; width: 85px; vertical-align: bottom; font-size: 10px;"></td>
					</tr>
					<tr class="last">
						<td class="addtext2">
							<div>
								<input type="checkbox" style="vertical-align: text-bottom;">
							</div>
						</td>
						<td class="addtext2" colspan="2">
							<div>
								<input class="fakedate" type="date"
									style="width: 87%; padding-left: 10px; font-size: 10px;">
							</div>
						</td>
						<td class="addtext2"><select class="fakepaycontent"
							style="font-size: 10px; width: 95%; height: 20px;">
								<option>선택</option>
								<option>교통비</option>
								<option>도서구입</option>
								<option>식대</option>
								<option>주유비</option>
								<option>지급수수료</option>
								<option>복리후생비</option>
						</select></td>
						<td class="addtext2">
							<div>
								<input class="fakepayprice" type="text" style="padding-left:2px; width: 80px; font-size: 10px;" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
							</div>
						</td>
						<td class="adddtext2">
							<div>
								<input type="text" class="fakepaycompany"
									style="padding-left:2px; width: 95px; vertical-align: bottom; font-size: 10px;">
								<button  type="button" onclick="addclientmodal(this);"
									style="width: 25px; border: 1px solid #27334C; font-size: 10px; height: 20px;">찾기</button>
							</div>
						</td>
						<td class="addtext2"><input type="text" class="fakepaybank"
							style="padding-left:2px; width: 85px; vertical-align: bottom; font-size: 10px;"></td>
						<td class="addtext2"><input type="text" class="fakebanknumber"
							style="padding-left:2px; width: 85px; vertical-align: bottom; font-size: 10px;"></td>
						<td class="addtext2"><input type="text" class="fakebankholder"
							style="padding-left:2px; width: 85px; vertical-align: bottom; font-size: 10px;"></td>
						<td class="addtext2"><input type="text" class="fakeremark"
							style="padding-left:2px; width: 85px; vertical-align: bottom; font-size: 10px;"></td>
					</tr>
					

					<tr>
						<td colspan="11" style="height:30px;">
							<div style="float: right; margin-right: 5px;">
								<button type="button" onclick="addexpenditem()" style="border: 1px solid gray; width:100px; height:20px; font-size: 12px; margin-right: 5px;">지출항목추가</button>
								<button type="button" class="deleteexpenditem" style="border: 1px solid gray; width:100px; height:20px; font-size: 12px;">지출항목삭제</button>
							</div>
						</td>
					</tr>
					
					<tr>
						<td class="addtext">
							<div>합계</div>
						</td>
						<td style="background-color: #FAFAFA;" colspan="10"><input class="fakedeposit"
							type="text" style="margin-left: 5px; width: 20%; text-align: right; padding-right:5px;" readonly="readonly" value="0원"></td>
					</tr>
					
					<tr>
						<td class="addtext">
							<div>내용요약</div>
						</td>
						<td style="background-color: #FAFAFA;" colspan="10"><input class="fakesummary" 
							type="text" style="margin-left: 5px; width: 98%; padding-left:5px; font-size: 10px;"></td>
					</tr>
					<tr>
						<td class="addtext">
							<div>부서공유</div>
						</td>
						<td style="background-color: #FAFAFA;" colspan="10">
							<input type="checkbox" style="vertical-align: text-bottom; margin-left: 5px;" class="fakeshare"> 
							<label>사용</label>
							<label style="color: red;">[선택시 문서가 작성자의 소속 부서원들에게 공유되며 해당 문서는 부서 결재함에서 확인이 가능합니다.]</label>
						</td>
					</tr>
					<tr>
						<td class="addtext" colspan="1">
							<div style="display: inline-block;">
								<input type="file" multiple id="fakeFiles" name="fileCode" style="width:75px;">
							</div>
						</td>
						<td class="addtext" colspan="6">
							<div style="display: inline-block;">파일명</div>
						</td>
						<td class="addtext" colspan="3">
							<div>크기</div>
						</td>
						
					</tr>
					<tr style="text-align: center;">
						<td>
						
						</td>
						<td colspan="6" style="padding-top:9px;" id="fileNametd">
						
						</td>
						<td colspan="3" style="padding-top:9px;" id="fileSizetd">
						
						</td>                                                             
					</tr>
					<tr>
						<td class="addtext4" colspan="1" >
							<div>세부내용</div>
						</td>
						<td colspan="10">
							<textarea name="ir1" id="ir1" rows="10" cols="100" style="width:980px; height: 300px;"></textarea>
						</td>
					</tr>
					<tr>
						<td colspan="11" style="height:30px; border: 1px solid white;">
							<div style="float: right; margin-right: 5px; margin-top: 5px;">
								<button  type="button" onclick="insert();" style="border: 1px solid #27334C; border-radius: 3px; background-color: #27334C; color: white;width:100px; height:20px; font-size: 12px; margin-right: 5px;">결재하기</button>
								<button  type="button" style="border: 1px solid gray; border-radius: 3px; width:100px; height:20px; font-size: 12px;">취소하기</button>
							</div>
						</td>
					</tr>
				</table>
				<br><br>
			</div>
		</div>
	<br><br><br>
	</div>
	</form>	
	<!-- The Modal1 -->
    <div id="myModal" class="modal">
 
      <!-- Modal content -->
      <div class="modal-content" style="">
            <p style="font-size: 17px;">직원목록</p>
            	<table style="font-size: 12px;">
            		<tr>
            			<td style="width: 200px;border: 1px solid black; height: 300px; padding-left:10px; padding-top:10px;" valign="top" >
            			
            			<button id="spread3" style="background-color: white;">
						<img id="img3"
							src="/opera/resources/icons/approvalImages/arrow.png">
						&nbsp;
							</button>OperaDemo
								<dl style="margin-left: 30px;" id="ddta3">
								<c:forEach var="deptList" items="${deptList }">
									<dt style="margin-top:2px;" class="deptList">${deptList.deptName }</dt>
								</c:forEach>
								</dl>
						</td>
						<td valign="top">
							<table style="margin-left:4px;">
								<tr>
									<td style="border: 1px solid #81848A; width:70px; height:30px; background-color: #B9BDC4; color: black; text-align: center;">성명</td>
									<td style="border: 1px solid #81848A; width:210px; height:30px; background-color: #B9BDC4; color: black;">
										<input onchange="print()" type="text" style="margin-left: 5px; height: 20px;" id="search1">
										<button style="width:40px; height:23px; color:white; background-color: #27334C; border-radius: 3px;" class="deptList">검색</button>
									</td>
								</tr>
							</table>
							<table  style="font-size: 10px; border-collapse: collapse; margin-left:5px;" id="modal1">
							<thead>
								<tr style=" text-align: center;">
									<td class="che" style="width:100px;height: 30px; border: 1px solid #81848A;">부서</td>
									<td class="che" style="width:100px; border: 1px solid #81848A;">성명</td>
									<td class="che" style="width:100px; border: 1px solid #81848A;">직위</td>
								</tr>
							</thead>
							<tbody>
							
							</tbody>
							<tfoot>
								<tr style="text-align: center;">
									<td colspan="4" style="padding-top: 10px;">
										
									</td>
								</tr>
							</tfoot>
							</table>
						</td>
						<td>
							<button 
							style="border-radius:2px; color: white; background-color: #27334C; margin-bottom: 5px; font-size: 10px; width:50px; height: 20px;"
							onclick="addapproval();"
							>
							결재
							</button><br>
							<button style="border-radius:2px; ; color: white; background-color: #E61358; font-size: 10px; width:50px; height: 20px;" onclick="addapproval2();">협조</button>
						</td>
						<td valign="top">
							<table style="border-collapse: collapse; text-align: center; margin-left: 5px;" id="checktable">
							<thead>
								<tr style="font-size: 10px;">
									<th style="font-size: 10px;border-right: 1px solid #81848A; width:30px; height:25px; font-weight: 500;">No</th>
									<th style="font-size: 10px;border-right: 1px solid #81848A; width:80px; height:25px;font-weight: 500;">결재/협조</th>
									<th style="font-size: 10px;border-right: 1px solid #81848A; width:80px; height:25px;font-weight: 500;">부서</th>
									<th style="font-size: 10px;border-right: 1px solid #81848A; width:70px; height:25px;font-weight: 500;">직위</th>
									<th style="font-size: 10px;border-right: 1px solid #81848A; width:70px; height:25px;font-weight: 500;">성명</th>
									<th style="font-size: 10px;border-right: 1px solid #81848A; width:70px; height:25px;font-weight: 500;">순서</th>
									<th style="font-size: 10px;border-right: 1px solid #81848A; width:30px; height:25px;font-weight: 500;">삭제</th>
								</tr>
							<thead>
							</table>
						</td>
            		</tr>
            	</table>
            	<div style="float: right;">
            		<button style="width: 70px; height: 25px; font-size: 12px; background-color: #27334C; color: white; border-radius: 4px;" class="add_modal1" onclick="add();">적용</button>
            		<button style="width: 70px; height: 25px; font-size: 12px;border: 1px solid #81848A; border-radius: 4px;"onClick="close_modal1();">닫기</button>
            	</div>
      </div>
 
    </div>
        <!--End Modal-->

	<!-- The Modal2 -->
    <div id="myModal2" class="modal">
 
      <!-- Modal content -->
      <div class="modal-content2" style="">
            <p style="font-size: 17px;">직원목록</p>
            	<table style="font-size: 12px;">
            		<tr>
            			<td style="width: 200px;border: 1px solid black; height: 300px; padding-left:10px; padding-top:10px;" valign="top" >
            			
            			<button id="spread8" style="background-color: white;">
						<img id="img8"
							src="/opera/resources/icons/approvalImages/arrow.png">
						&nbsp;
							</button>OperaDemo
								<dl style="margin-left: 30px;" id="ddta3">
								<c:forEach var="deptList" items="${deptList }">
									<dt style="margin-top:2px;" class="deptList">${deptList.deptName}</dt>
								</c:forEach>
								</dl>
						</td>
						<td valign="top">
							<table style="margin-left:4px;">
								<tr>
									<td style="border: 1px solid #81848A; width:70px; height:30px; background-color: #B9BDC4; color: black; text-align: center;">성명</td>
									<td style="border: 1px solid #81848A; width:210px; height:30px; background-color: #B9BDC4; color: black;">
										<input onchange="print()" type="text" style="margin-left: 5px; height: 20px;" id="search2">
										<button style="width:40px; height:23px; color:white; background-color: #27334C; border-radius: 3px;" class="deptList">검색</button>
									</td>
								</tr>
							</table>
							<table  style="font-size: 10px; border-collapse: collapse; margin-left:5px;" id="modal2">
							<thead>
								<tr style=" text-align: center;">
									<td class="che" style="width:100px;height: 30px; border: 1px solid #81848A;">부서</td>
									<td class="che" style="width:100px; border: 1px solid #81848A;">성명</td>
									<td class="che" style="width:100px; border: 1px solid #81848A;">직위</td>
								</tr>
							</thead>
							<tbody>
							
							</tbody>
							<tfoot>
								<tr style="text-align: center;">
									<td colspan="4" style="padding-top: 10px;">
										
									</td>
								</tr>
							</tfoot>
							</table>
						</td>
						<td>
							<button 
							style="border-radius:2px; color: white; background-color: #27334C; margin-bottom: 5px; font-size: 10px; width:50px; height: 20px;"
							onclick="addreceiver();"
							>
							추가
							</button>
						</td>
						<td valign="top">
							<table style="border-collapse: collapse; text-align: center; margin-left: 5px;" id="receivertable">
							<thead>
								<tr style="font-size: 10px; border: 1px solid #81848A;">
									<th style="border-right: 1px solid #81848A; width:30px; height:25px;">No</th>
									<th style="border-right: 1px solid #81848A; width:150px; height:20px;">받는이</th>
								</tr>
							<thead>
							</table>
						</td>
            		</tr>
            	</table>
            	<div style="float: right;">
            		<button style="width: 70px; height: 25px; font-size: 12px; background-color: #27334C; color: white; border-radius: 4px;" class="add_modal1" onclick="add2();">적용</button>
            		<button style="width: 70px; height: 25px; font-size: 12px;border: 1px solid #81848A; border-radius: 4px;"onClick="close_modal2();">닫기</button>
            	</div>
      </div>
 
    </div>
        <!--End Modal2-->
        
    
    
    <!-- Start apppaymodal -->    
    <div id="addpaymodal" class="modal">
 
      <!-- Modal content -->
      <div class="modal-content3" style="">
            <p style="font-size: 17px;">지급번호 등록</p>
            	<table style="font-size: 12px; border-collapse: collapse; margin-top:5px;" id="addProvision">
            		<tr style="text-align: center; color: black; background-color: #B9BDC4; font-weight: 600">
            			<td style="width:100px; height:25px; border: 1px solid #81848A;">카드 유형</td>
            			<td style="width:100px; border: 1px solid #81848A;">은행 유형</td>
            			<td style="width:250px; border: 1px solid #81848A;">번호 입력</td>
            		</tr>
            		<tr>
            			<td style="width:100px; height:25px; border: 1px solid #81848A; text-align: center;">
            				<select style="width: 70px; border:0px; outline: none; font-size: 12px;">
            					<option>법인카드</option>
            					<option>체크카드</option>
            					<option>복지카드</option>
            				</select>
            			</td>
            			<td style="width:100px; border: 1px solid #81848A; text-align: center;">
            				<select style="width: 70px; border:0px; outline: none; font-size: 12px;">
            					<option>신한은행</option>
            					<option>하나은행</option>
            					<option>경남은행</option>
            					<option>기업은행</option>
            					<option>국민은행</option>
            				</select>
            			</td>
            			<td style="width:250px; border: 1px solid #81848A;">
            				<input type="text" id="f1" onchange="print()" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="4" style="margin-left:5px; width: 50px;" />
            				<input type="text" id="f2" onchange="print()" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="4" style="margin-left:5px; width: 50px;" />
            				<input type="text" id="f3" onchange="print()" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="4" style="margin-left:5px; width: 50px;" />
            				<input type="text" id="f4" onchange="print()" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="4" style="margin-left:5px; width: 50px;" />
            			</td>
            		</tr>
            	</table>
            	<div style="float: right; margin-top:10px;">
            	
            	
            		<button style="width: 70px; height: 25px; font-size: 12px; background-color: #27334C; color: white; border-radius: 4px;" class="add_modal1" onclick="addProvision();">적용</button>
            		<button style="width: 70px; height: 25px; font-size: 12px;border: 1px solid #81848A; border-radius: 4px;"onClick="close_modal3();">닫기</button>
            	</div>
      </div>
 
    </div>
        <!--End Modal-->    
        
        
    <!-- Start addclientmodal -->
    <div id="addclientmodal" class="modal">
 
      <!-- Modal content -->
      <div class="modal-content4" style="">
            <p style="font-size: 17px;">거래처 목록</p>
            	<table style="border-collapse: collapse;">
            		<tr style="border: 1px solid #81848A;">
            			<td style="width:100px; height:30px; border: 1px solid #81848A; text-align: center;  font-size: 10px;">
            				<select style="width: 70px; border:0px; outline: none; font-size: 12px;" id="searchClientType">
            					<option>거래처명</option>
            					<option>주소</option>
            				</select>
            			</td>
            			<td style="width:400px; background-color: #B9BDC4; ">
            				<input onchange="print()" type="text" style="width: 100px; margin-left:5px; font-size: 10px; height:18px; vertical-align: middle;" id="searchClientContent">
            				<button style="width: 50px; height: 22px; font-size: 10px; background-color: #27334C; color: white; border-radius: 4px;" id="searchClient">검색</button>
            				
            				<div style="float: right; margin-right:5px;">
            					<button style="width: 70px; height: 22px; font-size: 10px; background-color: #27334C; color: white; border-radius: 4px;" onclick="addclientlist();">업체 등록</button>
            				</div>
            			</td>
            		</tr>
            	</table>
            	
            	<table style="border-collapse: collapse; margin-top:5px; text-align: center;" id="clientTable">
	            	<thead>
	            		<tr style="font-size: 10px;">
	            			<th style="width:30px; border:1px solid #81848C; height: 25px;font-size: 10px;">No</th>
	            			<th style="width:130px; border:1px solid #81848C; background-color: #B9BDC4;font-size: 10px;">거래처명</th>
	            			<th style="width:250px; border:1px solid #81848C; background-color: #B9BDC4;font-size: 10px;">주소</th>
	            			<th style="width:87px; border:1px solid #81848C; background-color: #B9BDC4;font-size: 10px;">등록/변경자</th>
	            		</tr>
	            	</thead>
	            	<tbody>
	            		
            		</tbody>
            		<tfoot>
            			<tr style="text-align: center;">
							<td colspan="4" style="padding-top: 10px;">
									
							</td>
						</tr>
            		</tfoot>
            	</table>
            	<div style="float: right; margin-top: 10px;">
            		<button style="width: 70px; height: 25px; font-size: 12px; background-color: #27334C; color: white; border-radius: 4px;" class="add_modal1" onclick="selectClientOne();">적용</button>
            		<button style="width: 70px; height: 25px; font-size: 12px;border: 1px solid #81848A; border-radius: 4px;"onClick="close_modal4();">닫기</button>
            	</div>
      </div>
 
    </div>
        <!--End addclientmodal-->
        
        
         <!-- Start addclientmodal -->
    <div id="addclientlistmodal" class="modal">
 
      <!-- Modal content -->
      <div class="modal-content4" style="">
            <p style="font-size: 17px;">거래처 등록</p>
            	<table style="border-collapse: collapse;" id="addclienttable">
            		<tr style="vertical-align: middle;">
            			<td class="addclientlisttd"> 거래처명 </td>
            			<td style="width: 350px; height: 28px; border: 1px solid #81848A;"> 
            			<input id="clientName" onchange="print()" type="text" style="height: 20px; font-size: 10px; margin-left:5px; width:350px; padding-left:5px;"></td>
            		</tr>
            		<tr style="vertical-align: middle;">
            			<td class="addclientlisttd"> 사업자등록번호 </td>
            			<td style="width: 300px; height: 28px; border: 1px solid #81848A;"> 
            			<input id="clientNumber1" onchange="print()" type="text" style="height: 20px; font-size: 10px; margin-left:5px; width:23px; padding-left:5px;" maxlength="3" onKeyup="this.value=this.value.replace(/[^0-9]/g,'')">
            			&nbsp;-&nbsp;
            			<input id="clientNumber2" onchange="print()" type="text" style="height: 20px; font-size: 10px; margin-left:5px; width:18px; padding-left:5px;" maxlength="2" onKeyup="this.value=this.value.replace(/[^0-9]/g,'')">
            			&nbsp;-&nbsp;
            			<input id="clientNumber3" onchange="print()" type="text" style="height: 20px; font-size: 10px; margin-left:5px; width:35px; padding-left:5px;" maxlength="5" onKeyup="this.value=this.value.replace(/[^0-9]/g,'')">
            			
            			</td>
            		</tr>
            		<tr style="vertical-align: middle;">
            			<td class="addclientlisttd"> 우편번호 </td>
            			<td style="width: 300px; height: 28px; border: 1px solid #81848A;"> 
            			<input onchange="print()" type="text" style="height: 20px; font-size: 10px; margin-left:5px; width:100px;padding-left:5px;" id="sample4_postcode" readonly="readonly">
						<input type="button" onclick="sample4_execDaumPostcode()" value="우편번호 찾기"><br>
						</td>
            		</tr> 
            		<tr style="vertical-align: middle;">
            			<td class="addclientlisttd" > 주소 </td>
            			<td style="width: 300px; height: 28px; border: 1px solid #81848A;"> 
            			<input onchange="print()" type="text" style="height: 20px; font-size: 10px; margin-left:5px; width:165px; padding-left:5px;" id="sample4_roadAddress" readonly="readonly">
            			<input onchange="print()" type="text" style="height: 20px; font-size: 10px; margin-left:5px; width:168px; padding-left:5px;" id="addressDetails">
            			
            			</td>
            		</tr>
            		<tr style="vertical-align: middle;">
            			<td class="addclientlisttd"> 은행명 </td>
            			<td style="width: 300px; height: 28px; border: 1px solid #81848A;"> 
            			<input id="clientBankName" onchange="print()" type="text" style="height: 20px; font-size: 10px; margin-left:5px; width:345px; padding-left:5px;"></td>
            		</tr>
            		<tr style="vertical-align: middle;">
            			<td class="addclientlisttd"> 계좌번호 </td>
            			<td style="width: 300px; height: 28px; border: 1px solid #81848A;"> 
            			<input id="clientBankNumber" onchange="print()" type="text" style="height: 20px; font-size: 10px; margin-left:5px; width:345px; padding-left:5px;"></td>
            		</tr>
            		<tr style="vertical-align: middle;">
            			<td class="addclientlisttd"> 예금주명 </td>
            			<td style="width: 300px; height: 28px; border: 1px solid #81848A;">
            			<input id="clientDepositName" onchange="print()" type="text" style="height: 20px; font-size: 10px; margin-left:5px; width:350px;padding-left:5px;"></td>
            		</tr>
            	</table>
            	<div style="float: right; margin-top: 10px;">
            		<button style="width: 70px; height: 25px; font-size: 12px; background-color: #27334C; color: white; border-radius: 4px;" class="add_modal1" onclick="addclient();">적용</button>
            		<button style="width: 70px; height: 25px; font-size: 12px;border: 1px solid #81848A; border-radius: 4px;"onClick="close_modal5();">닫기</button>
            	</div>
      </div>
 
    </div>
        <!--End addclientmodal-->
        
        
        
        
     
    <!-- 개인 결재선 관리  시작 -->
    
    <div id="personalApprovalList" class="modal">
      <!-- Modal content -->
      <div class="modal-content4" style="">
            <span style="font-size: 17px;">개인 결재선</span> <span> <div style="float: right; margin-right:5px;">
            					<button style="width: 70px; height: 22px; font-size: 10px; background-color: #27334C; color: white; border-radius: 4px;" onclick="open_modal11();">결재선 추가</button>
            				</div></span>
            	
            	<table style="border-collapse: collapse; margin-top:5px; text-align: center;" id="personalApprovalTable">
	            	<thead>
	            		<tr style="font-size: 10px;">
	            			<th style="width:30px; border:1px solid #81848C; height: 25px;font-size: 10px;">No</th>
	            			<th style="width:260px; border:1px solid #81848C; background-color: #B9BDC4;font-size: 10px;">주소</th>
	            			<th style="width:100px; border:1px solid #81848C; background-color: #B9BDC4;font-size: 10px;">등록/변경자</th>
	            			<th style="width:100px; border:1px solid #81848C; background-color: #B9BDC4;font-size: 10px;">등록번호</th>
	            		</tr>
	            	</thead>
	            	<tbody>
	            		
	            	</tbody>
            	</table>
            	<div style="float: right; margin-top: 10px;d">
            		<button style="width: 70px; height: 25px; font-size: 12px; background-color: #27334C; color: white; border-radius: 4px;" class="add_modal1" onclick="addMyApproval();">적용</button>
            		<button style="width: 70px; height: 25px; font-size: 12px;border: 1px solid #81848A; border-radius: 4px;"onClick="close_modal10();">닫기</button>
            	</div>
      </div>
 
    </div>
    
    <div id="addpersonalApprovalList" class="modal">
     <!-- Modal content -->
      <div class="modal-content" style="">
            <p style="font-size: 17px;">직원목록</p>
            	<table style="font-size: 12px;">
            		<tr>
            			<td style="width: 200px;border: 1px solid black; height: 300px; padding-left:10px; padding-top:10px;" valign="top" >
            			
            			<button id="spread3" style="background-color: white;">
						<img id="img3"
							src="/opera/resources/icons/approvalImages/arrow.png">
						&nbsp;
							</button>OperaDemo
								<dl style="margin-left: 30px;" id="ddta3">
								<c:forEach var="deptList" items="${deptList }">
									<dt style="margin-top:2px;" class="deptList">${deptList.deptName }</dt>
								</c:forEach>
								</dl>
						</td>
						<td valign="top">
							<table style="margin-left:4px;">
								<tr>
									<td style="border: 1px solid #81848A; width:70px; height:30px; background-color: #B9BDC4; color: black; text-align: center;">성명</td>
									<td style="border: 1px solid #81848A; width:210px; height:30px; background-color: #B9BDC4; color: black;">
										<input onchange="print()" type="text" style="margin-left: 5px; height: 20px;" id="search3">
										<button style="width:40px; height:23px; color:white; background-color: #27334C; border-radius: 3px;" class="deptList">검색</button>
									</td>
								</tr>
							</table>
							<table  style="font-size: 10px; border-collapse: collapse; margin-left:5px;" id="modal3">
							<thead>
								<tr style=" text-align: center;">
									<td class="che" style="width:100px;height: 30px; border: 1px solid #81848A;">부서</td>
									<td class="che" style="width:100px; border: 1px solid #81848A;">성명</td>
									<td class="che" style="width:100px; border: 1px solid #81848A;">직위</td>
								</tr>
							</thead>
							<tbody>
							
							</tbody>
							<tfoot>
								<tr style="text-align: center;">
									<td colspan="4" style="padding-top: 10px;">
										
									</td>
								</tr>
							</tfoot>
							</table>
						</td>
						<td>
							<button 
							style="border-radius:2px; color: white; background-color: #27334C; margin-bottom: 5px; font-size: 10px; width:50px; height: 20px;"
							onclick="addPersonalApprovalList1();"
							>
							결재
							</button><br>
							<button style="border-radius:2px; ; color: white; background-color: #E61358; font-size: 10px; width:50px; height: 20px;" onclick="addPersonalApprovalList2();">협조</button>
						</td>
						<td valign="top">
							<table style="border-collapse: collapse; text-align: center; margin-left: 5px;" id="personaltable">
							<thead>
								<tr>
									<th colspan="2" style="border-right: 1px solid #81848A; width:30px; height:30px;">결재선명</th>
									<th colspan="5"><input type="text" style="width: 315px;" id="personalApprovalTitle" onchange="print()"></th>
								</tr>
								<tr style="font-size: 10px; border: 1px solid #81848A;">
									<th style="border-right: 1px solid #81848A; width:30px; height:25px;">No</th>
									<th style="border-right: 1px solid #81848A; width:80px; height:20px;">결재/협조</th>
									<th style="border-right: 1px solid #81848A; width:80px; height:20px;">부서</th>
									<th style="border-right: 1px solid #81848A; width:70px; height:20px;">직위</th>
									<th style="border-right: 1px solid #81848A; width:70px; height:20px;">성명</th>
									<th style="border-right: 1px solid #81848A; width:70px; height:20px;">순서</th>
									<th style="border-right: 1px solid #81848A; width:30px; height:20px;">삭제</th>
								</tr>
							<thead>
							<tbody>
							
							</tbody>
							</table>
						</td>
            		</tr>
            	</table>
            	<div style="float: right;">
            		<button style="width: 70px; height: 25px; font-size: 12px; background-color: #27334C; color: white; border-radius: 4px;" class="add_modal11">적용</button>
            		<button style="width: 70px; height: 25px; font-size: 12px;border: 1px solid #81848A; border-radius: 4px;"onClick="close_modal11();">닫기</button>
            	</div>
      </div>
    </div>
    <!-- 개인 결재선 관리 끝 -->
	<script>
	
	$.ajax({
		type:"POST",
		url: "selectProVisionList.app",
		data: {
			"proCardDiv" : "법인카드"
		},
		success: function(data){
			$("#fakepaynumber").empty();
			if(data.length != 0){
				for(var i = 0 ; i < data.length; i ++){
					$("#fakepaynumber").append('<option>' + data[i].proBankDiv +'&nbsp&nbsp' + data[i].proCardNumber + '</option>');	
				}
			} else {
				$("#fakepaynumber").append('<option>등록된 지급번호가 없습니다.</option>');	
			}
			
		}

	});
	function insert(){
		$("#expendform").val($("input:radio[name='expendfor']:checked").val()); // 지출형식
		$("#realtitle").val($("#title").val()); // 제목
		$("#paytype").val($("#fakepaytype option:selected").val()); //지급 유형
		$("#paynumber").val($("#fakepaynumber option:selected").val()); //지급 번호
		
		
		// 년월일, 내용, 금액, 업체명, 지급은행, 계좌번호, 예금주, 비고 
		var fakedate = "";
        var fakepaycontent = "";
        var fakepayprice = "";
        	
        var fakepaycompany = "";
        var fakepaybank = "";
        var fakebanknumber = "";
        var fakebankholder = "";
        var fakeremark = "";
        	
        for(var i = 0; i < $(".fakedate").length; i ++){
            	fakedate += $(".fakedate")[i].value + ",";
            	fakepaycontent += $(".fakepaycontent")[i].value + ",";
            	fakepayprice += $(".fakepayprice")[i].value + "/";
            	
            	fakepaycompany += $(".fakepaycompany")[i].value + ",";
            	fakepaybank += $(".fakepaybank")[i].value + ",";
            	fakebanknumber += $(".fakebanknumber")[i].value + ",";
            	fakebankholder += $(".fakebankholder")[i].value + ",";
            	fakeremark += $(".fakeremark")[i].value + ",";
            	
            }
            fakedate = fakedate.substr(0, fakedate.length - 1);
            fakepaycontent = fakepaycontent.substr(0, fakepaycontent.length - 1);
            fakepayprice = fakepayprice.substr(0, fakepayprice.length - 1);
            
            fakepaycompany = fakepaycompany.substr(0, fakepaycompany.length - 1);
            fakepaybank = fakepaybank.substr(0, fakepaybank.length - 1);
            fakebanknumber = fakebanknumber.substr(0, fakebanknumber.length - 1);
            fakebankholder = fakebankholder.substr(0, fakebankholder.length - 1);
            fakeremark = fakeremark.substr(0, fakeremark.length - 1);
            
            
            
            $("#paydate").val(fakedate);
            $("#paycontent").val(fakepaycontent);
            $("#payprice").val(fakepayprice);
            
            $("#paycompany").val(fakepaycompany);
            $("#paybank").val(fakepaybank);
            $("#banknumber").val(fakebanknumber);
            $("#bankholder").val(fakebankholder);
            $("#remark").val(fakeremark);
            if($(".fakeshare").is(":checked") == true){
            	$("#share").val("O");
            } else {
            	$("#share").val("X");
            }
            $("#summary").val($(".fakesummary").val());
            oEditors.getById["ir1"].exec("UPDATE_CONTENTS_FIELD", []);
            $("#details").val(document.getElementById("ir1").value);
            
            $("#insertform").submit();
	}
	var child = 0;
	var listno = 1;
	var checking = 0;
	$(".ccc").click(function(){
		$(this).parent().children().css({"color":"black","background-color":"white"});
		$(this).css({"color":"white","background-color":"#27334C"});
		child = $(this);
	});	
	$(".ccc2").click(function(){
		$(this).parent().children().css({"color":"black","background-color":"white"});
		$(this).css({"color":"white","background-color":"#27334C"});
		child = $(this);
	});	
	function addapproval(){
		checking = 0;
		$("#checktable tr td").each(function(){
			if($(this).html() == child.find(':nth-child(2)').html()){
				alert("중복하여 적용할 수 없습니다.");
				checking = 1;
			}
		});
		
		if((child.find(':nth-child(2)').html() + child.find(':nth-child(3)').html().substr((child.find(':nth-child(2)').html() + child.find(':nth-child(3)').html() ),0 ,3) ) == userName){
			alert("자기자신을 적용할 수 없습니다.");
			checking = 1;
		}
		
		if(checking == 0){
			child.css({"color":"black","background-color":"white"});
			
			$("#checktable").append(
					'<tr style="border: 1px solid #81848A;">' +
					'<td style="border-right: 1px solid #81848A; height:25px;">' + listno +'</td>' +
					'<td style="border-right: 1px solid #81848A;">'+'<select style="width:60px; height: 18px; font-size:12px;"><option value="결재">결재</option><option value="협조">협조</option></select>' + '</td>' +
					'<td style="border-right: 1px solid #81848A;">' + child.find(':nth-child(1)').html()+ '</td>' +
					'<td style="border-right: 1px solid #81848A;">' + child.find(':nth-child(3)').html()+ '</td>' +
					'<td style="border-right: 1px solid #81848A;">' + child.find(':nth-child(2)').html()+ '</td>' +
					'<td style="border-right: 1px solid #81848A;">' + '<img src="/opera/resources/icons/approvalImages/uparrow.png" class="changenumup">' + '<img src="/opera/resources/icons/approvalImages/downarrow.png" class="changenumdown">'+ '</td>' +
					'<td style="border-right: 1px solid #81848A;">' + '<button style="color: red; background-color: white;" class="deletelist">X</button>'+ '</td>' +
					'</tr>'
					);
			
			listno += 1;
		}
	}
	function addapproval2(){
		checking = 0;
		$("#checktable tr td").each(function(){
			if($(this).html() == child.find(':nth-child(2)').html()){
				alert("중복하여 적용할 수 없습니다.");
				checking = 1;
			}
		});
		if((child.find(':nth-child(2)').html() + child.find(':nth-child(3)').html().substr((child.find(':nth-child(2)').html() + child.find(':nth-child(3)').html() ),0 ,3) ) == userName){
			alert("자기자신을 적용할 수 없습니다.");
			checking = 1;
		}
		if(checking == 0){
			child.css({"color":"black","background-color":"white"});
			
			$("#checktable").append(
					'<tr style="border: 1px solid #81848A;">' +
					'<td style="border-right: 1px solid #81848A; height:25px;">' + listno +'</td>' +
					'<td style="border-right: 1px solid #81848A;">'+'<select style="width:60px; height: 18px; font-size:12px;"><option value="협조">협조</option><option value="결재">결재</option></select>' + '</td>' +
					'<td style="border-right: 1px solid #81848A;">' + child.find(':nth-child(1)').html()+ '</td>' +
					'<td style="border-right: 1px solid #81848A;">' + child.find(':nth-child(3)').html()+ '</td>' +
					'<td style="border-right: 1px solid #81848A;">' + child.find(':nth-child(2)').html()+ '</td>' +
					'<td style="border-right: 1px solid #81848A;">' + '<img src="/opera/resources/icons/approvalImages/uparrow.png" class="changenumup">' + '<img src="/opera/resources/icons/approvalImages/downarrow.png" class="changenumdown">'+ '</td>' +
					'<td style="border-right: 1px solid #81848A;">' + '<button style="color: red; background-color: white;" class="deletelist">X</button>' + '</td>' +
					'</tr>'
					);
			listno += 1;
		}
	}
	$(document).on("click", ".deletelist",function(e){
		var changenum = 0;
		var number = 1;
		$(this).parent().parent().remove();
		$("#checktable tr td:nth-child(1)").each(function(index, ea){
			if(this.innerHTML == 'No'){
				
			} else {
				this.innerHTML = number;
				number += 1;	
			}
		});
		listno = number;
	});
	$(document).on("click", ".changenumup",function(e){
		var $tr = $(this).closest('tr');
		$tr.prev().before($tr);
		var number1 = 1;
		
		$("#checktable tr td:nth-child(1)").each(function(index, ea){
			if(this.innerHTML == 'No'){
				
			} else {
				this.innerHTML = number1;
				number1 += 1;
			}
		});
		listno = number1;
	});
	
	$(document).on("click", ".changenumdown",function(e){
		var $tr = $(this).closest('tr');
		$tr.next().after($tr);
		var number1 = 1;
		$("#checktable tr td:nth-child(1)").each(function(index, ea){
			if(this.innerHTML == 'No'){
				
			} else {
				this.innerHTML = number1;
				number1 += 1;	
			}
		});
		listno = number1;
	});
	var check22 = 0;
	//팝업 Close 기능
    function close_modal1(flag) {
         $('#myModal').fadeOut(300);
         $("#checktable").empty();
         $("#checktable").append(
        		 '<tr style="font-size: 10px; border: 1px solid #81848A;">' +
					'<td style="border-right: 1px solid #81848A; width:30px; height:25px;">No</td>'+
					'<td style="border-right: 1px solid #81848A; width:80px; height:25px;">결재/협조</td>'+
					'<td style="border-right: 1px solid #81848A; width:80px; height:25px;">부서</td>'+
					'<td style="border-right: 1px solid #81848A; width:70px; height:25px;">직위</td>'+
					'<td style="border-right: 1px solid #81848A; width:70px; height:25px;">성명</td>'+
					'<td style="border-right: 1px solid #81848A; width:70px; height:25px;">순서</td>'+
					'<td style="border-right: 1px solid #81848A; width:30px; height:25x;">삭제</td>'+
				'</tr>'
 		);
 		listno = 1;
    };
    jQuery.ajaxSettings.traditional = true;
    var value = 1 * 1;
    var dept = "";
    function open_modal1(flag) {
		if(check22 == 0){
			$("#checktable").empty();
	         $("#checktable").append(
	        		 '<tr style="font-size: 10px; border: 1px solid #81848A;">' +
						'<td style="border-right: 1px solid #81848A; width:30px; height:25px;">No</td>'+
						'<td style="border-right: 1px solid #81848A; width:80px; height:25px;">결재/협조</td>'+
						'<td style="border-right: 1px solid #81848A; width:80px; height:25px;">부서</td>'+
						'<td style="border-right: 1px solid #81848A; width:70px; height:25px;">직위</td>'+
						'<td style="border-right: 1px solid #81848A; width:70px; height:25px;">성명</td>'+
						'<td style="border-right: 1px solid #81848A; width:70px; height:25px;">순서</td>'+
						'<td style="border-right: 1px solid #81848A; width:30px; height:25x;">삭제</td>'+
					'</tr>'
	 		);
	         check22 = 1;
		}
    	value = 1;
    	dept = "";
    	$("#modal1 tbody").empty();
    	$("#modal2 tbody").empty();
    	$("#modal3 tbody").empty();
    	$("#modal1 tfoot tr td").empty();
    	$("#modal2 tfoot tr td").empty();
    	$("#modal3 tfoot tr td").empty();
        $('#myModal').fadeIn(300);
         $.ajax({
        	type:"POST",
        	url: "changeEmpList.app",
        	data: {
        		"dept" : dept,
        		"pageNum" : value
        	},
        	success: function(data){
        		
        		for(var i = 0; i < data.length - 1; i++){
    			$("#modal1 tbody").append('<tr style=" text-align: center;" class="ccc"><td style="width:100px;height: 30px; border: 1px solid #81848A;">'+data[i].empDept+'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empName +'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empJobName +'</td></tr>')
    			} 
        		if(value - 1 == 0){
        			$("#modal1 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'"><</button>&nbsp;');
        		} else{
        			$("#modal1 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value - 1) +'"><</button>&nbsp;');
        		}
        		
        		for(var i = 1; i <= data[data.length - 1].maxPage; i ++){
        			$("#modal1 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ i +'">' + i + '</button>&nbsp;');
        		}
        		if(value == data[data.length - 1].maxPage){
        			$("#modal1 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'">></button>&nbsp;');
        		} else{
        			$("#modal1 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value + 1) +'">></button>&nbsp;');
        		}
        	}
         });
    };
    var checker222 = "";
    $(document).on("click", ".deptList", function(){
    	if(this.innerHTML == "전체부서" || this.innerHTML == "검색"){
    		dept = "";
    	} else {
        	dept = this.innerHTML;
    	}
    	
    	var empName = "";
    	document.getElementById("search1").value = "";
		document.getElementById("search2").value = "";
		document.getElementById("search3").value = "";
    	
    	if(search1 == ""){
    		if(search2 == ""){
    			empName = search3;
    		} else {
    			empName = search2;
    		}
    	} else {
    		empName = search1;
    	}
    	if(checker == empName){
    		empName = "";
    	}
    	checker = empName;
    	value = 1;
    	$("#modal1 tbody").empty();
    	$("#modal2 tbody").empty();
    	$("#modal3 tbody").empty();
    	$("#modal1 tfoot tr td").empty();
    	$("#modal2 tfoot tr td").empty();
    	$("#modal3 tfoot tr td").empty();
         $.ajax({
        	type:"POST",
        	url: "changeEmpList.app",
        	data: {
        		"dept" : dept,
        		"pageNum" : value,
        		"empName": empName
        	},
        	success: function(data){
        		for(var i = 0; i < data.length - 1; i++){
    			$("#modal1 tbody").append('<tr style=" text-align: center;" class="ccc"><td style="width:100px;height: 30px; border: 1px solid #81848A;">'+data[i].empDept+'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empName +'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empJobName +'</td></tr>')
    			$("#modal2 tbody").append('<tr style=" text-align: center;" class="ccc"><td style="width:100px;height: 30px; border: 1px solid #81848A;">'+data[i].empDept+'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empName +'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empJobName +'</td></tr>')
    			$("#modal3 tbody").append('<tr style=" text-align: center;" class="ccc"><td style="width:100px;height: 30px; border: 1px solid #81848A;">'+data[i].empDept+'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empName +'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empJobName +'</td></tr>')
    			} 
        		if(value - 1 == 0){
        			$("#modal1 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'"><</button>&nbsp;');
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'"><</button>&nbsp;');
        			$("#modal3 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'"><</button>&nbsp;');
        		} else{
        			$("#modal1 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value - 1) +'"><</button>&nbsp;');
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value - 1) +'"><</button>&nbsp;');
        			$("#modal3 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value - 1) +'"><</button>&nbsp;');
        		}
        		for(var i = 1; i <= data[data.length - 1].maxPage; i ++){
        			$("#modal1 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ i +'">' + i + '</button>&nbsp;');
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ i +'">' + i + '</button>&nbsp;');
        			$("#modal3 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ i +'">' + i + '</button>&nbsp;');
        		}
        		if(value == data[data.length - 1].maxPage){
        			$("#modal1 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'">></button>&nbsp;');
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'">></button>&nbsp;');
        			$("#modal3 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'">></button>&nbsp;');
        		} else{
        			$("#modal1 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value + 1) +'">></button>&nbsp;');
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value + 1) +'">></button>&nbsp;');
        			$("#modal3 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value + 1) +'">></button>&nbsp;');
        		}
        	}
         });
    })
    $(document).on("click", ".aaa", function(){
    	value = this.value * 1;
    	$("#modal1 tbody").empty();
    	$("#modal1 tfoot tr td").empty();
    	$("#modal2 tbody").empty();
    	$("#modal2 tfoot tr td").empty();
    	$("#modal3 tbody").empty();
    	$("#modal3 tfoot tr td").empty();
         $.ajax({
        	type:"POST",
        	url: "changeEmpList.app",
        	data: {
        		"dept" : dept,
        		"pageNum" : value
        	},
        	success: function(data){
        		for(var i = 0; i < data.length - 1; i++){
    			$("#modal1 tbody").append('<tr style=" text-align: center;" class="ccc"><td style="width:100px;height: 30px; border: 1px solid #81848A;">'+data[i].empDept+'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empName +'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empJobName +'</td></tr>')
    			$("#modal2 tbody").append('<tr style=" text-align: center;" class="ccc"><td style="width:100px;height: 30px; border: 1px solid #81848A;">'+data[i].empDept+'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empName +'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empJobName +'</td></tr>')
    			$("#modal3 tbody").append('<tr style=" text-align: center;" class="ccc"><td style="width:100px;height: 30px; border: 1px solid #81848A;">'+data[i].empDept+'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empName +'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empJobName +'</td></tr>')
    			} 
        		if(value - 1 == 0){
        			$("#modal1 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'"><</button>&nbsp;');
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'"><</button>&nbsp;');
        			$("#modal3 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'"><</button>&nbsp;');
        		} else{
        			$("#modal1 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value - 1) +'"><</button>&nbsp;');
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value - 1) +'"><</button>&nbsp;');
        			$("#modal3 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value - 1) +'"><</button>&nbsp;');
        		}
        		
        		for(var i = 1; i <= data[data.length - 1].maxPage; i ++){
        			$("#modal1 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ i +'">' + i + '</button>&nbsp;');
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ i +'">' + i + '</button>&nbsp;');
        			$("#modal3 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ i +'">' + i + '</button>&nbsp;');
        		}
        		if(value == data[data.length - 1].maxPage){
        			$("#modal1 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'">></button>&nbsp;');
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'">></button>&nbsp;');
        			$("#modal3 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'">></button>&nbsp;');
        		} else{
        			$("#modal1 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value + 1) +'">></button>&nbsp;');
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value + 1) +'">></button>&nbsp;');
        			$("#modal3 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value + 1) +'">></button>&nbsp;');
        		}
        	}
         });
         
    });
    
    function open_modal2(flag) {
        $('#myModal2').fadeIn(300);
        value = 1;
    	dept = "";
    	$("#modal1 tbody").empty();
    	$("#modal2 tbody").empty();
    	$("#modal3 tbody").empty();
    	
    	$("#modal1 tfoot tr td").empty();
    	$("#modal2 tfoot tr td").empty();
    	$("#modal3 tfoot tr td").empty();
    	
         $.ajax({
        	type:"POST",
        	url: "changeEmpList.app",
        	data: {
        		"dept" : dept,
        		"pageNum" : value
        	},
        	success: function(data){
        		for(var i = 0; i < data.length - 1; i++){
    			$("#modal2 tbody").append('<tr style=" text-align: center;" class="ccc"><td style="width:100px;height: 30px; border: 1px solid #81848A;">'+data[i].empDept+'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empName +'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empJobName +'</td></tr>')
    			} 
        		if(value - 1 == 0){
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'"><</button>&nbsp;');
        		} else{
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value - 1) +'"><</button>&nbsp;');
        		}
        		
        		for(var i = 1; i <= data[data.length - 1].maxPage; i ++){
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ i +'">' + i + '</button>&nbsp;');
        		}
        		if(value == data[data.length - 1].maxPage){
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'">></button>&nbsp;');
        		} else{
        			$("#modal2 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value + 1) +'">></button>&nbsp;');
        		}
        	}
         });
   };
    function add(){
        	var appvonum = 2;
        	var arr = 3;
        	var chec = 1;
        	var appvonum2 = 1;
        	var arr2 = 2;
        	var c = 1;
        	var totals = "";
        	var approvals = "";
        	var cooperators = "";
        	for(var i = 0;i < 5; i ++){
        		$("#appline2 tr td:nth-child("+(i+1)+")").text("");
    			$("#appline2 tr th:nth-child("+(i+2)+")").text("");
    			$("#appline tr td:nth-child("+(i+2)+")").text("");
    			$("#appline tr th:nth-child("+(i+3)+")").text("");
        	}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
        	$("#checktable tr").each(function(){
        		var $check = $(this);
        		var cc = $check.find("option:selected");
        		if(cc.text() == '협조'){
        			$("#appline2 tr td:nth-child("+appvonum2+")").text($("#checktable tr:nth-child("+ chec + ") td:nth-child(5)").text());
        			$("#appline2 tr th:nth-child("+arr2+")").text($("#checktable tr:nth-child("+ chec + ") td:nth-child(4)").text());
        			arr2 += 1;
        			appvonum2 += 1;
        			totals += $("#checktable tr:nth-child("+ chec + ") td:nth-child(5)").text() + "협조,";
        			cooperators += $("#checktable tr:nth-child("+ chec + ") td:nth-child(5)").text() + ",";
        		} else if(cc.text() == '결재'){
        			$("#appline tr td:nth-child("+appvonum+")").text($("#checktable tr:nth-child("+ chec + ") td:nth-child(5)").text());
        			$("#appline tr th:nth-child("+arr+")").text($("#checktable tr:nth-child("+ chec + ") td:nth-child(4)").text());
        			appvonum += 1;
        			arr += 1;
        			totals += $("#checktable tr:nth-child("+ chec + ") td:nth-child(5)").text() + "결재,";
        			approvals += $("#checktable tr:nth-child("+ chec + ") td:nth-child(5)").text() + ",";
        		} else {
        			
        		}
        		chec += 1;
        	});
        	totals = totals.substr(0, totals.length-1);
        	approvals = approvals.substr(0, approvals.length-1);
        	cooperators = cooperators.substr(0, cooperators.length-1);
        	
        	$("#approvers").val(approvals);
        	$("#cooperators").val(cooperators);
        	$("#totals").val(totals);
        	$('#myModal').fadeOut(300);
    }
    
    
	</script>

	<!-- api -->
	<script type="text/javascript">
           var oEditors = [];
           nhn.husky.EZCreator.createInIFrame({
            oAppRef: oEditors,
            elPlaceHolder: "ir1",
            sSkinURI: "/opera/resources/se2/SmartEditor2Skin.html",
            //se2폴더 경로(프로젝트안에 넣어놓는게 좋음)
            fCreator: "createSEditor2"
           });
    </script>
    <script>
	    function submitContents(elClickedObj) {
		    // 에디터의 내용이 textarea에 적용된다.
		    oEditors.getById["ir1"].exec("UPDATE_CONTENTS_FIELD", []);
		    
		    // 에디터의 내용에 대한 값 검증은 이곳에서
		    // document.getElementById("ir1").value를 이용해서 처리한다.
		    
		    try {
		   		elClickedObj.form.submit();
		   		
		    } catch(e) {
		    	
		   	}
	    }
	</script>
	
	<!-- end api -->
	
	<script>
		$(document).ready(function() {
			var flag = true;
			$("#spread").click(function() {
				if (!flag) {
					flag = true;
					$("#img1").css("transform", "rotate(0deg)");
					$("#ddta1").slideDown(500);
				} else {
					flag = false;
					$("#img1").css("transform", "rotate(270deg)");
					$("#ddta1").slideUp(500);

				}
			});

			var flag2 = true;
			$("#spread2").click(function() {
				if (!flag2) {
					flag2 = true;
					$("#img2").css("transform", "rotate(0deg)");
					$("#ddta2").slideDown(500);
				} else {
					flag2 = false;
					$("#img2").css("transform", "rotate(270deg)");
					$("#ddta2").slideUp(500);

				}
			});
			
			
			var flag3 = true;
			$("#spread3").click(function() {
				if (!flag3) {
					flag3 = true;
					$("#img3").css("transform", "rotate(0deg)");
					$("#ddta3").slideDown(500);
				} else {
					flag3 = false;
					$("#img3").css("transform", "rotate(270deg)");
					$("#ddta3").slideUp(500);

				}
			});
			
			
			var flag4 = true;
			$("#spread4").click(function() {
				if (!flag4) {
					flag4 = true;
					$("#img4").css("transform", "rotate(0deg)");
					$("#ddta4").slideDown(500);
				} else {
					flag4 = false;
					$("#img4").css("transform", "rotate(270deg)");
					$("#ddta4").slideUp(500);

				}
			});
			var flag5 = true;
			$("#spread5").click(function() {
				if (!flag5) {
					flag5 = true;
					$("#img5").css("transform", "rotate(0deg)");
					$("#ddta5").slideDown(500);
				} else {
					flag5 = false;
					$("#img5").css("transform", "rotate(270deg)");
					$("#ddta5").slideUp(500);

				}
			});
			var flag6 = true;
			$("#spread6").click(function() {
				if (!flag6) {
					flag6 = true;
					$("#img6").css("transform", "rotate(0deg)");
					$("#ddta6").slideDown(500);
				} else {
					flag6 = false;
					$("#img6").css("transform", "rotate(270deg)");
					$("#ddta6").slideUp(500);

				}
			});
			var flag7 = true;
			$("#spread7").click(function() {
				if (!flag7) {
					flag7 = true;
					$("#img7").css("transform", "rotate(0deg)");
					$("#ddta7").slideDown(500);
				} else {
					flag7 = false;
					$("#img7").css("transform", "rotate(270deg)");
					$("#ddta7").slideUp(500);

				}
			});
			var flag8 = true;
			$("#spread8").click(function() {
				if (!flag8) {
					flag8 = true;
					$("#img8").css("transform", "rotate(0deg)");
					$("#ddta8").slideDown(500);
				} else {
					flag8 = false;
					$("#img8").css("transform", "rotate(270deg)");
					$("#ddta8").slideUp(500);

				}
			});
			var flag9 = true;
			$("#spread9").click(function() {
				if (!flag9) {
					flag9 = true;
					$("#img9").css("transform", "rotate(0deg)");
					$("#ddta9").slideDown(500);
				} else {
					flag9 = false;
					$("#img9").css("transform", "rotate(270deg)");
					$("#ddta9").slideUp(500);

				}
			});
			var flag10 = true;
			$("#spread10").click(function() {
				if (!flag10) {
					flag10 = true;
					$("#img10").css("transform", "rotate(0deg)");
					$("#ddta10").slideDown(500);
				} else {
					flag10 = false;
					$("#img10").css("transform", "rotate(270deg)");
					$("#ddta10").slideUp(500);
				}
			});
			var flag11 = true;
			$("#spread11").click(function() {
				if (!flag11) {
					flag11 = true;
					$("#img11").css("transform", "rotate(0deg)");
					$("#ddta11").slideDown(500);
				} else {
					flag11 = false;
					$("#img11").css("transform", "rotate(270deg)");
					$("#ddta11").slideUp(500);
				}
			});
			var flag12 = true;
			$("#spread12").click(function() {
				if (!flag12) {
					flag12 = true;
					$("#img12").css("transform", "rotate(0deg)");
					$("#ddta12").slideDown(500);
				} else {
					flag12 = false;
					$("#img12").css("transform", "rotate(270deg)");
					$("#ddta12").slideUp(500);
				}
			});
		});

		function approvalDocuList() {
			location.href = "selectDocuList.me";
		}
	</script>


	<script>
	$(document).on("keyup", ".fakepayprice", function(){
		return this.value=this.value.replace(/[^0-9]/g,'');
	});
	
	$(document).on("keyup keydown", ".fakepayprice", function(){
		$(this).val(comma(this.value));
		var num = 0;
		for(var i = 0; i < $(".fakepayprice").length; i++){
			var temp = uncomma($(".fakepayprice")[i].value) * 1;
			num += temp;
		}
		$(".fakedeposit").val(comma(num) + "원");
		$("#deposit").val(num);
	});
	function comma(str) {

	    str = String(str);

	    return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');

	}
	function uncomma(str) {

	    str = String(str);

	    return str.replace(/[^\d]+/g, '');

	}
	var checking = 0;
	var child = 0;
	var receiverno = 1;
	$(document).on("click",".ccc",function(){
		$(this).parent().children().css({"color":"black","background-color":"white"});
		$(this).css({"color":"white","background-color":"#27334C"});
		child = $(this);
	});
	function close_modal2(flag) {
        $('#myModal2').fadeOut(300);
    };
    var userName = "${loginUser.empName}";
    function addreceiver(){
    	checking = 0;
    	$("#receivertable tr td").each(function(){
			if($(this).html() == (child.find(':nth-child(2)').html() + child.find(':nth-child(3)').html() )){
				alert("중복하여 적용할 수 없습니다.");
				checking = 1;
			}
		});
    	
    	if((child.find(':nth-child(2)').html() + child.find(':nth-child(3)').html().substr((child.find(':nth-child(2)').html() + child.find(':nth-child(3)').html() ),0 ,3) ) == userName){
			alert("자기자신을 적용할 수 없습니다.");
			checking = 1;
		}
		if(checking == 0){
			child.css({"color":"black","background-color":"white"});
			
			$("#receivertable").append(
					'<tr style="border: 1px solid #81848A;">' +
					'<td style="border-right: 1px solid #81848A; width:30px; height:25px;">' + receiverno +'</td>' +
					'<td style="border-right: 1px solid #81848A;">'+ child.find(':nth-child(2)').html()+child.find(':nth-child(3)').html() + '</td>' +
					'</tr>'
					);
			
			receiverno += 1;
		}
    }
    var check = 3;
    var prolist = "";
    function add2(){
    	prolist = "";
    	check = 3;
    	$("#listpro").empty();
    		//$("#receivertable tr td:nth-child(2)")
    	$("#receivertable tr").each(function(index){
    		if(index == 0){
    			
    		}else if(index > 0){
    			$("#listpro").append(
                		'<div class="prolist">' +
            				'<label>'+$("#receivertable tr:nth-child("+ check +") td:nth-child(2)").text()+'</label>' +
            				'<button class="removebtn">X</button>' +
            			'</div>'	
                );
    			prolist += $("#receivertable tr:nth-child("+ check +") td:nth-child(2)").text() + ",";
    			check ++;
    		}
    	});
    	$("#receiver").val(prolist.substr(0,prolist.length-1));
    	$('#myModal2').fadeOut(300);
    }
    $(document).on("click",".removebtn",function(){
    	
    	$(this).parent().remove();
    	for(var i = 3; i < check; i ++){
    		if($(this).parent().text().substr(0,3) == $("#receivertable tr:nth-child("+ i +") td:nth-child(2)").text().substr(0,3)){
    			$("#receivertable tr:nth-child("+ i +")").remove();
    			receiverno -= 1;
    		}
    	}
    	for(var i = 0; i < receiverno; i ++){
    		$("#receivertable tr:nth-child("+(i+3)+")").find("td:nth-child(1)").text(i+1)
    	}
    	add2();
    });
    function addpaynumbermodal(){
    	$("#addpaymodal").fadeIn(300);
    }
    function close_modal3(){
    	$("#addpaymodal").fadeOut(300);
    }
    
    var checker111;
    function addclientmodal(da){
    	value = 1;
    	checker111 = da.parentNode.parentNode.parentNode;
    	$("#addclientmodal").fadeIn(300);
    	var empNo = ${loginUser.empNo};
    	var empName = "${loginUser.empName}";
    	$.ajax({
    		type: "POST",
    		url: "selectClientList.app",
    		data: {
    			"empNo": empNo,
    			"pageNum" : value,
    			"search": "",
    			"searchType": ""
    		},
    		success: function(data){
    			var number = data.length - 1;
    			$("#clientTable tbody").empty();
    			$("#clientTable tfoot tr td").empty();
				for(var i = 0 ; i < data.length - 1; i++){
					$("#clientTable tbody").append('<tr class="ccc4" >' +
	            			'<td style="border:1px solid #81848C; font-size: 10px; height: 25px;">' + (number - i) + '</td>' +
	            			'<td style="border:1px solid #81848C; font-size: 10px;">' + data[i].cliName +'</td>' +
	            			'<td style="border:1px solid #81848C; font-size: 10px;">' + data[i].cliAddress +'</td>' +
	            			'<td style="border:1px solid #81848C; font-size: 10px;">' + empName + '</td>' +
	            			'<td> <input type="hidden" value=' + data[i].cliNo + '> </td>' + 
	            		'</tr>'
	            	);
				}
				if(value - 1 == 0){
        			$("#clientTable tfoot tr td").append('<button class="qqq" style="background-color: white" value="'+ value +'"><</button>&nbsp;');
        		} else{
        			$("#clientTable tfoot tr td").append('<button class="qqq" style="background-color: white" value="'+ (value - 1) +'"><</button>&nbsp;');
        		}
        		for(var i = 1; i <= data[data.length - 1].maxPage; i ++){
        			$("#clientTable tfoot tr td").append('<button class="qqq" style="background-color: white" value="'+ i +'">' + i + '</button>&nbsp;');
        		}
        		if(value == data[data.length - 1].maxPage){
        			$("#clientTable tfoot tr td").append('<button class="qqq" style="background-color: white" value="'+ value +'">></button>&nbsp;');
        		} else{
        			$("#clientTable tfoot tr td").append('<button class="qqq" style="background-color: white" value="'+ (value + 1) +'">></button>&nbsp;');
        		}
    		}
    	});
    }
    
    function close_modal4(){
    	$("#addclientmodal").fadeOut(300);
    }
    function addclientlist(){
    	$("#addclientmodal").fadeOut(300);
    	$("#addclientlistmodal").fadeIn(300);
    }
    
    function close_modal5(){
    	$("#addclientlistmodal").fadeOut(300);
    }
    
    var no = 8;
    function addexpenditem(){
    	var tr = '<tr class="last"><td class="addtext2"><div><input type="checkbox" style="vertical-align: text-bottom;"></div></td><td class="addtext2" colspan="2">'
		+ '<div><input class="fakedate" type="date"style="width: 87%; padding-left: 10px; font-size: 10px;"></div></td><td class="addtext2"><select class="fakepaycontent" style="font-size: 10px; width: 95%; height: 20px;">'
		+ '<option>선택</option> <option>교통비</option> <option>도서구입</option> <option>식대</option> <option>주유비</option> <option>지급수수료</option> <option>복리후생비</option>'
		+ '</select></td> <td class="addtext2">	<div> <input class="fakepayprice" type="text" style="padding-left:2px; font-size: 10px; width: 80px;"> </div> </td> <td class="adddtext2"> <div> <input class="fakepaycompany" type="text"'
		+ 'style="width: 95px; vertical-align: bottom; font-size: 10px; padding-left:2px;"> <button onclick="addclientmodal(this);" style="width: 25px; border: 1px solid #27334C; font-size: 10px; height: 20px;">찾기</button><input type="hidden" style="width:90px;">'
		+ '</div> </td>	<td class="addtext2"><input class="fakepaybank" type="text" style="padding-left:2px; width: 85px; vertical-align: bottom; font-size: 10px;"></td> <td class="addtext2"><input class="fakebanknumber" type="text"'
		+ 'style="padding-left:2px; width: 85px; vertical-align: bottom; font-size: 10px;"></td> <td class="addtext2"><input class="fakebankholder" type="text" style="padding-left:2px; width:85px; vertical-align: bottom; font-size: 10px;"></td> <td class="addtext2"><input class="fakeremark" type="text" style="padding-left:2px; width: 85px; vertical-align: bottom; font-size: 10px;"></td></tr>'
    	
		//$(".last :last-child").after(tr);
		$(tr).insertAfter($("#asd tr:nth-child("+ no +")"));
		no++;
    }
    
    function deleteexpenditem(){

    }
    $(document).on("click", ".deleteexpenditem", function(){
    	var temp = no;
    	for(var i = 8; i <= no; i++){
    		
    		if($("#asd tr:nth-child("+ i +") td:first-child").find("input[type='checkbox']").is(":checked") == true){
    			$("#asd tr:nth-child("+ i +")").remove();
    			no --;
    			i -= 1;
    		}
    	}
    	
    });
 		// 오늘 날짜 넣기
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; // Jan is 0
        var yyyy = today.getFullYear();
 
        if(dd<10){
            dd = '0'+dd
        }
        if(mm<10){
            mm = '0'+mm
        }
 
        today = yyyy + '년 ' + mm + '월 ' + dd + "일";
    	$("#today").text(today);
	</script>
	




<!-- 다음 우편번호 API -->
<script>
    //본 예제에서는 도로명 주소 표기 방식에 대한 법령에 따라, 내려오는 데이터를 조합하여 올바른 주소를 구성하는 방법을 설명합니다.
    function sample4_execDaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 도로명 주소의 노출 규칙에 따라 주소를 표시한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var roadAddr = data.roadAddress; // 도로명 주소 변수
                var extraRoadAddr = ''; // 참고 항목 변수

                // 법정동명이 있을 경우 추가한다. (법정리는 제외)
                // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraRoadAddr += data.bname;
                }
                // 건물명이 있고, 공동주택일 경우 추가한다.
                if(data.buildingName !== '' && data.apartment === 'Y'){
                   extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                if(extraRoadAddr !== ''){
                    extraRoadAddr = ' (' + extraRoadAddr + ')';
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById('sample4_postcode').value = data.zonecode;
                document.getElementById("sample4_roadAddress").value = roadAddr;
                
                // 참고항목 문자열이 있을 경우 해당 필드에 넣는다.
                

                var guideTextBox = document.getElementById("guide");
                // 사용자가 '선택 안함'을 클릭한 경우, 예상 주소라는 표시를 해준다.
                
            }
        }).open();
    }
    function approvalDocuList(){
		location.href="selectDocuList.app";
	}
</script>

<script>
	var paylineChild;
	$(document).on("click", ".ccc3", function(){
		$(this).parent().children().css({"color":"black","background-color":"white"});
		$(this).css({"color":"white","background-color":"#27334C"});
		paylineChild = $(this);
	});
	
	function addMyApproval(){
		check22 = 1;
		var plNo = paylineChild.find(':nth-child(4)').text();
		$.ajax({
        	type:"POST",
        	url: "selectPersonalApprovalOne.app",
        	data: {
        		"plNo" : plNo
        	},
        	success: function(data){
        		listno = 1;
        		 $("#checktable").empty();
		         $("#checktable").append(
		        		 '<tr style="font-size: 10px; border: 1px solid #81848A;">' +
							'<td style="border-right: 1px solid #81848A; width:30px; height:25px;">No</td>'+
							'<td style="border-right: 1px solid #81848A; width:80px; height:25px;">결재/협조</td>'+
							'<td style="border-right: 1px solid #81848A; width:80px; height:25px;">부서</td>'+
							'<td style="border-right: 1px solid #81848A; width:70px; height:25px;">직위</td>'+
							'<td style="border-right: 1px solid #81848A; width:70px; height:25px;">성명</td>'+
							'<td style="border-right: 1px solid #81848A; width:70px; height:25px;">순서</td>'+
							'<td style="border-right: 1px solid #81848A; width:30px; height:25x;">삭제</td>'+
						'</tr>'
		 		);
        		for(var i = 0 ; i < data.length; i++){
        			if(data[i].loSort == '결재'){
	        		$("#checktable").append(
	    					'<tr style="border: 1px solid #81848A;">' +
	    					'<td style="border-right: 1px solid #81848A; height:25px;">' + listno +'</td>' +
	    					'<td style="border-right: 1px solid #81848A;">'+'<select style="width:60px; height: 18px; font-size:12px;"><option value="결재">결재</option><option value="협조">협조</option></select>' + '</td>' +
	    					'<td style="border-right: 1px solid #81848A;">' + data[i].empDeptName+ '</td>' +
	    					'<td style="border-right: 1px solid #81848A;">' + data[i].empJob+ '</td>' +
	    					'<td style="border-right: 1px solid #81848A;">' + data[i].empName+ '</td>' +
	    					'<td style="border-right: 1px solid #81848A;">' + '<img src="/opera/resources/icons/approvalImages/uparrow.png" class="changenumup">' + '<img src="/opera/resources/icons/approvalImages/downarrow.png" class="changenumdown">'+ '</td>' +
	    					'<td style="border-right: 1px solid #81848A;">' + '<button style="color: red; background-color: white;" class="deletelist">X</button>'+ '</td>' +
	    					'</tr>'
	    					);
        			} else if(data[i].loSort == '협조'){
    	        		$("#checktable").append(
    	    					'<tr style="border: 1px solid #81848A;">' +
    	    					'<td style="border-right: 1px solid #81848A; height:25px;">' + listno +'</td>' +
    	    					'<td style="border-right: 1px solid #81848A;">'+'<select style="width:60px; height: 18px; font-size:12px;"><option value="협조">협조</option><option value="결재">결재</option></select>' + '</td>' +
    	    					'<td style="border-right: 1px solid #81848A;">' + data[i].empDeptName+ '</td>' +
    	    					'<td style="border-right: 1px solid #81848A;">' + data[i].empJob+ '</td>' +
    	    					'<td style="border-right: 1px solid #81848A;">' + data[i].empName+ '</td>' +
    	    					'<td style="border-right: 1px solid #81848A;">' + '<img src="/opera/resources/icons/approvalImages/uparrow.png" class="changenumup">' + '<img src="/opera/resources/icons/approvalImages/downarrow.png" class="changenumdown">'+ '</td>' +
    	    					'<td style="border-right: 1px solid #81848A;">' + '<button style="color: red; background-color: white;" class="deletelist">X</button>'+ '</td>' +
    	    					'</tr>'
    	    					);
            			}
	    			listno += 1;
        		}
        		var appvonum = 2;
            	var arr = 3;
            	var chec = 1;
            	var appvonum2 = 1;
            	var arr2 = 2;
            	var c = 1;
            	var totals = "";
            	var approvals = "";
            	var cooperators = "";
            	for(var i = 0;i < 5; i ++){
            		$("#appline2 tr td:nth-child("+(i+1)+")").text("");
        			$("#appline2 tr th:nth-child("+(i+2)+")").text("");
        			$("#appline tr td:nth-child("+(i+2)+")").text("");
        			$("#appline tr th:nth-child("+(i+3)+")").text("");
            	}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
            	$("#checktable tr").each(function(){
            		var $check = $(this);
            		var cc = $check.find("option:selected");
            		if(cc.text() == '협조'){
            			$("#appline2 tr td:nth-child("+appvonum2+")").text($("#checktable tr:nth-child("+ chec + ") td:nth-child(5)").text());
            			$("#appline2 tr th:nth-child("+arr2+")").text($("#checktable tr:nth-child("+ chec + ") td:nth-child(4)").text());
            			arr2 += 1;
            			appvonum2 += 1;
            			totals += $("#checktable tr:nth-child("+ chec + ") td:nth-child(5)").text() + "협조,";
            			cooperators += $("#checktable tr:nth-child("+ chec + ") td:nth-child(5)").text() + ",";
            		} else if(cc.text() == '결재'){
            			$("#appline tr td:nth-child("+appvonum+")").text($("#checktable tr:nth-child("+ chec + ") td:nth-child(5)").text());
            			$("#appline tr th:nth-child("+arr+")").text($("#checktable tr:nth-child("+ chec + ") td:nth-child(4)").text());
            			appvonum += 1;
            			arr += 1;
            			totals += $("#checktable tr:nth-child("+ chec + ") td:nth-child(5)").text() + "결재,";
            			approvals += $("#checktable tr:nth-child("+ chec + ") td:nth-child(5)").text() + ",";
            		} else {
            			
            		}
            		chec += 1;
            	});
            	totals = totals.substr(0, totals.length-1);
            	approvals = approvals.substr(0, approvals.length-1);
            	cooperators = cooperators.substr(0, cooperators.length-1);
            	
            	$("#approvers").val(approvals);
            	$("#cooperators").val(cooperators);
            	$("#totals").val(totals);
            	$('#personalApprovalList').fadeOut(300);
            	
        	}
		});
	}
	
	function open_modal10(){
		$("#personalApprovalList").fadeIn(300);
		var empNo = ${loginUser.empNo};
		$.ajax({
        	type:"POST",
        	url: "selectPersonalApprovalList.app",
        	data: {
        		"empNo" : empNo
        	},
        	success: function(data){
        		var count = data.length;
        		$("#personalApprovalTable tbody").empty();
        		for(var i = 0; i < data.length; i ++){
        		$("#personalApprovalTable tbody").append('<tr class="ccc3" >'  +
            			'<td style="border:1px solid #81848C; font-size: 10px; height: 25px;">' + (count - i) + '</td>' +
            			'<td style="border:1px solid #81848C; font-size: 10px;">' + data[i].plName + '</td>' +
            			'<td style="border:1px solid #81848C; font-size: 10px;">' + data[i].empName + '</td>' +
            			'<td style="border:1px solid #81848C; font-size: 10px;">' + data[i].plNo + '</td>' +
            		'</tr>');
        		}
        	}
         }); 
	}
	function open_modal11(){
		$("#personalApprovalList").fadeOut(300);
		$("#addpersonalApprovalList").fadeIn(300);
		
		value = 1;
    	dept = "";
    	$("#modal1 tbody").empty();
    	$("#modal2 tbody").empty();
    	$("#modal3 tbody").empty();
    	$("#modal1 tfoot tr td").empty();
    	$("#modal3 tfoot tr td").empty();
    	$("#modal3 tfoot tr td").empty();
    	$.ajax({
        	type:"POST",
        	url: "changeEmpList.app",
        	data: {
        		"dept" : dept,
        		"pageNum" : value
        	},
        	success: function(data){
        		for(var i = 0; i < data.length - 1; i++){
    			$("#modal3 tbody").append('<tr style=" text-align: center;" class="ccc"><td style="width:90px;height: 30px; border: 1px solid #81848A;">'+data[i].empDept+'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empName +'</td><td style="width:90px; border: 1px solid #81848A;">'+ data[i].empJobName +'</td></tr>')
    			} 
        		if(value - 1 == 0){
        			$("#modal3 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'"><</button>&nbsp;');
        		} else{
        			$("#modal3 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value - 1) +'"><</button>&nbsp;');
        		}
        		
        		for(var i = 1; i <= data[data.length - 1].maxPage; i ++){
        			$("#modal3 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ i +'">' + i + '</button>&nbsp;');
        		}
        		if(value == data[data.length - 1].maxPage){
        			$("#modal3 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ value +'">></button>&nbsp;');
        		} else{
        			$("#modal3 tfoot tr td").append('<button class="aaa" style="background-color: white" value="'+ (value + 1) +'">></button>&nbsp;');
        		}
        	}
         });
	}
	var listno2 = 1;
	function addPersonalApprovalList1(){
		checking = 0;
		$("#personaltable tbody tr td").each(function(){
			if($(this).html() == child.find(':nth-child(2)').html()){
				alert("중복하여 적용할 수 없습니다.");
				checking = 1;
			}
		});
		if((child.find(':nth-child(2)').html() + child.find(':nth-child(3)').html().substr((child.find(':nth-child(2)').html() + child.find(':nth-child(3)').html() ),0 ,3) ) == userName){
			alert("자기자신을 적용할 수 없습니다.");
			checking = 1;
		}
		if(checking == 0){
			child.css({"color":"black","background-color":"white"});
			
			$("#personaltable tbody").append(
					'<tr style="border: 1px solid #81848A;">' +
					'<td style="border-right: 1px solid #81848A; height:25px;">' + listno2 +'</td>' +
					'<td style="border-right: 1px solid #81848A;">'+'<select style="width:60px; height: 18px; font-size:12px;"><option value="결재">결재</option><option value="협조">협조</option></select>' + '</td>' +
					'<td style="border-right: 1px solid #81848A;">' + child.find(':nth-child(1)').html()+ '</td>' +
					'<td style="border-right: 1px solid #81848A;">' + child.find(':nth-child(3)').html()+ '</td>' +
					'<td style="border-right: 1px solid #81848A;">' + child.find(':nth-child(2)').html()+ '</td>' +
					'<td style="border-right: 1px solid #81848A;">' + '<img src="/opera/resources/icons/approvalImages/uparrow.png" class="changenumup2">' + '<img src="/opera/resources/icons/approvalImages/downarrow.png" class="changenumdown2">'+ '</td>' +
					'<td style="border-right: 1px solid #81848A;">' + '<button style="color: red; background-color: white;" class="deletelist2">X</button>'+ '</td>' +
					'</tr>'
					);
			
			listno2 += 1;
			checker = 1;
		}
	}
	var checker = 0;
	function addPersonalApprovalList2(){
		checking = 0;
		$("#personaltable tbody tr td").each(function(){
			if($(this).html() == child.find(':nth-child(2)').html()){
				alert("중복하여 적용할 수 없습니다.");
				checking = 1;
			}
		});
		if((child.find(':nth-child(2)').html() + child.find(':nth-child(3)').html().substr((child.find(':nth-child(2)').html() + child.find(':nth-child(3)').html() ),0 ,3) ) == userName){
			alert("자기자신을 적용할 수 없습니다.");
			checking = 1;
		}
		if(checking == 0){
			child.css({"color":"black","background-color":"white"});
			
			$("#personaltable tbody").append(
					'<tr style="border: 1px solid #81848A;">' +
					'<td style="border-right: 1px solid #81848A; height:25px;">' + listno2 +'</td>' +
					'<td style="border-right: 1px solid #81848A;">'+'<select style="width:60px; height: 18px; font-size:12px;"><option value="협조">협조</option><option value="결재">결재</option></select>' + '</td>' +
					'<td style="border-right: 1px solid #81848A;">' + child.find(':nth-child(1)').html()+ '</td>' +
					'<td style="border-right: 1px solid #81848A;">' + child.find(':nth-child(3)').html()+ '</td>' +
					'<td style="border-right: 1px solid #81848A;">' + child.find(':nth-child(2)').html()+ '</td>' +
					'<td style="border-right: 1px solid #81848A;">' + '<img src="/opera/resources/icons/approvalImages/uparrow.png" class="changenumup2">' + '<img src="/opera/resources/icons/approvalImages/downarrow.png" class="changenumdown2">'+ '</td>' +
					'<td style="border-right: 1px solid #81848A;">' + '<button style="color: red; background-color: white;" class="deletelist2">X</button>' + '</td>' +
					'</tr>'
					);
			listno2 += 1;
			checker = 1;
		}
	}
	$(document).on("click", ".deletelist2",function(e){
		var changenum = 0;
		var number = 1;
		$(this).parent().parent().remove();
		$("#personaltable tbody tr td:nth-child(1)").each(function(index, ea){
			if(this.innerHTML == 'No'){
				
			} else {
				this.innerHTML = number;
				number += 1;	
			}
		});
		listno2 = number;
		add();
	});
	$(document).on("click", ".changenumup2",function(e){
		var $tr = $(this).closest('tr');
		$tr.prev().before($tr);
		var number1 = 1;
		
		$("#personaltable tbody tr td:nth-child(1)").each(function(index, ea){
			if(this.innerHTML == 'No'){
				
			} else {
				this.innerHTML = number1;
				number1 += 1;
			}
		});
		listno2 = number1;
	});
	
	$(document).on("click", ".changenumdown2",function(e){
		var $tr = $(this).closest('tr');
		$tr.next().after($tr);
		var number1 = 1;
		$("#personaltable tbody tr td:nth-child(1)").each(function(index, ea){
			if(this.innerHTML == 'No'){
				
			} else {
				this.innerHTML = number1;
				number1 += 1;	
			}
		});
		listno2 = number1;
	});
	var title = "";
	
	var f1 = "";
	var f2 = "";
	var f3 = "";
	var f4 = "";
	
	var clientName = ""; 
	var clientNumber1 = "";
	var clientNumber2 = "";
	var clientNumber3 = "";
	
	var clientAddressNumber = "";
	var clientAddress = "";
	var clientAddressDetail = "";
	
	var clientBankNumber = "";
	var clientBankName = "";
	var clientDepositName = "";
	
	var search1 = "";
	var search2 = "";
	var search3 = "";
	
	var searchClientContent = "";
	
	
	function print(){
		title = document.getElementById("personalApprovalTitle").value;
		f1 = document.getElementById("f1").value;
		f2 = document.getElementById("f2").value;
		f3 = document.getElementById("f3").value;
		f4 = document.getElementById("f4").value;
		
		clientName = document.getElementById("clientName").value; 
		clientNumber1 = document.getElementById("clientNumber1").value;
		clientNumber2 = document.getElementById("clientNumber2").value;
		clientNumber3 = document.getElementById("clientNumber3").value;
		
		clientAddressNumber = document.getElementById("sample4_postcode").value;
		clientAddress = document.getElementById("sample4_roadAddress").value;
		clientAddressDetail = document.getElementById("addressDetails").value;
		
		clientBankNumber = document.getElementById("clientBankNumber").value;
		clientBankName = document.getElementById("clientBankName").value;
		clientDepositName = document.getElementById("clientDepositName").value;
		
		search1 = document.getElementById("search1").value;
		search2 = document.getElementById("search2").value;
		search3 = document.getElementById("search3").value;
		
		searchClientContent = document.getElementById("searchClientContent").value;
		
	}
	var userName = "${loginUser.empName}";
	$(document).on("click",".add_modal11",function(){

		var loSort = "";
		var empName = "";
		var empJob = "";
		var check = 1;
		if(title == null || title == ""){
			alert("결재선명을 입력하세요.");
			return false;
		} else if(checker == 0){
			alert("결재인원을 추가하세요.");
			return false;
		} else if((child.find(':nth-child(2)').html() + child.find(':nth-child(3)').html().substr((child.find(':nth-child(2)').html() + child.find(':nth-child(3)').html() ),0 ,3) ) == userName){
			alert("자기자신을 적용할 수 없습니다.");
			return false;
		}
		else {
			$("#personaltable tbody tr").each(function(index){
				var $check = $(this);
	    		var cc = $check.find("option:selected");
	    		loSort += cc.text() + ",";
	    		empJob += $(this).find(":nth-child(4)").text() + ",";
				empName += $(this).find(":nth-child(5)").text() + ",";
			});
			
			loSort = loSort.substr(0, loSort.length - 1);
			empName = empName.substr(0, empName.length - 1);
			empJob = empJob.substr(0, empJob.length - 1);
			
 			$.ajax({
	        	type:"POST",
	        	url: "addPersonalApprovalList.app",
	        	data: {
	        		"loSort" : loSort,
	        		"empName" : empName,
	        		"empJob" : empJob,
	        		"title" : title
	        	}, success: function(){
	        		
	        	}
	        })
			$("#addpersonalApprovalList").fadeOut(300);
			alert("결재선이 추가되었습니다.");
			$("#personaltable tbody").empty();
			listno2 = 1;
			document.getElementById("personalApprovalTitle").value = "";
			return false;
		}
	});
	function close_modal10(){
		$("#personalApprovalList").fadeOut(300);
	}
	function close_modal11(){
		$("#addpersonalApprovalList").fadeOut(300);
	}
	
	function addProvision(){
		
		var proCardDiv = $("#addProvision tr:nth-child(2) td:nth-child(1)").find("option:selected").text();
		var proBankDiv = $("#addProvision tr:nth-child(2) td:nth-child(2)").find("option:selected").text()
		var proCardNumber = f1 + "-" +f2 + "-" +f3 + "-" + f4;
		
		$.ajax({
        	type:"POST",
        	url: "addProvision.app",
        	data: {
        		"proCardDiv" : proCardDiv,
        		"proBankDiv" : proBankDiv,
        		"proCardNumber" : proCardNumber
        	}, success: function(){
        		alert("지급번호가 등록되었습니다!");
        		$("#addpaymodal").fadeOut(300);
        		return false;
        	}
        });
		
		$.ajax({
			type:"POST",
			url: "selectProVisionList.app",
			data: {
				"proCardDiv" : $("#fakepaytype option:selected").text()
			},
			success: function(data){
				$("#fakepaynumber").empty();
				if(data.length != 0){
					for(var i = 0 ; i < data.length; i ++){
						$("#fakepaynumber").append('<option>' + data[i].proBankDiv +'&nbsp&nbsp' + data[i].proCardNumber + '</option>');	
					}
				} else {
					$("#fakepaynumber").append('<option>등록된 지급번호가 없습니다.</option>');	
				}
				
			}

		});
	}
	$(document).on("change", "#fakepaytype", function(){
		var proCardDiv = $(this).find("option:selected").text()
		
		$.ajax({
			type:"POST",
			url: "selectProVisionList.app",
			data: {
				"proCardDiv" : proCardDiv
			},
			success: function(data){
				$("#fakepaynumber").empty();
				if(data.length != 0){
					for(var i = 0 ; i < data.length; i ++){
						$("#fakepaynumber").append('<option>' + data[i].proBankDiv +'&nbsp&nbsp' + data[i].proCardNumber + '</option>');	
					}
				} else {
					$("#fakepaynumber").append('<option>등록된 지급번호가 없습니다.</option>');	
				}
				
			}
		});
	});
	
	function addclient(){
		
		var fullcNumber = clientNumber1 + "-" + clientNumber2 + "-" + clientNumber3;
		var fullAddress = "("+clientAddressNumber + ") " + clientAddress + " " + clientAddressDetail;
		
		var empNo = ${loginUser.empNo};
		$.ajax({
			type:"POST",
			url: "insertClient.app",
			data:{
				"cliName": clientName,
				"cliAddress": fullAddress,
				"empNo": empNo,
				"cliLinumber": fullcNumber,
				"cliBankNumber": clientBankNumber,
				"cliBankName": clientBankName,
				"cliDepositName": clientDepositName
			},
			success: function(data){
				alert("업체 등록이 완료되었습니다.");
				$("#addclientlistmodal").fadeOut(300);
				return false;
			}
		});
		
	}
	var clientChild;
	$(document).on("click", ".ccc4", function(){
		$(this).parent().children().css({"color":"black","background-color":"white"});
		$(this).css({"color":"white","background-color":"#27334C"});
		clientChild = $(this).find("input").val();
		
	});
	
	function selectClientOne(){
		if(clientChild != null){
			
			$.ajax({
				type: "POST",
				url: "selectClient.app",
				data: {
					"cliNo": clientChild
				},
				success: function(data){
					checker111.querySelector('.fakepaycompany').value = data.cliName;
					checker111.querySelector('.fakepaybank').value = data.cliBankName;
					checker111.querySelector('.fakebanknumber').value = data.cliBankNumber;
					checker111.querySelector('.fakebankholder').value = data.cliDepositName;
				}
			});
			
		}
		$("#addclientmodal").fadeOut(300);
	}
	
	$(document).on("change", "#fakeFiles", function(){
		$("#fileNametd").empty();
		$("#fileSizetd").empty();
		for(var i = 0; i < $("input[type=file]")[0].files.length; i++){
			
			$("#fileNametd").append('<div style="height: 20px;">' + $("input[type=file]")[0].files[i].name + '</div>');
			$("#fileSizetd").append('<div style="height: 20px;">' + ($("input[type=file]")[0].files[i].size / 1000) + 'Mb</div>');
			
		}
	 // $('input[type=file]')[0].files[0].name;
	 // $("#imgUpload")[0].files[0].type;
	 // $("#imgUpload")[0].files[0].size;
	});
	$(document).on("click", ".qqq", function(da){
    	value = this.value * 1;
    	//checker111 = da.parentNode.parentNode.parentNode;
    	var empNo = ${loginUser.empNo};
    	var empName = "${loginUser.empName}";
    	$.ajax({
    		type: "POST",
    		url: "selectClientList.app",
    		data: {
    			"empNo": empNo,
    			"pageNum" : value
    		},
    		success: function(data){
    			var number = data.length - 1;
    			$("#clientTable tbody").empty();
    			$("#clientTable tfoot tr td").empty();
				for(var i = 0 ; i < data.length - 1; i++){
					$("#clientTable tbody").append('<tr class="ccc4" >' +
	            			'<td style="border:1px solid #81848C; font-size: 10px; height: 25px;">' + (number - i) + '</td>' +
	            			'<td style="border:1px solid #81848C; font-size: 10px;">' + data[i].cliName +'</td>' +
	            			'<td style="border:1px solid #81848C; font-size: 10px;">' + data[i].cliAddress +'</td>' +
	            			'<td style="border:1px solid #81848C; font-size: 10px;">' + empName + '</td>' +
	            			'<td> <input type="hidden" value=' + data[i].cliNo + '> </td>' + 
	            		'</tr>'
	            	);
				}
				if(value - 1 == 0){
        			$("#clientTable tfoot tr td").append('<button class="qqq" style="background-color: white" value="'+ value +'"><</button>&nbsp;');
        		} else{
        			$("#clientTable tfoot tr td").append('<button class="qqq" style="background-color: white" value="'+ (value - 1) +'"><</button>&nbsp;');
        		}
        		for(var i = 1; i <= data[data.length - 1].maxPage; i ++){
        			$("#clientTable tfoot tr td").append('<button class="qqq" style="background-color: white" value="'+ i +'">' + i + '</button>&nbsp;');
        		}
        		if(value == data[data.length - 1].maxPage){
        			$("#clientTable tfoot tr td").append('<button class="qqq" style="background-color: white" value="'+ value +'">></button>&nbsp;');
        		} else{
        			$("#clientTable tfoot tr td").append('<button class="qqq" style="background-color: white" value="'+ (value + 1) +'">></button>&nbsp;');
        		}
    		}
    	});
	})
	
	$(document).on("click", "#searchClient", function(){
		value = 1;
    	var empNo = ${loginUser.empNo};
    	var empName = "${loginUser.empName}";
    	
    	console.log("client content : " + searchClientContent);
    	
    	console.log($("#searchClientType option:selected").text());
    	var searchType = $("#searchClientType option:selected").text();
    	$.ajax({
    		type: "POST",
    		url: "selectClientList.app",
    		data: {
    			"empNo": empNo,
    			"pageNum" : value,
    			"search": searchClientContent,
    			"searchType": searchType
    		},
    		success: function(data){
    			var number = data.length - 1;
    			$("#clientTable tbody").empty();
    			$("#clientTable tfoot tr td").empty();
				for(var i = 0 ; i < data.length - 1; i++){
					$("#clientTable tbody").append('<tr class="ccc4" >' +
	            			'<td style="border:1px solid #81848C; font-size: 10px; height: 25px;">' + (number - i) + '</td>' +
	            			'<td style="border:1px solid #81848C; font-size: 10px;">' + data[i].cliName +'</td>' +
	            			'<td style="border:1px solid #81848C; font-size: 10px;">' + data[i].cliAddress +'</td>' +
	            			'<td style="border:1px solid #81848C; font-size: 10px;">' + empName + '</td>' +
	            			'<td> <input type="hidden" value=' + data[i].cliNo + '> </td>' + 
	            		'</tr>'
	            	);
				}
				if(value - 1 == 0){
        			$("#clientTable tfoot tr td").append('<button class="qqq" style="background-color: white" value="'+ value +'"><</button>&nbsp;');
        		} else{
        			$("#clientTable tfoot tr td").append('<button class="qqq" style="background-color: white" value="'+ (value - 1) +'"><</button>&nbsp;');
        		}
        		for(var i = 1; i <= data[data.length - 1].maxPage; i ++){
        			$("#clientTable tfoot tr td").append('<button class="qqq" style="background-color: white" value="'+ i +'">' + i + '</button>&nbsp;');
        		}
        		if(value == data[data.length - 1].maxPage){
        			$("#clientTable tfoot tr td").append('<button class="qqq" style="background-color: white" value="'+ value +'">></button>&nbsp;');
        		} else{
        			$("#clientTable tfoot tr td").append('<button class="qqq" style="background-color: white" value="'+ (value + 1) +'">></button>&nbsp;');
        		}
    		}
    	});
    	document.getElementById("searchClientContent").value = "";
	});
</script>


</body>
</html>