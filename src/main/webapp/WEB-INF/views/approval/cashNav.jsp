<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<div
		style="display: inline-block; margin-left: 50px; margin-top: 50px; padding-top: 20px; background-color: #E9E9E9; width: 256px; height: 844px; position: fixed;">
		<div align="center">
			<div style="margin-bottom: 30px; display: inline-block;">
				<button class="mainbutton" onclick="approvalDocuList();">기안문 작성</button>
			</div>
			
			<div class="mainbarfont">
				<c:out value="${ing }" />
			</div>
			<div class="mainbarfont">
				<c:out value="${check }" />
			</div>
			<div class="mainbarfont2">
				<label class="ddtag1" style="font-size: 14px; font-weight: 600; padding-left:0px;">결재진행</label>
			</div>
			<div class="mainbarfont2" style="">
				<label class="ddtag1" style="font-size: 14px; font-weight: 600; padding-left:0px;">결재요청</label>
			</div>
		</div>
		<br>
		<br>
		<div style="float: left; padding-left: 30px;" class="mainbarfont3">
			<dl style="padding-bottom: 13px;">
				<dt style="color: #565656; font-weight: 600;">
					<button id="spread" style="margin-right:4px;">
						<img id="img1" src="/opera/resources/icons/approvalImages/arrow.png">&nbsp;&nbsp;
					</button>
					진행중인 문서
				</dt>
				<div id="ddta1">
					<dd class="ddtag1">전체</dd>
					<dd class="ddtag1">요청</dd>
					<dd class="ddtag1">진행</dd>
					<dd class="ddtag1">확인</dd>
				</div>
			</dl>
			<dl style="padding-bottom: 2px;">
				<dt style="color: #565656; font-weight: 600;">
					<button id="spread2">
						<img id="img2"
							src="/opera/resources/icons/approvalImages/arrow.png">
						&nbsp;&nbsp;
					</button>
					문서함
				</dt>
				<div id="ddta2">
					<dd class="ddtag1" style="width:80px;">전체문서</dd>
					<dd class="ddtag1" style="width:80px;">승인문서</dd>
					<dd class="ddtag1" style="width:80px;">반려문서</dd>
				</div>
			</dl>
			<dl>
				<dt style="color: #565656; font-weight: 600;">
					<dd class="ddtag1" style="width:90px; font-weight: 600; color:#565656; font-family: 'Gothic A1', sans-serif; font-size: 18px;">부서결재함</dd>
				</dt>
			</dl>
		</div>
	</div>
	<form method="post" action="approvalInProgress.app" id="approvalInProgress">
		<input type="hidden" id="status2" name="status">
		<input type="hidden" id="status3" name="status2">
	</form>
	<script>
	var content = "";
	$(document).on("click", ".ddtag1", function(){
		var a = this.innerHTML;
		console.log(a.slice(-3));
		if(a.slice(-2) == "문서"){
			console.log("문서입니다.");
			if(a.substr(0, 2) == "전체"){
				$("#status2").val("전체1");
			} else {
				$("#status2").val(a.substr(0, 2));
			}
		} else if(a.slice(-2) == "재함"){
			$("#status2").val("부서");
			console.log("부서결재함입니다..");
		} else if(a.slice(-1) == "일" || a.slice(-1) == "월"){
			console.log("일이나 월입니다.");
			if(a.slice(-1) == "월"){
				$("#status2").val("1개월");
			} else if(a.slice(-1) == "일"){
				if(a.slice(-2) == "당일"){
					$("#status2").val("당일");
				} else if(a.slice(-3) == "1주일"){
					$("#status2").val("1주일");
				} else if(a.slice(-3) == "2주일"){
					$("#status2").val("2주일");
				} else if(a.slice(-3) == "3주일"){
					$("#status2").val("3주일");
				}
			}
		} else if(a.slice(-2) == "검색"){
			$("#status2").val($("#stype option:selected").text());
			$("#status3").val(content);
			
			console.log($("#scontent"));
			console.log($("#status3").val());
		} else {
			$("#status2").val(a.slice(-2));
			console.log("진행중인문서입니다.");
		}
		$("#approvalInProgress").submit();
	});
	
	function print(){
		content = document.getElementById("scontent").value;
	}
	</script>
</body>
</html>