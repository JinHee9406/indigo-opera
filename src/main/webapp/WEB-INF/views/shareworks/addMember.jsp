<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
<link rel="stylesheet" href="/opera/resources/css/treemenuStyle.css">
<style>
		.mem_wrap {
			display: none;
			position: fixed;
			left: 0;
			top: 0;
			width: 100%;
			height: 100%;
			z-index: 10000
		}
		
		.dark_bg {
			position: absolute;
			width: 100%;
			height: 100%;
			background: #000;
			filter: alpha(opacity = 60);
			opacity: .6;
			-moz-opacity: .6
		}
		
		.mem_box {
			position: relative;
			top: 5%;
			width: 850px;
			height: 650px;
			background: #fff;
			margin: 0 auto;
			border-radius: 8px;
			background: #fff;
			border: 1px solid #ccc
		}
		
		.mem_box .close {
			position: absolute;
			right: 6px;
			top: 6px;
			width: 16px;
			height: 16px;
			cursor: pointer
		}
		
		#addWindowTitle{
			float:left; 
			font-weight: bold; 
			margin-left: 80px; 
			margin-top: 45px;
			color: #626262;
		}
		
		.closeBtn{
			float:right; 
			margin-left: 555px; 
			font-size: 1.25em; 
			font-weight: bold;
			cursor: pointer;
		}
		
		#windowIcon{
			width: 35px;
			height: 35px;
			padding-right: 10px;
			margin-bottom: -5px;
		}
		
		#addmember{
			width: 60px;
			height: 25px;
			background: #27334C;
			font-weight: bold;
			color: white;
			outline:none;	
		}
		
		#removemember{
			margin-top: 10px;
			width: 60px;
			height: 25px;
			font-weight: bold;
			outline:none;	
		}
		
		#searchSubmit{
			width: 60px;
			height: 25px;
			background: #27334C;
			font-weight: bold;
			color: white;
			outline:none;	
			margin-left: 10px;
		}
		
		#memSearch{
			clear:both; 
			width: 400px; 
			height: 35px; 
			border: 1px solid #515151;
			border-radius: 5px;
			background: #F8F8F8;
			padding-top: 5px;
		}
		
		#memTree{
			float: left; 
			width: 190px; 
			height: 400px; 
			border: 1px solid #515151; 
			border-radius: 5px;
			background: #F8F8F8;
			margin-top: 20px;
		}
		
		.conName{
			margin-top: 10px; 
			margin-left: 10px;
			font-size: 0.9em;
		}
		
		.memCon{
			width: 170px; 
			height:330px; 
			border: 1px solid black;
			margin: 10px; 
			background: white;
		}
		
		#memList{
			float: left; 
			width: 190px; 
			height: 400px; 
			border: 1px solid #515151; 
			background: #F8F8F8;
			border-radius: 5px;
			margin-top: 20px; 
			margin-left: 20px;
		}
		
		#memResult{
			float: left; 
			width: 190px; 
			height: 400px; 
			border: 1px solid #515151; 
			background: #F8F8F8;
			border-radius: 5px;
			margin-top: 20px; 
			margin-left: 5px;
		}
		
		#saveBtn{
			clear: both; 
			float: right; 
			margin-top: 15px;
			margin-right: 100px;
		}
		
		#saveBtn button{
			width: 80px; 
			height: 30px; 
			background: #27334C; 
			color: white; 
			font-weight: bold;
			border-radius: 5px;
			outline: none;
		}
		.memberEle{
			margin: 5px;
			margin-left: 15px;
			font-size: 0.8em;
		}
		
		li div {
		
			cursor: pointer;
		}
</style>
</head>
<body>
		<div class="mem_wrap" style="display: none;">
			<div class="dark_bg" onclick="jQuery('.mem_wrap').fadeOut('slow')"></div>
			<div class="mem_box">
				<div id="addWindowTitle">
					<div style="margin-bottom: 15px;">
						<div class="closeBtn" onclick="jQuery('.mem_wrap').fadeOut('slow')">X</div>
							<img id="windowIcon" src="/opera/resources/icons/meetingIcon-black.png"><a style="font-size: 1.6em;">직원목록</a>
					</div>
				</div>
				<br>
				<div style="margin-left: 85px;">
					<div id="memSearch">
						<input type="text" placeholder="직원 이름을 입력해주세요" style="border-radius: 5px; margin-left: 50px; height: 25px; width: 250px; "><button id="searchSubmit">검색</button>
					</div>
					<div id="memTree"><div class="conName">부서 명</div>
						<div class="memCon">
										
										<!-- 트리메뉴-->
							<!-- 마지막 리스트부분에 class="last",class="end" 넣어주세요 -->
							<div class="tree_box">
							    <div class="con">
							        <ul id="tree_menu" class="tree_menu" style="margin-top: -30px;">
							            <li class="depth_1"><strong onclick="loadMemberList('all')" style="cursor: pointer">전체직원</strong>
							                <ul class="depth_2" id="treeDept">
							                    <li>
							                        <a href="#none"><em>폴더</em>경영진</a>
							                        <ul class="depth_3" id="lanker">
							                        </ul>
							                    </li>
							                  	<li>
							                        <a href="#none" ><em>폴더</em>부서별직원</a>
							                        <ul class="depth_3" id="inner">
							                        </ul>
							                    </li>
							                    <li class="last">
							                        <a href="#none"><em>폴더</em>외부직원</a>
							                        <ul class="depth_3" id="outer">
							                        </ul>
							                    </li>
							                </ul>
							            </li>
							        </ul>
							    </div>
							</div>
							<!--//트리메뉴-->	
							</div>
					</div>
					<div id="memList"><div class="conName">직원 명</div>
						<div class="memCon" id="memberContainer">
						</div>
					</div>
					<div style="float: left; width: 60px; margin-top: 150px; margin-left: 5px;">
						<button id="addmember" onclick="addMemberBtn();">추가</button>
						<button id="addmember" onclick="addAllMemberBtn();" style="margin-top: 10px;">전부추가</button>
						<button id="removemember" onclick="removeMemberBtn();">제외</button>
					</div>
					<div id="memResult"><div class="conName">참여 직원</div>
						<div class="memCon" id="addedContainer">
							
						</div>
					</div>
				</div>
				<div id="saveBtn">
					<button onclick="saveUsers();">저장</button>
				</div>
			</div>
		</div>
		<script>
			var memberList = new Array();
			var memberNameList = new Array();
			
			function openWindow(){
				jQuery('.mem_wrap').fadeIn('slow');
				 tree_menu();
				 loadDeptList();
			}
		</script>
		<script>
			function addMemberBtn(){
				$("input:checkbox[class='checkEmp']:checked").each(function(index){
					var checked = $(this).val();
					var checkedSplit = checked.split(',');
					if(!memberList.includes(checkedSplit[0])){
						memberList.push(checkedSplit[0]);
						memberNameList.push(checkedSplit[1]+" "+checkedSplit[2]);
						$("#addedContainer").append("<div id="+checkedSplit[0]+" class='memberEle'>"+checkedSplit[1]+" "+checkedSplit[2]+"<input class='checkOut' type='checkbox' value="+checkedSplit[0]+","+checkedSplit[1]+","+checkedSplit[2]+"></div>");
					}
					
				}) 
				$("input:checkbox[class='checkEmp']:checked").prop("checked",false);
			}

			function addAllMemberBtn(){
				$("input:checkbox[class='checkEmp']").each(function(index){
					var checked = $(this).val();
					var checkedSplit = checked.split(',');
					if(!memberList.includes(checkedSplit[0])){
						memberList.push(checkedSplit[0]);
						memberNameList.push(checkedSplit[1]+" "+checkedSplit[2]);
						$("#addedContainer").append("<div id="+checkedSplit[0]+" class='memberEle'>"+checkedSplit[1]+" "+checkedSplit[2]+"<input class='checkOut' type='checkbox' value="+checkedSplit[0]+","+checkedSplit[1]+","+checkedSplit[2]+"></div>");
					}
					
				}) 
				$("input:checkbox[class='checkEmp']:checked").prop("checked",false);
			}

			function removeMemberBtn(){
				console.log(memberList);
				$("input:checkbox[class='checkOut']:checked").each(function(index){
					var checked = $(this).val();
					var checkedSplit = checked.split(',');
					if(memberList.includes(checkedSplit[0])){
						$("#"+checkedSplit[0]).remove();
						memberNameList.splice(memberNameList.indexOf(checkedSplit[1]+" "+checkedSplit[2]),1);
						memberList.splice(memberList.indexOf(checkedSplit[0]),1);
					}		
				}) 
			}

		</script>
		<script>
			function loadJoindMember(){
				$.ajax({
					url:"joindMemberList.sha",
					type:"post",
					data:{
						memberList : memberListFirst
						},
					success:function(data) {
						var memberLists = data.memList;
						var index = 0;
						$("#addedContainer").children().remove()
						memberLists.forEach(function(){
							if(memberLists[index].empNo != ${loginUser.empNo}){
								$("#addedContainer").append("<div id="+memberLists[index].empNo+" class='memberEle'>"+memberLists[index].empName+" "+memberLists[index].empJobName+"<input class='checkOut' type='checkbox' value="+memberLists[index].empNo+","+memberLists[index].empName+","+memberLists[index].empJobName+"></div>");
								memberList.push(memberLists[index].empNo);
								memberNameList.push(memberLists[index].empName+" "+memberLists[index].empJobName)
							}
							index += 1;

						})
					},
					error:function(){
						console.log("에러!");
					}
				});
			}

		
			function loadMemberList(value){
				var deptCode = value;
				$.ajax({
					url:"memberList.sha",
					type:"post",
					data:{
						deptCode : deptCode
						},
					success:function(data) {
						var memberLists = data.memberList;
						var index = 0;
						$("#memberContainer").children().remove()
						memberLists.forEach(function(){
							if(memberLists[index].empNo != ${loginUser.empNo}){
								$("#memberContainer").append("<div class='memberEle'>"+memberLists[index].empName+" "+memberLists[index].empJobName+"<input class='checkEmp' type='checkbox' value="+memberLists[index].empNo+","+memberLists[index].empName+","+memberLists[index].empJobName+"></div>");
							}
							index += 1;
						})
					},
					error:function(){
						console.log("에러!");
					}
				});
	
				
				return false;
			}
		</script>
		
		<script>
			function loadDeptList(){
		
				$.ajax({
					url:"deptList.sha",
					type:"post",
					data:{},
					success:function(data) {
						var deptList = data.deptList;
						var index = 0;
						deptList.forEach(function(){
							var deptCode = String(deptList[index].deptCode);
							switch(deptList[index].deptType){
							case "본부": 
								$("#lanker").append("<li><div onclick="+"loadMemberList('"+deptCode+"');"+">"+deptList[index].deptName+"</div></li>")
								break;
							case "내부": 
								$("#inner").append("<li><div onclick="+"loadMemberList('"+deptCode+"');"+">"+deptList[index].deptName+"</div></li>")
								break;
							case "외부": 
								$("#outer").append("<li><div onclick="+"loadMemberList('"+deptCode+"');"+">"+deptList[index].deptName+"</div></li>")
								break;
							}

							index += 1;
						})
						$("#lanker>li:last").attr("class", "end");
						$("#inner>li:last").attr("class", "end");
						$("#outer>li:last").attr("class", "end");
					},
					error:function(){
						console.log("에러!");
					}
				});

				
				return false;
			}

		</script>
		
		
		<script>
			function tree_menu() {
				console.log("트리펼치기");
				  $('ul.depth_2 >li > a').click(function(e) {

				    var temp_el = $(this).next('ul');
				    var depth_3 = $('.depth_3');
	
				    // 처음에 모두 슬라이드 업 시켜준다.
				    depth_3.slideUp(300);
				    // 클릭한 순간 모두 on(-)을 제거한다.// +가 나오도록
				    depth_3.parent().find('em').removeClass('on');
	
				    if (temp_el.is(':hidden')) {
				      temp_el.slideDown(300);
				      $(this).find('em').addClass('on').html('하위폴더 열림');
				    } else {
				      temp_el.slideUp(300);
				      $(this).find('em').removeClass('on').html('하위폴더 닫힘');
				    }
	
				    return false;
	
				  });
				}
			if ($('#tree_menu').is(':visible')) {
				 tree_menu();
			}
		</script>
		<script>
			function saveUsers(){
				var users = memberList.toString();
				var userName = memberNameList.toString();
				document.getElementById("workUserName").value = userName;
				document.getElementById("workUser").value = users;
				jQuery('.mem_wrap').fadeOut('slow');
			}
		</script>
</body>
</html>