<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
<style>
        #scheduleAside {
            float: left;
            padding-left: 60px;
            padding-top: 50px;
            width: 200px;
            height: 800px;
            background: #E5E5E5;
        }

        #AsideFirstBtn {
            margin-left: 25px;
            margin-top: 25px;
            padding-top: 10px;
            width: 150px;
            height: 30px;
            background: #27334C;
            color: white;
            text-align: center;
            cursor: pointer;
        }
        
        #nextBtn{
            cursor: pointer;
        }

        #prevBtn{
            cursor: pointer;
        }

        #deptBtn{
          cursor: pointer;
        }
       
        #persBtn{
           cursor: pointer;
        }

</style>
</head>
<body>
	 <div id="scheduleAside">
        <div id="AsideFirstBtn" onclick="location.href='knowledgeAdd.me'">일정 등록</div>

        <!-- 미니 캘린더 -->
        <div style="border-bottom: gray 1px solid;">
            <br>
            <div style="margin-left: 50px;">
                <input type="button" id="nextBtn" value="<">
                <span>2020.08</span>
                <input type="button" id="prevBtn" value=">">
            </div>
            <div style="margin-top:10px; margin-left: 25px; margin-bottom: 20px; width: 150px; height: 150px; background-color: white;">
                달력입니다^^

            </div>

        </div>

        <!-- 일정구분 -->
        <div style="margin-top: 25px; margin-left: 10px;">
            <div>
                <input type="checkbox" id="allCheck" style="margin-bottom: 20px;"> 전체캘린더
            </div>
            <!-- 부서캘린더 -->
            <div>
                <input type="checkbox" id="deptCheck"> 부서캘린더
                
                <input type="button" id="deptBtn" value="+">
                <input type="button" id="deptBtn" value="*">
                
            </div>
            <!-- 개인캘린더 -->
            <div>
                <input type="checkbox" id="persCheck"> 개인캘린더
             
                <input type="button" id="persBtn" value="+">
                <input type="button" id="persBtn" value="*">
               
            </div>

        </div>
     </div>
</body>
</html>