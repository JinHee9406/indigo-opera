<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
   #knowledgeAside{
      float:left; 
      padding-left:60px; 
      padding-top: 50px; 
      width: 220px; 
      height: 800px; 
      background: #E5E5E5;
   }
   
    /* #AsideFirstBtn{
      
      padding-top: 10px; 
      width:100%; 
      height: 30px;
      background: #627192; 
      color: white; 
      text-align: center;
      cursor: pointer;
   }  */
   
    .AsideBtn{
      padding-top: 10px; 
      width:94%; 
      height: 40px;
      background: #FFFFFF; 
      color: 57585E; 
      text-align: left;
      cursor: pointer;
      border: 1px solid lightgray;
      padding-top: 20px;      
      font-size: 1.25em;
      padding-left: 10px;
   } 
</style>
</head>
<body>
      <div id="knowledgeAside">
         <div id="AsideFirstBtn" class ="AsideBtn" style="background: #627192; color: white;">조직도</div>
         <div id="AsideFirstBtn2" class ="AsideBtn" onclick="location.href='psersonList.me'">인사목록</div>
         <div id="AsideFirstBtn3" class ="AsideBtn" onclick="location.href='wait.me'">승인대기목록</div>
         <div id="AsideFirstBtn4" class ="AsideBtn" onclick="location.href='insertForm.me'">직원등록</div>
         	<!-- <div style="margin-left: 25px;margin-top: 25px; width: 150px; height: 80%; background: white;">
         </div> -->
      </div>
      
      
      
      <script type="text/javascript">
      	$(".AsideBtn").click(function () {
      		$(this).parent().children().css({"background":"#FFFFFF","color":"#57585E"});
			$(this).css({"background":"#627192","color":"white"});
		});
      	
      	<%-- $(document).ready(function () {
      		<% String Typelist = request.getParameter("Typelist"); %>
			var boardType = '<%=Typelist%>';
			
			if(boardType == 'com'){
				$("#AsideFirstBtn3").parent().children().css({"background":"#FFFFFF","color":"#57585E"});
				$("#AsideFirstBtn3").css({"background":"#627192","color":"white"});
			}
			
			if(boardType == 'dept'){
				$("#AsideFirstBtn2").parent().children().css({"background":"#FFFFFF","color":"#57585E"});
				$("#AsideFirstBtn2").css({"background":"#627192","color":"white"});
			}
			
			if(boardType == 'not'){
				$("#AsideFirstBtn").parent().children().css({"background":"#FFFFFF","color":"#57585E"});
				$("#AsideFirstBtn").css({"background":"#627192","color":"white"});
			}
			
		}); --%>
      	
      	$(function () {
			var boardType = '${Typelist}';
			if(boardType == "me"){
				$("#AsideFirstBtn3").parent().children().css({"background":"#FFFFFF","color":"#57585E"});
				$("#AsideFirstBtn3").css({"background":"#627192","color":"white"});
			}
			
			if(boardType == "me"){
				$("#AsideFirstBtn2").parent().children().css({"background":"#FFFFFF","color":"#57585E"});
				$("#AsideFirstBtn2").css({"background":"#627192","color":"white"});
			}
			
			if(boardType == "me"){
				$("#AsideFirstBtn").parent().children().css({"background":"#FFFFFF","color":"#57585E"});
				$("#AsideFirstBtn").css({"background":"#627192","color":"white"});
			}
			
		});
      </script>
</body>
</html>
