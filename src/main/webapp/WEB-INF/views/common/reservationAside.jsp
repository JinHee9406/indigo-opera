<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	ul, ol, li{list-style:none;}
	#asideBack{
		z-index: 1;
	}
	#listMenu {
		width: 89%; 
		height:40px;
		padding-top: 20px; 
		padding-left: 10%;
	}
	
	#now {
		width: 89%; 
		height:40px;
		padding-top: 20px; 
		padding-left: 10%;
	}
	
	.on{
		background: #627192; 
		border: 1px solid #627192;
		color: white; 
	}
	
	.off{
		background: white; 
		border: 1px solid #E5E5E5;
		color: #27334C;
	}
	
	.tree_box { width:100%; font-size: 12px;}
	
	.tree_box .title { padding:5px 0 5px 19px ;background:#f8f8f9;}
	
	.tree_box .title strong {margin-right:12px;}
	
	.tree_menu {line-height:18px;}
	
	.tree_menu strong {font-weight:normal;}
	
	.tree_menu label input {vertical-align:-2px;}
	
	
	.tree_menu .depth_2 li {
		margin-top:-2px;
	}
	
	.tree_menu .depth_2 li a em {
		display:inline-block;
		width:31px;
		height:11px;
		font-size:0;
		line-height:0;
		vertical-align:middle;
	}
	
	.tree_menu .depth_2 li a em.on {
		background-position:0 100%;
	}
	
	.tree_menu li.last {
		background:none;
	}
	
	.tree_menu li.last {
		background:none;
	}
	
	.tree_menu .depth_3 {
		display:none;
		padding-left:23px;
	}
	
	.tree_menu .depth_3 li {
		margin:0;
		padding:3px 0 0 14px;
		line-height:1;
	}
	
	.tree_menu .depth_3 li a {
		display:block;
		padding-left:15px;
		margin-top: 15px;
		margin-bottom: 15px;
		font-size: 1.25em;
	}

	.tree_menu .depth_4 {
		display:none;
		padding-left:23px;
	}
	
	.tree_menu .depth_4 li {
		margin:0;
		padding:3px 0 0 14px;
		line-height:1;
	}
	
	.tree_menu .depth_4 li a {
		display:block;
		padding-left:15px;
		padding-left:15px;
		margin-top: 15px;
		margin-bottom: 15px;
		font-size: 1.25em;
	}
	
	.selected{
		color: #E61358;
		font-weight: bold;
	}
	
	li a {
		cursor: pointer;
	}
	
</style>
<link rel="stylesheet" href="/opera/resources/css/asideStyle.css">
<body>
	<div id="asideBack">
	<div class="tree_box">
		<div class="tree_menu">
			<ul class="depth_2">
				<li>
				<div class="on" id="now" onclick="tree_menu();">예약현황</div>
					<ul class="depth_3">
						<li><a id="reservationState" onclick="location.href='reservationMain.res'">회의실예약현황</a></li>
						<li><a id="reservationAdd" onclick="location.href='reservationAdd.res'">회의실예약신청</a></li>
					</ul>
				</li>
			</ul>
	<!-- 		<div class="reservationMenu" id="off" onclick="location.href='reservationAdd.res'">예약신청</div> -->
			<ul class="depth_2">
				<li>
					<div class="off" id="listMenu" onclick="tree_menu2();">예약내역</div>
					<ul class="depth_4">
						<li><a id="reservationLog" onclick="location.href='reservationList.res?loginUser=${loginUser.empNo}'">회의실예약이력</a></li>
					</ul>
				</li>
			</ul>
			
		</div>
	</div>

	</div>
	<script>
		function selectNow(){
			$("#now").attr('class','on');
			$("#listMenu").attr('class','off');
		}

		function selectList(){
			$("#now").attr('class','off');
			$("#listMenu").attr('class','on');
			}
	</script>
	<script>
		var open1 = false;
		var open2 = false;
		function tree_menu() {
				selectNow();
			    var temp_el = $(this).next('ul');
			    var depth_3 = $('.depth_3');
			    if(open1 == false) {
				    depth_3.slideDown(300);
				    open1 = true;
			    }
			    else{
				     depth_3.slideUp(300);
				     open1 = false;
			    }
			}
		function tree_menu2() {
			selectList();
		    var temp_el = $(this).next('ul');
		    var depth_4 = $('.depth_4');
		    if(open2 == false) {
			    depth_4.slideDown(300);
			    open2 = true;
		    }
		    else{
			     depth_4.slideUp(300);
			     open2 = false;
		    }
		}
	</script>

</body>
</html>