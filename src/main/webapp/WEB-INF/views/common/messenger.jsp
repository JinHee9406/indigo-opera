<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera Talk</title>
<style>
	body{
		overflow: hidden;
		padding: 0px;
		margin: 0px;
	}
</style>
</head>
<body>
 <iframe id="messengerWindow" src="http://localhost:8080" style="height:650px;width:400px;position:absolute;top:-10px;left:-10px;right:0px;bottom:0px">
 </iframe>
 <input type="hidden" id="userId" value="${messengerUser.empNo}">
<script>
	var number = 0;
	var messen = document.getElementById("messengerWindow");
	function sendMessage(){
		messen.contentWindow.postMessage("sendingMessage", "*")
	}
	var id= document.getElementById("userId");
	setTimeout(function(){
		messen.contentWindow.postMessage(id.value, "*")
	}, 2000);
	
</script>
</body>
</html>