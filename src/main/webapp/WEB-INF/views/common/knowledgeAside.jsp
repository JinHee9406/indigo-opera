<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
<link rel="stylesheet" href="/opera/resources/css/asideStyle.css">
<link rel="stylesheet" href="/opera/resources/css/treemenuStyle.css">
</head>
<body>
		<div id="asideBack">
			<div id="AsideFirstBtn" onclick="location.href='knowledgeAdd.know'">지식공유 작성</div>
			
			<div style="margin-left: 25px;margin-top: 25px; width: 150px; height: 80%; background: white;">
			<!-- 트리메뉴-->
				<!-- 마지막 리스트부분에 class="last",class="end" 넣어주세요 -->
				<div class="tree_box">
				    <div class="con">
				        <ul id="tree_menu" class="tree_menu">
				            <li class="depth_1"><a href="knowledgeMain.know"><strong>전체지식</strong></a>
				                <ul class="depth_2" >
				                    <li>
				                        <a href="#none"><em>폴더</em>공통관련지식</a>
				                        <ul class="depth_3">
				                            <li class="end"><a href="knowledgeType.know?knowType=회사관련">회사관련지식</a></li>
				                        </ul>
				                    </li>
				                  	<li>
				                        <a href="#none" ><em>폴더</em>부서별관련지식</a>
				                        <ul class="depth_3">
				                            <li><a href="knowledgeType.know?knowType=연구개발부관련" >연구개발부서관련</a></li>
				                            <li><a href="knowledgeType.know?knowType=기획부관련">기획부서관련</a></li>
				                            <li><a href="knowledgeType.know?knowType=인사총무부관련">인사총무관련</a></li>
				                            <li class="end"><a href="knowledgeType.know?knowType=재무회계부관련">재무회계관련</a></li>
				                        </ul>
				                    </li>
				                    <li class="last">
				                        <a href="#none"><em>폴더</em>외부관련지식</a>
				                        <ul class="depth_3">
				                        	<li><a href="knowledgeType.know?knowType=경비부관련">경비부서관련</a></li>
				                            <li class="end"><a href="knowledgeType.know?knowType=회사외부관련">회사외부관련지식</a></li>
				                        </ul>
				                    </li>
				                </ul>
				            </li>
				        </ul>
				    </div>
				</div>
				<!--//트리메뉴-->	
			</div>
		</div>
		
		<script>
			function tree_menu() {
				  // $('.depth_2');
				  $('ul.depth_2 >li > a').click(function(e) {

				    var temp_el = $(this).next('ul');
				    var depth_3 = $('.depth_3');
	
				    // 처음에 모두 슬라이드 업 시켜준다.
				    depth_3.slideUp(300);
				    // 클릭한 순간 모두 on(-)을 제거한다.// +가 나오도록
				    depth_3.parent().find('em').removeClass('on');
	
				    if (temp_el.is(':hidden')) {
				      temp_el.slideDown(300);
				      $(this).find('em').addClass('on').html('하위폴더 열림');
				    } else {
				      temp_el.slideUp(300);
				      $(this).find('em').removeClass('on').html('하위폴더 닫힘');
				    }
	
				    return false;
	
				  });
				}
				if ($('#tree_menu').is(':visible')) {
				  tree_menu();
				}
		</script>
</body>
</html>