<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
<style>
   #myPageAside{
      float:left; 
      padding-left:60px; 
      padding-top: 50px; 
      width: 220px; 
      height: 800px; 
      background: #E5E5E5;
      position: fixed;
   }
        
         .AsideBtn{
      padding-top: 10px; 
      width:94%; 
      height: 40px;
      background: #FFFFFF; 
      color: 57585E; 
      text-align: left;
      cursor: pointer;
      border: 1px solid lightgray;
      padding-top: 20px;      
      font-size: 1.25em;
      padding-left: 10px;
   } 
   
</style>
</head>
<body>
    
    
    <div id="myPageAside">
         <div id="AsideFirstBtn" class ="AsideBtn" style="background: #627192; color: white; " onclick="location.href='myPageMain.my'">내정보</div>
         <div id="AsideFirstBtn2" class ="AsideBtn" onclick="location.href='manageAlert.my'">결재알림설정</div>
         <div id="AsideFirstBtn3" class ="AsideBtn" onclick="location.href='manageLine.my'">개인결재선관리</div>
         <div id="AsideFirstBtn4" class ="AsideBtn" onclick="location.href='manageReception.my'">개인수신자관리</div>
         <div id="AsideFirstBtn5" class ="AsideBtn" onclick="location.href='bookMark.my'">중요업무</div>
    </div>
       
      <script type="text/javascript">
      	$(".AsideBtn").click(function () {
      		$(this).parent().children().css({"background":"#FFFFFF","color":"#57585E"});
			$(this).css({"background":"#627192","color":"white"});
		});
      </script>
      
      
</body>
</html>