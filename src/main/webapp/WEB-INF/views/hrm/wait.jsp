<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
   <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <link rel="stylesheet" href="/fpv/resources/css/noticeStyle.css">
<title>Opera GroupWare</title>
<style type="text/css">
   #writeBtn:hover{
      cursor: pointer;
   }
   td:hover{
      cursor: pointer;
   }
   .pagingBtn:hover{
      cursor: pointer;
   }
   .pagingSide:hover{
      cursor: pointer;
   }
</style>
</head>
<body>
   <jsp:include page="../common/operaNav.jsp"/>   
   <jsp:include page="../common/hrmAside.jsp"/>   
   <section>
      <form action="wait.me" style="margin-left: 300px; margin-top: 100px;">
      <div id="mainDiv">
         <div id="titleDiv">
            <a id="titleName" style="color: #57585E;">승인대기목록</a>
         </div>
         <hr style="margin-top: 1%; margin-bottom: 2%;">
        
            <div style="width: 1140px; clear: both; text-align: center; position: absolute;">
               <table id="listTable">
                  <tr class="head" style="background-color:#27334C; color:white;">
                     <th width="20px;" > </th>
                     <th width="200px;" >사번</th>
                     <th width="140px;" >이름</th>
                     <th width="200px;" >직위</th>
                     <th width="260px;" >부서</th>
                     <th width="180px;" >입사일</th>
                     <th width="140px;" >상태</th>
                     <th width="140px;" >등록자</th>
                  </tr>
                  <tr class="row">
                     <td ><div style="border: 0.5px solid gray; width: 10px; height: 10px; margin-left: 20px;"></div></td>
                     <td >201707343</td>
                     <td >인효근</td>
                     <td >대리</td>
                     <td >수프림팀</td>
                     <td >2017/06/09</td>
                     <td >승인 대기중</td>
                     <td > </td>
                  </tr>
                  
                  
               </table>
                  <div id="writeBtn" style="float:right; padding: 5px; font-size: 14px; margin-right: 5px; margin-top: 9px; width: 55px; height: 20px; background: #27334C; color: white; ">
                  <p style="margin-top: 2px;">승인</p>
            </div> 
            <div id="writeBtn" style="float:right; padding: 5px; font-size: 14px; margin-right: 5px; margin-top: 9px; width: 55px; height: 20px; background: #27334C; color: white; ">
             <p style="margin-top: 2px;">취소</p>
            </div> 
               <br><br>
                 
            </div>
         </div>
         </form>
   </section>
   <script>
      function openAside(){

      }
      $("#listTable tr").click(function(){
         location.href="wait.me";
      })
   </script>
</body>
</html>
