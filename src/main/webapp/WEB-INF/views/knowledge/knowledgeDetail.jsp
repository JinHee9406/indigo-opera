<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
   <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <link rel="stylesheet" href="/opera/resources/css/knowledgeStyle.css">
   <link rel="stylesheet" href="/opera/resources/css/basePageStyle.css">
<title>Opera GroupWare</title>
<style>
   	#mainDiv {
			float: left;
			margin-left: 10px;
			margin-top: 120px;
		}
	.detailBtn{
		width: 90px;
		height: 20px;
		border-radius: 5px;
		border: 1px solid #ccc;
		margin-right: 8px;
		margin-bottom: 5px;
		text-align: center;
		font-size: 0.9em;
		font-weight: bold;
		float: right;
		padding-top: 3px;
		border: 1px solid #9F9F9F;
		box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
		border-radius: 5px;
		cursor: pointer;
	}
	
	#replyContainer{
		border-radius: 5px; 
		margin-top: 10px; 
		margin-bottom: 10px; 
		margin-left: 25px; 
		width: 95%; 
		background:#DCDCDC;
	}
	
	#replyText{
		float: left;
		margin: 40px; 
		margin-bottom: 20px; 
		width: 800px; 
		height: 80px; 
		resize: none;
	}
	#addReplyBtn{
		float: left;
		width: 90px; 
		margin-top: 40px;
		height: 60px; 
		background: #27334C;
		color: white;
		font-weight: bold;
	}
	
	.reply{
		width: 950px; 
		height: 100%;
		min-height: 40px;
		border: 1px solid #616161; 
		border-radius: 5px; 
		background:#F0F0F0;
		margin-top: 5px;
		margin-bottom: 5px;
	}
	
	.reply a{
		margin: 10px;
		margin-left: 40px;
		margin-bottom: 10px;
		float: left;
	}
	.replyName{
		font-weight: bold;
		color: #555555;
	}
	.replyContent{
		margin-top: 10px;
		width: 60%;
		outline: none;
		resize: none;
		background: none;
		border: none;
	}
	
	.replyDate{
		font-size: 0.8em;
	}
	

</style>
</head>
<body>
	<jsp:include page="../common/knowledgeAside.jsp"/>	
	<section>
		<div id="mainDiv">
				<div id="titleDiv">
					<a id="titleName">내용 보기</a>
				</div>
				<div style="margin-bottom: 15px;">
					<div class="detailBtn" onclick="location.href='knowledgeMain.know'">목록으로</div>
					<div class="detailBtn" onclick="removeKnowledge();">글 지우기</div>
					<!-- postNum, knowledgeTitle, knowledgeDetail, knowledgeDate, showPostCnt, knowledgeType, knowledgeUser, knowledgeUserName -->
						<form id="fixElements" action="knowledgeFix.know" method="post">
							<input type="hidden" name="knowledgeNum" value="${knowledge.knowledgeNum}">
							<input type="hidden" name="knowledgeTitle" value="${knowledge.knowledgeTitle}">
							<input type="hidden" name="knowledgeDetail" value='<c:out value="${knowledge.knowledgeDetail}"/>'>
							<input type="hidden" name="knowledgeDate" value="${knowledge.knowledgeDate}">
							<input type="hidden" name="showpostCnt" value="${knowledge.showpostCnt}">
							<input type="hidden" name="knowledgeType" value="${knowledge.knowledgeType}">
							<input type="hidden" name="knowledgeUser" value="${knowledge.knowledgeUser}">
							<input type="hidden" name="knowledgeUserName" value="${knowledge.knowledgeUserName}">							
						</form>
					<div class="detailBtn" onclick="fixKnowledge();">글 수정</div>
				</div>
				<br>
				<div id="detailHeader" style="width: 100%; height: 100px; border-top: 2px solid #848484; border-bottom: 2px solid #848484; background: #F9F9F9;">
					<div id="detailTitle" style="margin-top: 15px; margin-left: 40px; font-size: 1.2em;"><h2><c:out value="${ knowledge.knowledgeTitle }"/></h2></div>
					<div id="detailName" style="margin-left: 40px; font-size: 0.9em; margin-top: 10px; color: #909090;"><c:out value="${ knowledge.knowledgeUserName }"/><br>
						<div style="float:right; margin-right: 55px; color: black;">조회수&nbsp;&nbsp;&nbsp;<c:out value="${ knowledge.showpostCnt }"/></div>
						<div style="float:right; margin-right: 35px; color: black;"><c:out value="${ knowledge.knowledgeDate }"/></div>
						<div style="float:right; margin-right: 35px; color: black;"><c:out value="${ knowledge.knowledgeType }"/></div>	
					</div>
				</div>
				<div id="knowledgeDetail" style="width: 100%; height: 100%; border: 1px solid #B0B0B0; border-radius: 5px;">
					<div id="detailCon" style="height: 100%; padding: 10px;">
					${ knowledge.knowledgeDetail }
					</div>
					<div id="replyContainer">
						<div>
							<form id="applySubmit" action="addKnowledgeReply.know" method="post">
								<textarea id="replyText" name="replyContent" placeholder="내용을 작성해주세요!"></textarea>
								<input type="hidden" name="userName" value="${loginUser.empName} ${loginUser.empJobName}">
								<input type="hidden" name="userNum" value="${loginUser.empNo}">
								<input type="hidden" name="knowledgeNum" value="${knowledge.knowledgeNum}">					
							</form>
							<button id="addReplyBtn" onclick="applySubmit();">댓글 쓰기</button>
						</div>
						<hr style="clear: both;">
						<div style="margin-left:40px;">
						<c:set var="replyCnt" value="0"/>
						<c:forEach var="knowReply" items="${knowledgeReply}">
							<div class="reply">
								<a class="replyName">${knowReply.userName}</a>
								<a class="replyDate">${knowReply.replyDate}</a>
								<textarea class="replyContent" id="${replyCnt}">${knowReply.replyContent}</textarea>
								<c:set var="replyCnt" value="${replyCnt+1}"/>
							</div>
						</c:forEach>
						</div>
						<hr>
					</div>
				</div>
		</div>

	</section>
	<jsp:include page="../common/operaNav.jsp"/>
		
	<script>
		function fixKnowledge(){
			if('${loginUser.empNo}' == '${knowledge.knowledgeUser}'){
				if(confirm("게시글을 수정하시겠습니까?") == true){
					document.getElementById("fixElements").submit();
				}
			} else {
				alert("글을 수정할수 있는 권한이 없습니다.");
			}
			
		}

		function removeKnowledge(){
			if('${loginUser.empNo}' == '${knowledge.knowledgeUser}'){
				if(confirm("공유글을 지우시겠습니까?") == true){
					location.href = "removeKnowledge.know?knowNum=${knowledge.knowledgeNum}"
				}
			} else {
				alert("글을 지울수 있는 권한이 없습니다.");
			}
		}

		function applySubmit(){
			if(document.getElementById("replyText").value == ""){
				alert("내용을 입력해주세요!");
			}else {
				document.getElementById("applySubmit").submit();
				}

		}
		
		function resizeApply(){
			for(var i = 0; i < '${replyCnt}'; i++){
				$("#"+ i ).height(1).height($("#"+ i ).prop('scrollHeight') + 12);
			}
		}
		resizeApply();
	</script>
</body>
</html>