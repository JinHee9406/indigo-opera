<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
   <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <link rel="stylesheet" href="/opera/resources/css/knowledgeStyle.css">
   <link rel="stylesheet" href="/opera/resources/css/basePageStyle.css">
   <style>
	   #okBtn{
	   		width: 100px;
	   		height: 30px;
	   		border-radius: 5px;
	   		background: #27334C;
	   		font-weight: bold;
	   		color: white;
	   }
   
   </style>
</head>
<body>
	<jsp:include page="../common/knowledgeAside.jsp"/>	
	<jsp:include page="../common/operaNav.jsp"/>
	<section>
		<div style="text-align: center; margin-left: 100px; margin-top: 100px;">
			<c:out value="${ message }"/><br>
			<c:if test="${key == 'addKnowledge'}">
				<button id="okBtn" onclick="location.href='knowledgeMain.know'">확인</button>
			</c:if>
			<c:if test="${key == 'addReply'}">
				<form id="checkKnow" action="knowledgeDetail.know" method="post">
					<input type="hidden" name="knowledgeNum" value="${KnowNum}">
				</form>
				<button id="okBtn" onclick="backToDetail();">확인</button>
			</c:if>
			<c:if test="${key == 'notPost'}">
				<button id="okBtn" onclick="location.href='knowledgeAdd.know'">게시글작성하기</button>
			</c:if>
			<c:if test="${key == 'notPostType'}">
				<button id="okBtn" onclick="location.href='knowledgeMain.know'">목록으로</button>
			</c:if>
		</div>
	</section>
	<script>
		function backToDetail(){
				document.getElementById("checkKnow").submit();
			}
	</script>
</body>
</html>