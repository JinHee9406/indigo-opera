<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
   <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <link rel="stylesheet" href="/opera/resources/css/noticeStyle.css">
<title>Opera GroupWare</title>
<style type="text/css">
	#writeBtn:hover{
		cursor: pointer;
	}
	td:hover{
		cursor: pointer;
	}
	.pagingBtn:hover{
		cursor: pointer;
	}
	.pagingSide:hover{
		cursor: pointer;
	}
	#searchBtn:hover{
		cursor:pointer;
	}
</style>
</head>
<body>
   <jsp:include page="../common/operaNav.jsp"/>   
   <jsp:include page="../common/attendAside.jsp"/>   
   <section >
      <div id="mainDiv" style="width:950px;">
         <div id="titleDiv">
            <a id="titleName" style="color: #57585E;">소속출근통계 - ${month }</a>
         </div>
         <hr style="margin-top: 1%; margin-bottom: 2%;">
         <div style="border: 0.5px solid #C4C4C4; margin: 10px; width: 950px; height: 35px; background: white;">
            <a style="float:left; padding: 10px; font-weight: 500; margin-left: 5px;">출근통계</a>
            <div style="float:left; margin-left: 4px; margin-right: 5px; border: 2px solid #C4C4C4; width:0.05px; height: 32px; "></div>
            <div style=" float: left; background: #C4C4C4; width:60px; padding:3px; margin-top: 4px; border-radius: 3px; text-align: center; border: 0.5px solid gray; margin-left: 5px;">당일</div>
            <div style=" float: left; background: #C4C4C4; width:60px; padding:3px; margin-top: 4px; border-radius: 3px; text-align: center; border: 0.5px solid gray;margin-left: 5px;">3일</div>
            <div style=" float: left; background: #C4C4C4; width:60px; padding:3px; margin-top: 4px; border-radius: 3px; text-align: center; border: 0.5px solid gray;margin-left: 5px;">7일</div>
            <div style=" float: left; background: #C4C4C4; width:60px; padding:3px; margin-top: 4px; border-radius: 3px; text-align: center; border: 0.5px solid gray;margin-left: 5px;">30일</div>
            <div style="float:left; padding: 8px;">
               <select style="background: white; color: gray; font-size: 15px; width: 120px;">
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
               </select>
               ~
            </div>
            
            <div style="float:left; padding: 8px;">
               <select style="background: white; color: gray; font-size: 15px; width: 120px;">
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
                  <option>2020/08/07</option>
               </select>
            </div>
            <div style="float:left; padding: 5px; text-align:center; font-size: 14px; margin-left: 10px; margin-top: 6px; width: 55px; height: 15px; background: #27334C; color: white; "
            id="searchBtn">
               검색
            </div>
            <br>
            <br>
            <br>
            <div style="width: 1150px; clear: both; text-align: center; ">
               <table id="listTable" style="margin-left:0;">
               <thead>
                  <tr class="head" style="background-color:#27334C; color:white;">
                     <th width="165px;" style="border: 2px; border-style:solid;  border-color: lightgray;">성명</th>
                     <th width="165px;" style="border: 2px; border-style:solid;  border-color: lightgray;">소속부서</th>
                     <th width="165px;" style="border: 2px; border-style:solid;  border-color: lightgray;">정상출근</th>
                     <th width="165px;" style="border: 2px; border-style:solid;  border-color: lightgray;">지각</th>
                     <th width="165px;" style="border: 2px; border-style:solid;  border-color: lightgray;">결근</th>
                     <th width="165px;" style="border: 2px; border-style:solid;  border-color: lightgray;">반차</th>
                     <th width="165px;" style="border: 2px; border-style:solid;  border-color: lightgray;">휴가</th>
                  </tr>
               </thead>
               <tbody>
                  <c:if test="${loginUser.empJobCode eq 'JOB1' || loginUser.empJobCode eq 'JOB2' || loginUser.empJobCode eq 'JOB3' || loginUser.empJobCode eq 'JOB4'   }">
                  <c:forEach var="da" items="${deptAtt}">
                  <tr class="row">
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">${da.empName }</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">${da.deptName }</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">${da.attCnt }</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">${da.lateCnt }</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">${da.absCnt }</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">0</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">0</td>
                  </tr>
                  </c:forEach>
                  </c:if>
                  <c:if test="${loginUser.empJobCode ne 'JOB1' && loginUser.empJobCode ne 'JOB2' && loginUser.empJobCode ne 'JOB3' && loginUser.empJobCode ne 'JOB4'   }">
                  <c:forEach var="da" items="${deptAtt}">
                  <c:if test="${loginUser.empNo eq da.empNo }">
                  <tr class="row">
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">${da.empName }</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">${da.deptName }</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">${da.attCnt }</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">${da.lateCnt }</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">${da.absCnt }</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">0</td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">0</td>
                  </tr>
                  </c:if>
                  </c:forEach>
                  </c:if>
               </tbody>   
               </table>
                  
               <br><br>
                 
         </div>
      </div>
     </div>
   </section>
   <script>
      function openAside(){

      }
      $("#listTable tr").click(function(){
         location.href="knowledgeDetail.me";
      })
   </script>
   <script type="text/javascript">
   $(document).ready(
	function () {
		
		var forYear = '${month }'.substring(0,4);
		var forMon = '${month }'.substring(4,6);
		
		
		
	    var date1 = new Date(forYear, forMon - 1, 01); // 2017-11-30
	    var date2 = new Date(); // 2017-12-6
	
	    var count = 0;
	    
	
	
	while(true) {  
	
	
	    var temp_date = date1;
	    if(temp_date.getTime() > date2.getTime()) {
	        console.log("count : " + count);
	        break;
	    } else {
	        var tmp = temp_date.getDay();
	        if(tmp == 0 || tmp == 6) {
	            // 주말
	            console.log("주말");
	            count++;         
	        } else {
	            // 평일
	            console.log("평일");
	        }
	        temp_date.setDate(date1.getDate() + 1); 
	    }
	}
	var a = 0;
	$("#listTable tbody tr td:nth-child(5)").each(function(){
		/* var total = $("#listTable tbody tr td:nth-child(5)").text() - count;
		var total = $("#tdtd"+a).text() - count; 
		a ++;
		console.log("a"+ a +" : "+ total); */
		console.log(this);
		this.innerHTML = this.innerHTML - count;
		
	});
	console.log("a : " + a);
	console.log($("#listTable tbody tr td:nth-child(5)").text());
	
	
	});
</script>




</body>
</html>