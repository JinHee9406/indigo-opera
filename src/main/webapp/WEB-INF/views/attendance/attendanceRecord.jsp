<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
   <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <link rel="stylesheet" href="/opera/resources/css/noticeStyle.css">
<title>Opera GroupWare</title>
<style type="text/css">
	#writeBtn:hover{
		cursor: pointer;
	}
	td:hover{
		cursor: pointer;
	}
	.pagingBtn:hover{
		cursor: pointer;
	}
	.pagingSide:hover{
		cursor: pointer;
	}
	#searchBtn:hover{
		cursor:pointer;
	}
</style>
</head>
<body>
   <jsp:include page="../common/operaNav.jsp"/>   
   <jsp:include page="../common/attendAside.jsp"/>   
   <section >
      <div id="mainDiv" style="width:950px;">
         <div id="titleDiv">
            <a id="titleName" style="color: #57585E;">출근기록</a>
         </div>
         <hr style="margin-top: 1%; margin-bottom: 2%;">
         <div style="border: 0.5px solid #C4C4C4; margin: 10px; width: 950px; height: 35px; background: white;">
            <a style="float:left; padding: 10px; font-weight: 500; margin-left: 5px;">출근통계</a>
            <div style="float:left; margin-left: 4px; margin-right: 5px; border: 2px solid #C4C4C4; width:0.05px; height: 32px; "></div>
            <div style=" float: left; background: #C4C4C4; width:60px; padding:3px; margin-top: 4px; 
            border-radius: 3px; text-align: center; border: 0.5px solid gray; margin-left: 5px;" onclick="goAttend();">${month }</div>
            <br>
            <br>
            <div style="width: 1150px; clear: both; text-align: center; ">
               <table id="listTable" style="margin-left:0;">
                  <tr class="head" style="background-color:#27334C; color:white;">
                     <th width="255px;" style="border: 2px; border-style:solid;  border-color: lightgray;">일자</th>
                     <th width="255px;" style="border: 2px; border-style:solid;  border-color: lightgray;">출근</th>
                     <th width="255px;" style="border: 2px; border-style:solid;  border-color: lightgray;">퇴근</th>
                     <th width="255px;" style="border: 2px; border-style:solid;  border-color: lightgray;">상태</th>
                  </tr>
                  <c:forEach var="att" items="${attendance}">
                  <tr class="row">
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;"><fmt:formatDate value="${att.workTime}" pattern="yyyy-MM-dd"/></td>
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;"><fmt:formatDate value="${att.workTime}" pattern="HH:mm"/></td>
                     <c:if test="${not empty att.homeTime }">
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;"><fmt:formatDate value="${att.homeTime}" pattern="HH:mm"/></td>
                     </c:if>
                     <c:if test="${empty att.homeTime }">
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">퇴근 기록이 없습니다.</td>
                     </c:if>
                     <fmt:formatDate var="worktime2" value="${att.workTime }" pattern="HH"/>
                     <fmt:formatNumber var="formatmin" value="${worktime2 }" minIntegerDigits="2"/>
                     <c:if test="${formatmin >= 9}">
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">지각</td>
                     </c:if>
                     <c:if test="${formatmin < 9}">
                     <td style="border: 2px; border-style:solid;  border-color: lightgray;">정상출근</td>
                     </c:if>
                  </tr>
                  </c:forEach>
                  
               </table>
                  
               <br><br>
                 
         </div>
      </div>
     </div>
     
     
   </section>
   <script>
      function openAside(){

      }
      $("#listTable tr").click(function(){
         location.href="knowledgeDetail.me";
      })
   </script>
   <script type="text/javascript">
   var today = new Date();
   var dd = today.getDate();
   var mm = today.getMonth()+1; // Jan is 0
   var yyyy = today.getFullYear();

   if(dd<10){
       dd = '0'+dd
   }
   if(mm<10){
       mm = '0'+mm
   }

   today = ""+yyyy +  mm +  dd ;
  /* $("#today").text(today); */
   
   	function goAttend() {
		location.href="attendMain.att?date="+today;
	}
   </script>
</body>
</html>