<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Login</title>
<style>
	html {
	background: #27334C;
	width: 1440px;
	height: 900px;
	}
	#loginArea {
	background: white;
	width: 700px;
	height: 500px;
	margin-left: auto;
	margin-right: auto;
	margin-top: 200px;
	border-radius: 20px;
	font-weight: 900;
	font-size: 15px;
	}
	#opera{
	font-size: 100px;
	text-align: center;
	}
	#login{
	width:315px;
	height:50px;
	background-color: #27334C;
	color:white;
	font-size:18px;
	border: 0px;
	}

</style>
</head>
<body>
	<div id="loginArea">
		<div id="opera"><a style="color:#E61358;">O</a><a style="color:#27334C;">PERA</a></div>
		 	<form action="login.me" method="post">
			<table align="center">
			<tr>
				<td><input type="text" name="empNo" maxlength="30" placeholder="아이디를 입력하세요" style="width:300px; height:50px; margin-top:50px; padding-left: 10px;"></td>
			</tr>
			<tr>
        		<td><input type="password" name="empPwd" maxlength="30" placeholder="비밀번호를 입력하세요" style="width:300px; height:50px; margin-top:20px; padding-left: 10px;"></td>
        	</tr>
       		 <tr>
				<td><input type="checkbox"style="margin-top: 10px;">아이디저장&nbsp;&nbsp;&nbsp;비밀번호찾기</td>
			</tr>
			<tr>
				<td><button id="login">로그인</button></td>
			</tr>
			</table>
			</form>
	</div>
</body>
</html>