<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
		.resDetail_wrap {
			display: none;
			position: fixed;
			left: 0;
			top: 0;
			width: 100%;
			height: 100%;
			z-index: 10000
		}
		
		.dark_bg {
			position: absolute;
			width: 100%;
			height: 100%;
			background: #000;
			filter: alpha(opacity = 60);
			opacity: .6;
			-moz-opacity: .6
		}
		
		.resDetail_box {
			position: relative;
			top: 5%;
			width: 850px;
			height: 600px;
			background: #fff;
			margin: 0 auto;
			border-radius: 8px;
			background: #fff;
			border: 1px solid #ccc
		}
		
		.resDetail_box .close {
			position: absolute;
			right: 6px;
			top: 6px;
			width: 16px;
			height: 16px;
			cursor: pointer
		}
		
		#addWindowTitle{
			float:left; 
			font-weight: bold; 
			margin-left: 80px; 
			margin-top: 45px;
			color: #626262;
		}
		
		
		#windowIcon{
			width: 35px;
			height: 35px;
			padding-right: 10px;
			margin-bottom: -5px;
		}
		
		#reservTable th{
		 width: 250px;
		}
		
		.closeBtn{
			float:right; 
			margin-left: 555px; 
			font-size: 1.25em; 
			font-weight: bold;
			cursor: pointer;
		}
		
		#cencelBtn{
			margin-top: 10px; 
			margin-left: 290px; 
			background: #E61358; 
			color: white; 
			font-weight: bold; 
			width: 150px; 
			height: 35px;
			border-radius: 25px;
			cursor:pointer;
			outline: none;
		}

</style>
</head>
<body>
		<div class="resDetail_wrap" style="display: none;">
			<div class="dark_bg" onclick="jQuery('.resDetail_wrap').fadeOut('slow')"></div>
			<div class="resDetail_box">
			<div id="addWindowTitle">
				<div style="margin-bottom: 15px;">
					<div class="closeBtn" onclick="jQuery('.resDetail_wrap').fadeOut('slow')">X</div>
					<img id="windowIcon" src="/opera/resources/icons/meetingIcon-black.png"><a style="font-size: 1.6em;">예약상세</a>
					</div>
					<!--<img class="close" onclick="jQuery('.res_wrap').fadeOut('slow')"  width="20px" height="20px"> -->
				</div>
				<br>
				<hr style="clear: both; width: 700px; margin-left: 75px;">
				<div id="reswindowCon">
					<div id="windowConBack" style="margin-left: 45px; margin-top: 25px; width:755px; height: 410px; background: #F5F5F5">
						<table id="reservTable" style="width: 100%;  height: 380px;">
							<tr style="height: 50px;">
								<th>회의실</th>
								<td><div id="roomDetail"></div></td>
							</tr>
							<tr style="height: 50px;">
								<th>신청인</th>
								<td><div id="reservEmpDetail"></div></td>
							</tr>							
							<tr style="height: 50px; ">
								<th>예약시간</th>
								<td><div id="reservStimeDetail"></div>
								</td>
							</tr>							
							<tr style="height: 50px;">
								<th>신청인원</th>
								<td><div id="personDetail"></div></td>
							</tr>							
							<tr style="height: 200px;">
								<th>신청사유</th>
								<td><div id="resSelectDetail"></div></td>
							</tr>
						</table>
						<form id="cencelForm" action="cencelReservation.res" method="post">
							<input type="hidden" id="reservId" name="resNo">
							<input type="hidden" id="reservEmp" name="empNo">
						</form>
						<button id="cencelBtn" onclick="cencelFunc();">예약취소</button>
					</div>
				</div>
			</div>
		</div>
		<script>
			function cencelFunc(){
				var reservationEmpNo = document.getElementById("reservEmp").value;

				if(${loginUser.empNo} == reservationEmpNo){
					if(confirm("예약을 취소하시겠습니까?")){
						document.getElementById("cencelForm").submit();
					}
				} else {
					alert("예약을 취소할수 있는 권한이 없습니다");
				}
			}
		</script>
</body>
</html>