<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
   <link href="https://fonts.googleapis.com/css2?family=Gothic+A1&display=swap" rel="stylesheet">
   <link rel="stylesheet" href="/opera/resources/css/calendarStyle.css"> 
   <link rel="stylesheet" href="/opera/resources/css/basePageStyle.css">
   <style>
   		#mainDiv {
			float: left;
			width: 1200px;
			margin-left: 20px;
			margin-top: 70px;
		}
		
		#dateMonth{
			font-size: 2em; 
			font-weight: bold; 
			color: #ef3333; 
			padding-left: 5px; 
			padding-right: 5px;
		}
		
		#dateDay{
			font-size: 2em; 
			font-weight: bold; 
			color: #ef3333; 
			padding-left: 5px; 
			padding-right: 15px;
		}
		.res_wrap {
			display: none;
			position: fixed;
			left: 0;
			top: 0;
			width: 100%;
			height: 100%;
			z-index: 10000
		}
		
		.dark_bg {
			position: absolute;
			width: 100%;
			height: 100%;
			background: #000;
			filter: alpha(opacity = 60);
			opacity: .6;
			-moz-opacity: .6
		}
		
		.res_box {
			position: relative;
			top: 5%;
			width: 850px;
			height: 600px;
			background: #fff;
			margin: 0 auto;
			border-radius: 8px;
			background: #fff;
			border: 1px solid #ccc
		}
		
		.res_box .close {
			position: absolute;
			right: 6px;
			top: 6px;
			width: 16px;
			height: 16px;
			cursor: pointer
		}
		
		#addWindowTitle{
			float:left; 
			font-weight: bold; 
			margin-left: 80px; 
			margin-top: 45px;
			color: #626262;
		}
		
		
		
		#windowIcon{
			width: 35px;
			height: 35px;
			padding-right: 10px;
			margin-bottom: -5px;
		}
		
		#reservTable th{
		 width: 250px;
		}
		
		.closeBtn{
			float:right; 
			margin-left: 555px; 
			font-size: 1.25em; 
			font-weight: bold;
			cursor: pointer;
		}
		
		#roomTable{
			margin-top: 15px;
			width: 120px; 
			text-align: center;
			border-collapse: collapse;
		}
		
		#roomTable th{
			height: 130px;
			border: 1px solid #5B5B5B;
		}
		
		#reservTimeTable{
			height: 30px; 
			text-align: center;
		}
		
		#reservTimeTable td{
			width: 100px;
		}
		
		#reservAddTable{
			margin-top: 15px;
			border-collapse: collapse;	
			z-index: 0;
		}
		
		#reservAddTable td{
			width: 101px; 
			height: 128px; 
			text-align: center;
			border: 1px solid #5B5B5B;
		}
		
		#reservTimes{
			border-radius:25px; 
			width: 1042px; 
			margin-left: 135px;
			border: 1px solid #737373; 
			background: #ECECEC;
		}
		
		td.ui-selecting{
			background: #27334C;
		}
		
		td.ui-selected{
			background: #27334C;
		}
		
		#submitBtn{
			margin-top: 10px; 
			margin-left: 290px; 
			background: #27334C; 
			color: white; 
			font-weight: bold; 
			width: 150px; 
			height: 35px;
			border-radius: 25px;
		}
		
		.timeSet{
			padding-top: 6px; 
			text-align: center;
			border-radius: 20px; 
			height: 24px; 
			background: #C4C4C4; 
			color: black;
			z-index: 0;
			cursor: pointer;
		}
   </style>
</head>
<body>
		<jsp:include page="../common/reservationAside.jsp" />
		<jsp:include page="../common/operaNav.jsp" />
		<jsp:include page="../reservation/reservationDetail.jsp"/>
		<div class="res_wrap" style="display: none;">
			<div class="dark_bg" onclick="jQuery('.res_wrap').fadeOut('slow')"></div>
			<div class="res_box">
			<div id="addWindowTitle">
				<div style="margin-bottom: 15px;">
					<div class="closeBtn" onclick="jQuery('.res_wrap').fadeOut('slow')">X</div>
					<img id="windowIcon" src="/opera/resources/icons/meetingIcon-black.png"><a style="font-size: 1.6em;">예약신청</a>
					</div>
					<!--<img class="close" onclick="jQuery('.res_wrap').fadeOut('slow')"  width="20px" height="20px"> -->
				</div>
				<br>
				<hr style="clear: both; width: 700px; margin-left: 75px;">
				<div id="reswindowCon">
					<div id="windowConBack" style="margin-left: 45px; margin-top: 25px; width:755px; height: 410px; background: #F5F5F5">
						<form id="resForm" action="reservationPut.res" method="post">
						<table id="reservTable" style="width: 100%;  height: 380px;">
							<tr style="height: 50px;">
								<th>회의실</th>
								<td><input type="text" id="roomName" value=""><input id="roomId" type="hidden" name="roomNo"></td>
							</tr>
							<tr style="height: 50px;">
								<th>신청인</th>
								<td><div id="userNameInModal">${loginUser.empName} ${loginUser.empJobName}</div><input name="empNo" type="hidden" value="${loginUser.empNo}"></td>
							</tr>							
							<tr style="height: 50px; ">
								<th>예약시간</th>
								<td>
									<input style="display: none;" id="resDay" type="date" name="resDay">
									<a id="resrvAddDate"></a>&nbsp;&nbsp;&nbsp;&nbsp;<input name="startDate" id="startTime" type="time"> ~ <input name="endDate" id="stopTime" type="time">
								</td>
							</tr>							
							<tr style="height: 50px;">
								<th>신청인원</th>
								<td><input name="personNum" type="number" style="text-align:center; width: 50px; font-size: 1.25em;"></td>
							</tr>							
							<tr style="height: 200px;">
								<th>신청사유</th>
								<td><textarea name="resDetail" style="resize: none; width:100%; height: 100%;"></textarea></td>
							</tr>
						</table>
						</form>
						<button id="submitBtn" onclick="submitRes();">신청하기</button>
					</div>
				</div>
			</div>
		</div>


	<section>
		<div id="mainDiv">
			<div id="titleDiv">
				<img id="titleIcon"
					src="/opera/resources/icons/meetingIcon-black.png"><a
					id="titleName">예약신청</a>
			</div>
			<hr>
			<div id="dateContainer" style="margin-top: 10px;">
				 <a style="cursor: pointer;" onclick="prevDate();">&#60;</a><a id="dateyear" style="padding-left: 15px; padding-right: 5px;"></a>. 
				 <a id="dateMonth"></a>. 
				 <a id="dateDay"></a> 
				 <a style="cursor: pointer;" onclick="nextDate();">&#62;</a>
			</div>
			<div id="sideContainer" style=" margin-top: 25px;">
				<div id="meetingRoomLists" style="float: left;">
					<table id="roomTable">
						<tr><th style="width: 120px; height: 30px; border: none"></th></tr>
						<c:forEach var="room" items="${meetingRoom}">
						<tr><th>${room.roomName}<input type="hidden" value="${room.roomNo}"></th></tr>
						</c:forEach>
					</table>
				</div>
				<div id="reservTimes">
					<table id="reservTimeTable">
						<tr >
							<td>10</td>
							<td>11</td>
							<td>12</td>
							<td>13</td>
							<td>14</td>
							<td>15</td>
							<td>16</td>
							<td>17</td>
							<td>18</td>
							<td>19</td>
						</tr>
					</table>
				</div>
				<div id="reservLists" style="float: left; margin-left: 15px">
				<c:set var="roomCnt" value="0"></c:set>
				<c:forEach var="room" items="${meetingRoom}">
					<div class="roomDiv" id="room${room.roomNo}" style="position: absolute; margin-top: ${60 + 131*roomCnt}px;">
					</div>
				<c:set var="roomCnt" value="${roomCnt+1}"/>
				</c:forEach>
					<table id="reservAddTable">
							<c:forEach var="room" items="${meetingRoom}">
							<tr>
								<td id="res10">
									<input id="tableRoom" type="hidden" value="${room.roomName}">
									<input id="tableRoomId" type="hidden" value="${room.roomNo }">
									<input id="timeVal" type="hidden" value="10">
								</td>
								<td id="res11">
									<input id="tableRoom" type="hidden" value="${room.roomName}">
									<input id="tableRoomId" type="hidden" value="${room.roomNo }">
									<input id="timeVal" type="hidden" value="11">
								</td>
								<td id="res12">
									<input id="tableRoom" type="hidden" value="${room.roomName}">
									<input id="tableRoomId" type="hidden" value="${room.roomNo }">
									<input id="timeVal" type="hidden" value="12">
								</td>
								<td id="res13">
									<input id="tableRoom" type="hidden" value="${room.roomName}">
									<input id="tableRoomId" type="hidden" value="${room.roomNo }">
									<input id="timeVal" type="hidden" value="13">
								</td>
								<td id="res14">
									<input id="tableRoom" type="hidden" value="${room.roomName}">
									<input id="tableRoomId" type="hidden" value="${room.roomNo }">
									<input id="timeVal" type="hidden" value="14">
								</td>
								<td id="res15">
									<input id="tableRoom" type="hidden" value="${room.roomName}">
									<input id="tableRoomId" type="hidden" value="${room.roomNo }">
									<input id="timeVal" type="hidden" value="15">
								</td>
								<td id="res16">
									<input id="tableRoom" type="hidden" value="${room.roomName}">
									<input id="tableRoomId" type="hidden" value="${room.roomNo }">
									<input id="timeVal" type="hidden" value="16">
								</td>
								<td id="res17">
									<input id="tableRoom" type="hidden" value="${room.roomName}">
									<input id="tableRoomId" type="hidden" value="${room.roomNo }">
									<input id="timeVal" type="hidden" value="17">
								</td>
								<td id="res18">
									<input id="tableRoom" type="hidden" value="${room.roomName}">
									<input id="tableRoomId" type="hidden" value="${room.roomNo }">
									<input id="timeVal" type="hidden" value="18">
								</td>
								<td id="res19">
									<input id="tableRoom" type="hidden" value="${room.roomName}">
									<input id="tableRoomId" type="hidden" value="${room.roomNo }">
									<input id="timeVal" type="hidden" value="19">
								</td>
							</tr>
							</c:forEach>
					</table>
				</div>
			</div>
		</div>
	</section>
	<script>
		function openDetail(value){

			$.ajax({
				url:"reservationDetail.res",
				type:"post",
				data:{resNo: value},
				success:function(data) {

					console.log(data.resDetail);

					$("#reservId").val(data.detail.resNo);
					$("#reservEmp").val(data.detail.empNo);
					$("#roomDetail").text(data.detail.roomName);
					$("#reservEmpDetail").text(data.detail.empName);
					$("#reservStimeDetail").text(data.detail.startDate + " ~ " + data.detail.endDate);
					$("#personDetail").text(data.detail.personNum +"명");
					$("#resSelectDetail").text(data.detail.resDetail);
				},
				error:function(){
					console.log("에러!");
				}
			});

			
			jQuery('.resDetail_wrap').fadeIn('slow');
		}

	
		function showReservation(dayString) {
			$(".roomDiv").children().remove();
			var insertDay;

			insertDay = dayString.substr(0,4) + "-" + dayString.substr(5,2)+"-"+dayString.substr(8,2)
			
			$.ajax({
				url:"reservationStatus.res",
				type:"post",
				data:{dayString: insertDay},
				success:function(data) {
					var reservList = data.resList;
					console.log(reservList.length);
					var index = 0;
					reservList.forEach(function(){
						var startTime = reservList[index].startDate.substr(0,2);
						var endTime = reservList[index].endDate.substr(0,2);

						var startGraph = startTime - 11;
						var graphLength = endTime - startTime;

						if(reservList[index].empNo != ${loginUser.empNo}){
							$("#room" + reservList[index].roomNo).append("<div onclick='openDetail("+reservList[index].resNo+");' class='timeSet' style='position:absolute; margin-left:"+ (102 + (102.5) * startGraph) +
									"px; width: "+ (102.5*graphLength) +"px;' >"+
									reservList[index].empName+" 외 "+(reservList[index].personNum-1)+
									"<input class='roomData' type='hidden' value='"+reservList[index].roomNo+"'>"+
									"<input class='startTimeData' type='hidden' value='"+reservList[index].startDate+"'>"+
									"<input class='endTimeData' type='hidden' value='"+reservList[index].endDate+"'>"+
									"명</div>");
						} else {
							$("#room" + reservList[index].roomNo).append("<div onclick='openDetail("+reservList[index].resNo+");' class='timeSet' style='position:absolute; background:#27334C; color:white;  margin-left:"+ (102 + (102.5) * startGraph) +
									"px; width: "+ (102.5*graphLength) +"px;' >"+
									reservList[index].empName+" 외 "+(reservList[index].personNum-1)+
									"<input class='roomData' type='hidden' value='"+reservList[index].roomNo+"'>"+
									"<input class='startTimeData' type='hidden' value='"+reservList[index].startDate+"'>"+
									"<input class='endTimeData' type='hidden' value='"+reservList[index].endDate+"'>"+
									"명</div>");
						}
						index += 1;
					})

				},
				error:function(){
					console.log("에러!");
				}
			});
			
			return false;
		}
	</script>
	
	
	<script>
		tree_menu();//Aside에 있는 메뉴 
		$(function(){
				$("#reservationAdd").addClass('selected');
				$("#reservationState").removeClass();
				$("#reservationLog").removeClass();
			})
	</script>
<%-- 	<div onclick="openWindow('${room.roomName}');" style="width: 100%; height: 100%;"></div> --%>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<script>
		var yearText = document.getElementById('dateyear');
		var monthText = document.getElementById('dateMonth');
		var dayText = document.getElementById('dateDay');
		var today = new Date();
		var maxDate = 0;
		var selectedDate;

		var yearString;
		var monthString;
		var dayString;

		checkMonth(today.getMonth()+1, today.getFullYear());
		
		yearText.innerText = today.getFullYear();
		yearString = today.getFullYear();
		
		function monthInner(Month){
			if(Month < 10){
				monthText.innerText = "0" + Month;
				monthString = "0" + Month;
				}
			else {
				monthText.innerText = Month;
				monthString = Month;
				}
			}
		

		function dayInner(Day){
			if( Day < 10){
				dayText.innerText = "0" + Day;
				dayString =  "0" + Day;
				}
			else {
				dayText.innerText = Day;
				dayString = Day;
				}
			}
		
		monthInner(today.getMonth()+1);
		dayInner(today.getDate());

		
		selectedDate = yearString+"-"+monthString+"-"+dayString;
		showReservation(selectedDate)
		
		function prevDate(){
			var nextYear = parseInt(yearText.innerText);
			var nextMonth = parseInt(monthText.innerText);
			var nextDate = parseInt(dayText.innerText);
			nextDate -= 1;
			if(nextDate <= 0){

					nextDate = maxDate;
					nextMonth -= 1;

					if(nextMonth <= 0) {
						nextMonth = 12;
						nextYear -= 1;
						checkMonth(nextMonth, nextYear);
						nextDate = maxDate;
					} else {
						checkMonth(nextMonth, nextYear);
						nextDate = maxDate;
					}
				}
			monthInner(nextMonth);
			dayInner(nextDate);
			yearText.innerText = nextYear;

			
			selectedDate = yearString+"-"+monthString+"-"+dayString;
			showReservation(selectedDate)
			}

		function nextDate(){
			var nextYear = parseInt(yearText.innerText);
			var nextMonth = parseInt(monthText.innerText);
			var nextDate = parseInt(dayText.innerText);
			nextDate += 1;
			if(nextDate > maxDate){

					nextDate = 1;
					nextMonth += 1;

					if(nextMonth > 12) {
						nextMonth = 1;
						nextYear += 1;
						checkMonth(nextMonth, nextYear);
						nextDate = 1;
					} else {
						checkMonth(nextMonth, nextYear);
						nextDate = 1;
					}
				}
			monthInner(nextMonth);
			dayInner(nextDate);
			yearText.innerText = nextYear;
			selectedDate = yearString+"-"+monthString+"-"+dayString;
			showReservation(selectedDate)
			}
		
		function checkMonth(Month, Year){
				switch(Month){
					case 1 :
					case 3 :
					case 5 :
					case 7 :
					case 8 : 
					case 10 :
					case 12 : maxDate = 31; break;
					case 4 :
					case 6 :
					case 9 :
					case 11 : maxDate = 30; break;
					case 2 :
						if(Year % 4 == 0) {
							maxDate = 29; break; }
						else { maxDate = 28; break; }
				}
			}
	</script>
	
		<script>
		var roomName;
		var roomNum;
		var startTime;
		var stopTime;
	
	    $(function(){
	        $("#reservAddTable").selectable({
		        filter: 'td',
		        selected: function(event, ui){
			        var set = $(this).find("td.ui-selected");
			        var length = set.length;
			        $(this).find("td.ui-selected input").each(function(){
				        roomName = $("td.ui-selected input#tableRoom").eq(0).val();
				        roomNum = $("td.ui-selected input#tableRoomId").eq(0).val();
				        startTime = $("td.ui-selected input#timeVal").eq(0).val();
				        stopTime = $("td.ui-selected input#timeVal").eq(length-1).val();
				    })
		        },
		        stop: function(event, ui){

					var getResvRooms = document.getElementsByClassName("roomData");
					var getResvStime = document.getElementsByClassName("startTimeData");
					var getResvEtime = document.getElementsByClassName("endTimeData");
					var canReserv = true;
			
					for(var i = 0; i < getResvRooms.length; i++){
						if(roomNum == getResvRooms[i].value){
							if(startTime > (getResvEtime[i].value.substr(0,2))){
								continue;
							} else  {
								if(stopTime < (getResvStime[i].value.substr(0,2))){
								continue;
								} else {
									canReserv = false;
									break;
								}
							}
						}else{
							continue;
						}
					}

					if(canReserv){
						openWindow(roomName,roomNum, startTime, stopTime);
					} else {
						alert("해당 시간에는 예약할수 없습니다")
					}
			    }
	        })
	    })
	    
		function openWindow(roomName,roomNum, startTime, stopTime){

			var changeNum = parseInt(stopTime);
			var getDate = new Date();
			changeNum = changeNum + 1;
			stopTime = String(changeNum);
				
			$("#roomName").val(roomName);
			$("#roomId").val(roomNum);
			$("#startTime").val(startTime+":00");
			$("#stopTime").val(stopTime+":00");
			var inputDate = getDate.getFullYear();
			if(getDate.getMonth()+1 < 10){
				inputDate = inputDate +"-0"+ (getDate.getMonth()+1);
			} else {
				inputDate = inputDate +"-"+ (getDate.getMonth()+1);
			}
			inputDate = inputDate +"-"+ getDate.getDate();

			$("#resDay").val(selectedDate);
			
			jQuery('.res_wrap').fadeIn('slow');
				
			setDate();
		}
	
		function setDate(){

			var dateArea = document.getElementById("resrvAddDate");
			var dateMonth = document.getElementById("dateMonth").innerText;
			var dateDay = document.getElementById("dateDay").innerText;

			dateArea.innerText = dateMonth + "월 " + dateDay + "일 ";
		}

	</script>
	<script>
		function submitRes(){
			document.getElementById("resForm").submit();
			}

	</script>

</body>
</html>