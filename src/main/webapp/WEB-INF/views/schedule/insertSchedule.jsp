<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
<style>
	    #main-content{
            margin-left: 300px;
        }
        table {
            display: inline-block;
            margin-top: -300px;
            margin-left: 200px;
            width: 1000px;
        }
        td{
            width: 600px;
            border: lightgray 1px solid;
        }
        th{
            width: 100px;
            background-color: #27334C;
            color: lightgray;
        }
        #saveBtn{
            display: inline-block;
        }
        #cancelBtn{
            display: inline-block;
        }
</style>
</head>
<body>
	<jsp:include page="../common/scheduleAside.jsp" />
	<jsp:include page="../common/operaNav.jsp" />
	<section>
	   <!-- main-content -->
    <div id="main-content">
    <form id="schedulePost" action="insertSchedule.sch" method="POST">
        <table>
            <tr>
                <th>일자</th>
                <td><input type="date" id="startDate"> - <input type="date" id="endDate"> 
                <input type="checkbox" id="allDay">종일
                </td>
            </tr>
            <tr>
                <th>제목</th>
                <td><input type="text" id="title" name="title"></td>
            </tr>
            <tr>
                <th>일정타입</th>
                <td>
                    <select>
                        <option id="work">업무</option>
                        <option id="goWork">출장</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th>장소</th>
                <td><input type="text" id="location"></td>
            </tr>
            <tr>
                <th>알림</th>
                <td><input type="checkbox" id="alert">d-day</td>
            </tr>
            <tr>
                <th>반복설정</th>
                <td><input type="button" value="반복설정" onclick="" id="repeat"></td>
            </tr>
            <tr>
                <th>캘린더</th>
                <td>
                    <select>
                        <option id="deptCal">부서캘린더</option>
                        <option id="perCal">마이캘린더</option>
                    </select>
                    <select>
                        <option>회의실</option>
                        <option>휴가공유</option>
                        <option>디자인실</option>
                        <option>병원</option>
                        <option>휴가</option>
                        <option>주식</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th>내용</th>
                <td><textarea cols="100" rows="25" style="resize: none;" id="detail" name="detail"></textarea></td>
            </tr>

        </table>
	        
	        <input type="button" id="saveBtn" value="저장" onclick="submitSchedule();">
	        <input type="button" id="cancelBtn" value="취소" onclick="">
	        </form>
    	</div>
    	
    	<script>
    		function submitSchedule(){
    			scheduleForm.submit();
    		}
    	</script>
    </section>
</body>
</html>