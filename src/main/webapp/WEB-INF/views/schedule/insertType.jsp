<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Opera GroupWare</title>
<style>
     #main-content{
            margin-left: 300px;
        }
        table {
            display: inline-block;
            margin-top: -300px;
            margin-left: 200px;
            width: 1000px;
        }
        td{
            width: 600px;
            border: lightgray 1px solid;
        }
        th{
            width: 100px;
            background-color: #27334C;
            color: lightgray;
        }
        #saveBtn{
            display: inline-block;
        }
        #cancelBtn{
            display: inline-block;
        }
       
</style>
</head>
<body>
	<jsp:include page="../common/scheduleAside.jsp" />
	<jsp:include page="../common/operaNav.jsp" />
	<section>
		<div id="main-content">
        <table>
            <tr>
                <th>이름</th>
                <td><input type="text">
                </td>
            </tr>
            <tr>
                <th>색상</th>
                <td><input type="color"></td>
            </tr>
            <tr>
                <th>내용</th>
                <td><textarea cols="100" rows="25" style="resize: none;"></textarea></td>
            </tr>
           

        </table>
        
        <input type="button" id="saveBtn" value="저장" onclick="">
        <input type="button" id="cancelBtn" value="취소" onclick="">
    	</div>
	</section>
</body>
</html>