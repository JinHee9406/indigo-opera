
var socket = io()
var roomNumber;


socket.on('update', function(data) {
    var chat = document.getElementById('chat')
    //var message = document.createElement('div')//이 부분에서 메세지 div를 html 에 생성
    //var node = document.createTextNode(`${data.realName}`+" : "+`${data.message}`)
    //var className = ''
    // 타입에 따라 적용할 클래스를 다르게 지정
    console.log(data);

    switch(data.type) {
      case '문자':
        $("#chat").append("<div class='otherName'>"+data.realName+"<br></div><div class='other'>"+data.message+"</div>")
        break
      case '사진':
        $("#chat").append("<div class='otherName'>"+data.realName+"<br></div><div class='other'><img src='/resources/uploadFiles/"+data.message+"'></div>");
        break
      case 'connect':
        $("#chat").append("<div class='connect'>"+data.realName+": "+data.message+"</div>")
        break
      case 'disconnect':
        $("#chat").append("<div class='disconnect'>"+data.realName+": "+data.message+"</div>")
        break
    }
    /*message.classList.add(className)
    message.appendChild(node)
    chat.appendChild(message)*/
    chat.scrollTop = chat.scrollHeight;
  })

  

  socket.on('loadMessage', function(data) {
    var chat = document.getElementById('chat')
   // var message = document.createElement('div')//이 부분에서 메세지 div를 html 에 생성
    //var node = document.createTextNode(`${data.CHAT_EMPNO}: ${data.CHAT_DETAIL}`)
    //var className = ''
    if(data != null){
      if(data.CHAT_EMPNO != name){
        // className = 'other'
        switch(data.CHAT_TYPE){
          case '문자':
              $("#chat").append("<div class='otherName'>"+data.CHAT_EMPNAME+"<br></div><div class='other'>"+data.CHAT_DETAIL+"</div>")
            break;
          case '사진':
              $("#chat").append("<div class='otherName'>"+data.CHAT_EMPNAME+"<br></div><div class='other'><img src='/resources/uploadFiles/"+data.CHAT_DETAIL+"'></div>");
            break;
          default:
              $("#chat").append("<div class='otherName'>"+data.CHAT_EMPNAME+"<br></div><div class='other'>"+data.CHAT_DETAIL+"</div>")
            break;
        }
        
       } else {

        switch(data.CHAT_TYPE){
          case '문자':
              $("#chat").append("<div class='me'>"+data.CHAT_DETAIL+"</div>")
            break;
          case '사진':
              $("#chat").append("<div class='me'><img src='/resources/uploadFiles/"+data.CHAT_DETAIL+"'></div>");
            break;
          default:
              $("#chat").append("<div class='me'>"+data.CHAT_DETAIL+"</div>")
            break;
        }
      
       }
    }
     chat.scrollTop = chat.scrollHeight;
  })

  //아이디별 채팅방 select결과 출력
  socket.on('callRoomList', function(data){
    $("#chatroomLists").children().remove();
    var dataString = data.rows.toString();
    var dataElements = dataString.split(",");
    for(var i = 0; i < dataElements.length; i++){
      $("#chatroomLists").append("<div id='room"+data.rows[i].CHATROOM_NO.toString()+"' class='chatroom' onclick='selectRoom("+data.rows[i].CHATROOM_NO.toString()+");'>"+data.rows[i].CHATROOM_HOST+"</div>");
      room.push(data.rows[i].CHATROOM_NO);
    }
  })

  socket.on('removeRoomList', function(){
    $("#chatroomLists").children().remove();
  })

  socket.on('moveNewRoom', function(data){
    var roomNumber = data.rows[0][0];
    socket.emit('checkRoomUpdate');
    socket.emit('firstConn', name, realName);
    changeNewRoom(roomNumber);
  })

  socket.on('checkRoomUpdate', function(){
    socket.emit('firstConn', name, realName);
  })

  socket.on('checkUserName', function(data){
    document.getElementById("loginUserCon").innerText = data;
    selectedMemberName += data+",";
  })

  socket.on('loadDeptTree', function(data){
    var deptList = data.rows.toString().split(',');
    for(var i = 0; i < deptList.length ; i++){
      switch(data.rows[i].DEPT_TYPE){
        case "본부":
          $("#lanker").append("<li><a href='#none'>"+data.rows[i].DEPT_NAME+"</a><ul class='depth_4' id='"+data.rows[i].DEPT_NAME+"'></ul></li>")
          break;
        case "내부":
          $("#inner").append("<li><a href='#none'>"+data.rows[i].DEPT_NAME+"</a><ul class='depth_4' id='"+data.rows[i].DEPT_NAME+"'></ul></li>")
          break;
        case "외부":
          $("#outer").append("<li><a href='#none'>"+data.rows[i].DEPT_NAME+"</a><ul class='depth_4' id='"+data.rows[i].DEPT_NAME+"'></ul></li>")
          break;
      }
    }
    $("#lanker>li:last").attr("class", "end");
    $("#inner>li:last").attr("class", "end");
    $("#outer>li:last").attr("class", "end");
  })

  socket.on('loadUserTree', function(data){
    var userList = data.rows.toString().split(',');
    for(var i = 0; i < userList.length ; i++){
      if(name != data.rows[i].EMP_NO){
        console.log(data.rows[i].EMP_NO)
        $("#"+data.rows[i].DEPT_NAME).append("<li><div class='memberEle'>"+data.rows[i].EMP_NAME+" "+data.rows[i].JOB_NAME+
        "<input class='checkEmp' name='checkEmp' type='checkbox' value="+data.rows[i].EMP_NO+","+data.rows[i].EMP_NAME+","+data.rows[i].JOB_NAME+"></div></li>")
      }
    }
  })

  function sendFile() {
    var form = $('#fileForm')[0];
    var formData = new FormData(form);

    $.ajax({
        type: 'post',
        url: '/upload',
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
          var changedFileName = data.substr(29,data.length);
          var type = '사진';
          $("#chat").append("<div class='me'><img src='/resources/uploadFiles/"+changedFileName+"'></div>");
          chat.scrollTop = chat.scrollHeight;
          socket.emit('message', {type: 'message', num: roomNumber, userName: realName ,message: changedFileName, type: type})
        },
        error: function (err) {
            console.log(err);
        }
    });
  }
  var fileName;

  function sendFile2(filename) {
    console.log("파일전송");
    var form = $('#fileForm')[0];
    var formData = new FormData(form);
    fileName = filename;
    $.ajax({
        type: 'post',
        url: '/upload2',
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
          console.log("출력");
          console.log(data);
          var type = '파일';
          $("#chat").append("<div class='me' onclick='fileDownload("+data.fileName+");'>"+data.fileName+"</div>");
          chat.scrollTop = chat.scrollHeight;
          socket.emit('message', {type: 'message', num: roomNumber, userName: realName ,message: changedFileName, type: type})
        },
        error: function (err) {
            console.log(err);
        }
    });
  }
//전송 함수
function send(){
  // 입력되어있는 데이터 가져오기
  var message = document.getElementById('test').value
  var type = '문자';
  // 가져왔으니 데이터 빈칸으로 변경
  document.getElementById('test').value = ''
  $("#chat").append("<div class='me'>"+message+"</div>")
    socket.emit('message', {type: 'message', num: roomNumber, userName: realName ,message: message, type: type})
    chat.scrollTop = chat.scrollHeight;
}

function fileDownload(nameData){

  var fileOriginName = nameData;

  $.ajax({
    type: 'get',
    url: '/download',
    data: {
      filename: fileOriginName
    },
    processData: false,
    contentType: false,
    success: function (data) {
      console.log(data);
    },
    error: function (err) {
        console.log(err);
    }
});
}

