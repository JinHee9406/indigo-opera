package com.indigo.opera.knowledge.model.exception;

public class addKnowledgeException extends Exception{
	public addKnowledgeException(String msg) {
		super(msg);
	}
}
