package com.indigo.opera.knowledge.model.service;

import java.util.ArrayList;

import com.indigo.opera.knowledge.model.exception.addKnowReplyException;
import com.indigo.opera.knowledge.model.exception.addKnowledgeException;
import com.indigo.opera.knowledge.model.exception.fixKnowledgeException;
import com.indigo.opera.knowledge.model.exception.knowledgeListException;
import com.indigo.opera.knowledge.model.exception.knowledgeOneException;
import com.indigo.opera.knowledge.model.vo.Knowledge;
import com.indigo.opera.knowledge.model.vo.PageInfo;
import com.indigo.opera.knowledge.model.vo.Reply;
import com.indigo.opera.knowledge.model.vo.SearchCondition;

public interface KnowledgeService {
	public ArrayList<Knowledge> showKnowledgeList(PageInfo pi) throws knowledgeListException;
	
	public ArrayList<Knowledge> typeKnowledgeList(PageInfo pi, String knowType) throws knowledgeListException;

	public ArrayList<Knowledge> searchKnowledgeList(PageInfo pi, SearchCondition sc) throws knowledgeListException;
	
	public int addKnowledge(Knowledge know) throws addKnowledgeException;

	public Knowledge selectOneKnowledge(String knowledgeNum) throws knowledgeOneException;

	public int fixKnowledge(Knowledge know) throws fixKnowledgeException;

	public ArrayList<Reply> selectReplyListOne(String knowledgeNum);

	public int addKnowledgeReply(Reply reply) throws addKnowReplyException;

	public int getListSize();
	
	public int getListSize(String knowType);
	
	public int getListSize(SearchCondition searchCondition);
	
	public int updateViewCount(Knowledge knowledgeDetail);

	public int removeKnowledge(String knowNum);


}
