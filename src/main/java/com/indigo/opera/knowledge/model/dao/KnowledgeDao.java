package com.indigo.opera.knowledge.model.dao;

import java.util.ArrayList;

import org.mybatis.spring.SqlSessionTemplate;

import com.indigo.opera.knowledge.model.exception.addKnowReplyException;
import com.indigo.opera.knowledge.model.exception.addKnowledgeException;
import com.indigo.opera.knowledge.model.exception.fixKnowledgeException;
import com.indigo.opera.knowledge.model.exception.knowledgeListException;
import com.indigo.opera.knowledge.model.exception.knowledgeOneException;
import com.indigo.opera.knowledge.model.vo.Knowledge;
import com.indigo.opera.knowledge.model.vo.PageInfo;
import com.indigo.opera.knowledge.model.vo.Reply;
import com.indigo.opera.knowledge.model.vo.SearchCondition;

public interface KnowledgeDao {
	ArrayList<Knowledge> showKnowledgeList(SqlSessionTemplate sqlSession, PageInfo pi) throws knowledgeListException;

	ArrayList<Knowledge> typeKnowledgeList(SqlSessionTemplate sqlSession, PageInfo pi, String knowType) throws knowledgeListException;
	 
	ArrayList<Knowledge> searchKnowledgeList(SqlSessionTemplate sqlSession, PageInfo pi, SearchCondition sc)  throws knowledgeListException;
	
	int addKnowledge(SqlSessionTemplate sqlSession, Knowledge know) throws addKnowledgeException;

	Knowledge selectOneKnowledge(SqlSessionTemplate sqlSession, String knowledgeNum) throws knowledgeOneException;

	int fixKnowledge(SqlSessionTemplate sqlSession, Knowledge know) throws fixKnowledgeException;

	ArrayList<Reply> selectReplyListOne(SqlSessionTemplate sqlSession, String knowledgeNum);

	int addKnowledgeReply(SqlSessionTemplate sqlSession, Reply reply) throws addKnowReplyException;

	int getListSize(SqlSessionTemplate sqlSession);
	
	int getListSize(SqlSessionTemplate sqlSession, String knowType);

	int getListSize(SqlSessionTemplate sqlSession, SearchCondition searchCondition);
	
	int updateViewCount(SqlSessionTemplate sqlSession, Knowledge knowledgeDetail);

	int removeKnowledge(SqlSessionTemplate sqlSession, String knowNum);




}
