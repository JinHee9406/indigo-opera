package com.indigo.opera.knowledge.model.vo;

import java.sql.Date;

public class Knowledge {
	private String knowledgeNum;
	private int postNum;
	private String knowledgeTitle;
	private String knowledgeDetail;
	private Date knowledgeDate;
	private int showpostCnt;
	private String knowledgeType;
	private String knowledgeUser;
	private String knowledgeUserName;
	
	public Knowledge() {}

	public Knowledge(String knowledgeNum, int postNum, String knowledgeTitle, String knowledgeDetail,
			Date knowledgeDate, int showpostCnt, String knowledgeType, String knowledgeUser, String knowledgeUserName) {
		super();
		this.knowledgeNum = knowledgeNum;
		this.postNum = postNum;
		this.knowledgeTitle = knowledgeTitle;
		this.knowledgeDetail = knowledgeDetail;
		this.knowledgeDate = knowledgeDate;
		this.showpostCnt = showpostCnt;
		this.knowledgeType = knowledgeType;
		this.knowledgeUser = knowledgeUser;
		this.knowledgeUserName = knowledgeUserName;
	}

	public String getKnowledgeNum() {
		return knowledgeNum;
	}

	public void setKnowledgeNum(String knowledgeNum) {
		this.knowledgeNum = knowledgeNum;
	}

	public int getPostNum() {
		return postNum;
	}

	public void setPostNum(int postNum) {
		this.postNum = postNum;
	}

	public String getKnowledgeTitle() {
		return knowledgeTitle;
	}

	public void setKnowledgeTitle(String knowledgeTitle) {
		this.knowledgeTitle = knowledgeTitle;
	}

	public String getKnowledgeDetail() {
		return knowledgeDetail;
	}

	public void setKnowledgeDetail(String knowledgeDetail) {
		this.knowledgeDetail = knowledgeDetail;
	}

	public Date getKnowledgeDate() {
		return knowledgeDate;
	}

	public void setKnowledgeDate(Date knowledgeDate) {
		this.knowledgeDate = knowledgeDate;
	}

	public int getShowpostCnt() {
		return showpostCnt;
	}

	public void setShowpostCnt(int showpostCnt) {
		this.showpostCnt = showpostCnt;
	}

	public String getKnowledgeType() {
		return knowledgeType;
	}

	public void setKnowledgeType(String knowledgeType) {
		this.knowledgeType = knowledgeType;
	}

	public String getKnowledgeUser() {
		return knowledgeUser;
	}

	public void setKnowledgeUser(String knowledgeUser) {
		this.knowledgeUser = knowledgeUser;
	}

	public String getKnowledgeUserName() {
		return knowledgeUserName;
	}

	public void setKnowledgeUserName(String knowledgeUserName) {
		this.knowledgeUserName = knowledgeUserName;
	}

	@Override
	public String toString() {
		return "Knowledge [knowledgeNum=" + knowledgeNum + ", postNum=" + postNum + ", knowledgeTitle=" + knowledgeTitle
				+ ", knowledgeDetail=" + knowledgeDetail + ", knowledgeDate=" + knowledgeDate + ", showpostCnt="
				+ showpostCnt + ", knowledgeType=" + knowledgeType + ", knowledgeUser=" + knowledgeUser
				+ ", knowledgeUserName=" + knowledgeUserName + "]";
	}
	
	
}
