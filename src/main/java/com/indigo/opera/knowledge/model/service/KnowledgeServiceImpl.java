package com.indigo.opera.knowledge.model.service;

import java.util.ArrayList;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indigo.opera.knowledge.model.dao.KnowledgeDao;
import com.indigo.opera.knowledge.model.exception.addKnowReplyException;
import com.indigo.opera.knowledge.model.exception.addKnowledgeException;
import com.indigo.opera.knowledge.model.exception.fixKnowledgeException;
import com.indigo.opera.knowledge.model.exception.knowledgeListException;
import com.indigo.opera.knowledge.model.exception.knowledgeOneException;
import com.indigo.opera.knowledge.model.vo.Knowledge;
import com.indigo.opera.knowledge.model.vo.PageInfo;
import com.indigo.opera.knowledge.model.vo.Reply;
import com.indigo.opera.knowledge.model.vo.SearchCondition;
@Service
public class KnowledgeServiceImpl implements KnowledgeService{
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private KnowledgeDao knowledgeDao;
	
	@Override
	public int getListSize() {
		int size = knowledgeDao.getListSize(sqlSession);
		return size;
	}
	
	@Override
	public int getListSize(String knowType) {
		int size = knowledgeDao.getListSize(sqlSession, knowType);
		return size;
	}

	@Override
	public int getListSize(SearchCondition searchCondition) {
		int size = knowledgeDao.getListSize(sqlSession, searchCondition);
		return size;
	}
	
	@Override
	public ArrayList<Knowledge> showKnowledgeList(PageInfo pi) throws knowledgeListException {
		ArrayList<Knowledge> knowledgeList = null;
		
		knowledgeList = knowledgeDao.showKnowledgeList(sqlSession, pi);
		
		return knowledgeList;
	}
	
	@Override
	public ArrayList<Knowledge> typeKnowledgeList(PageInfo pi, String knowType) throws knowledgeListException {
		ArrayList<Knowledge> knowledgeList = null;
		
		knowledgeList = knowledgeDao.typeKnowledgeList(sqlSession, pi, knowType);
		
		return knowledgeList;
	}
	
	@Override
	public ArrayList<Knowledge> searchKnowledgeList(PageInfo pi, SearchCondition sc) throws knowledgeListException {

		ArrayList<Knowledge> knowledgeList = null;
		
		knowledgeList = knowledgeDao.searchKnowledgeList(sqlSession, pi, sc);
		
		return knowledgeList;
	}
	
	@Override
	public int addKnowledge(Knowledge know) throws addKnowledgeException {
		
		int result = knowledgeDao.addKnowledge(sqlSession, know);
		
		return result;
	}

	@Override
	public Knowledge selectOneKnowledge(String knowledgeNum) throws knowledgeOneException {
		
		Knowledge result = knowledgeDao.selectOneKnowledge(sqlSession, knowledgeNum);
		
		return result;
	}

	@Override
	public int fixKnowledge(Knowledge know) throws fixKnowledgeException {
		
		int result = knowledgeDao.fixKnowledge(sqlSession, know);
		
		return result;
	}

	@Override
	public ArrayList<Reply> selectReplyListOne(String knowledgeNum) {
		
		ArrayList<Reply> list = knowledgeDao.selectReplyListOne(sqlSession, knowledgeNum);
		
		return list;
	}

	@Override
	public int addKnowledgeReply(Reply reply) throws addKnowReplyException {
		
		int result = knowledgeDao.addKnowledgeReply(sqlSession, reply);
		
		return result;
	}

	@Override
	public int updateViewCount(Knowledge knowledgeDetail) {
		
		int result = knowledgeDao.updateViewCount(sqlSession, knowledgeDetail);
		
		return result;
	}

	@Override
	public int removeKnowledge(String knowNum) {
		
		int result = knowledgeDao.removeKnowledge(sqlSession, knowNum);
		
		return result;
	}




}
