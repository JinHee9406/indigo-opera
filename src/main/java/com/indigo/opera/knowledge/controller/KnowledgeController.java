package com.indigo.opera.knowledge.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.indigo.opera.common.Pagination;
import com.indigo.opera.knowledge.model.exception.addKnowReplyException;
import com.indigo.opera.knowledge.model.exception.addKnowledgeException;
import com.indigo.opera.knowledge.model.exception.fixKnowledgeException;
import com.indigo.opera.knowledge.model.exception.knowledgeListException;
import com.indigo.opera.knowledge.model.exception.knowledgeOneException;
import com.indigo.opera.knowledge.model.service.KnowledgeService;
import com.indigo.opera.knowledge.model.vo.Knowledge;
import com.indigo.opera.knowledge.model.vo.PageInfo;
import com.indigo.opera.knowledge.model.vo.Reply;
import com.indigo.opera.knowledge.model.vo.SearchCondition;

@Controller
public class KnowledgeController {
	
	@Autowired
	private KnowledgeService knowService;
	
	@RequestMapping("knowledgeMain.know")
	public String showKnowledgePage(Model model, String pageNum) {
		
		int currentPage;
		if(pageNum == null) {
			currentPage = 1;
		} else {
			currentPage = Integer.parseInt(pageNum);
		}

		ArrayList<Knowledge> knowledge;
		try {

			int listCount = knowService.getListSize();
			
			PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
			
			knowledge = knowService.showKnowledgeList(pi);
			
			if(knowledge.size() > 0) {
				int postNumber = pi.getCurFirstNum();
				
				for(int i=0; i < knowledge.size(); i++) {
					knowledge.get(i).setPostNum(postNumber--);
				}
				
				model.addAttribute("knowledge", knowledge);
				model.addAttribute("pi", pi);
				model.addAttribute("Typelist", "not");
				
				return "knowledge/knowledgeMain";
			}else {
				String message = "공유글이 없습니다!";
				String key = "notPost";
				
				model.addAttribute("message", message);
				model.addAttribute("key", key);
				return "knowledge/knowledgeResult";
			}
		
		} catch (knowledgeListException e) {
			e.printStackTrace();
			
			return "knowledge/knowledgeMain";
		}
			
		
	}
	
	@RequestMapping("knowledgeType.know")
	public String typeKnowledgePage(Model model, String pageNum, String knowType) {
		
		int currentPage;
		if(pageNum == null) {
			currentPage = 1;
		} else {
			currentPage = Integer.parseInt(pageNum);
		}

		ArrayList<Knowledge> knowledge;
		try {

			int listCount = knowService.getListSize(knowType);
			
			PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
			
			knowledge = knowService.typeKnowledgeList(pi, knowType);
			
			if(knowledge.size() > 0) {
				int postNumber = pi.getCurFirstNum();
				
				for(int i=0; i < knowledge.size(); i++) {
					knowledge.get(i).setPostNum(postNumber--);
				}
				
				model.addAttribute("knowledge", knowledge);
				model.addAttribute("pi", pi);
				model.addAttribute("Typelist", knowType);
				
				return "knowledge/knowledgeMain";
			}else {
				String message = "공유글이 없습니다!";
				String key = "notPostType";
				
				model.addAttribute("message", message);
				model.addAttribute("key", key);
				return "knowledge/knowledgeResult";
			}
			
			
		} catch (knowledgeListException e) {
			e.printStackTrace();
			
			return "knowledge/knowledgeMain";
		}
			
		
	}
	
	
	@RequestMapping("knowledgeSearch.know")
	public String searchKnowledgePage(Model model, String pageNum, String searchType, String searchText) {
		
		int currentPage;
		if(pageNum == null) {
			currentPage = 1;
		} else {
			currentPage = Integer.parseInt(pageNum);
		}

		ArrayList<Knowledge> knowledge;
		try {
			
			SearchCondition sc = new SearchCondition();
			if(searchType.equals("writer")) {
				sc.setWriter(searchText);
			} else if(searchType.equals("title")) {
				sc.setTitle(searchText);
			} else if(searchType.equals("content")) {
				sc.setContent(searchText);
			}
			
			int listCount = knowService.getListSize(sc);
			
			PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
			
			knowledge = knowService.searchKnowledgeList(pi, sc);
			
			if(knowledge.size() > 0) {
				int postNumber = pi.getCurFirstNum();
				
				for(int i=0; i < knowledge.size(); i++) {
					knowledge.get(i).setPostNum(postNumber--);
				}
				
				model.addAttribute("knowledge", knowledge);
				model.addAttribute("pi", pi);
				model.addAttribute("searchlist", sc);
				
				return "knowledge/knowledgeMain";
			}else {
				String message = "검색결과가 없습니다!";
				String key = "notPostType";
				
				model.addAttribute("message", message);
				model.addAttribute("key", key);
				return "knowledge/knowledgeResult";
			}
			
			
		} catch (knowledgeListException e) {
			e.printStackTrace();
			
			return "knowledge/knowledgeMain";
		}
	}
	
	
	
	@RequestMapping("knowledgeAdd.know")
	public String showKnowledgeAddPage() {
		return "knowledge/knowledgeAdd";
	}
	
	
	@RequestMapping("newKnowledge.know")
	public String addKnowledgeResult(Model model, Knowledge know) {
		
		try {
			int result = knowService.addKnowledge(know);
			
			String message = "공유글 작성을 완료했습니다!";
			String key = "addKnowledge";
			
			model.addAttribute("message", message);
			model.addAttribute("key", key);
			return "knowledge/knowledgeResult";
		} catch (addKnowledgeException e) {
			e.printStackTrace();
			return " ";
		}
	}
	
	
	@RequestMapping("knowledgeDetail.know")
	public String showKnowledgeDetailPage(Model model, String knowledgeNum) {
		try {
			Knowledge knowledgeDetail = knowService.selectOneKnowledge(knowledgeNum);
			
			ArrayList<Reply> replyList = knowService.selectReplyListOne(knowledgeDetail.getKnowledgeNum());
			
			knowledgeDetail.setShowpostCnt(knowledgeDetail.getShowpostCnt() + 1);
			
			int addViews = knowService.updateViewCount(knowledgeDetail);
			
			model.addAttribute("knowledge", knowledgeDetail);
			
			model.addAttribute("knowledgeReply", replyList);
			
		} catch (knowledgeOneException e) {
			e.printStackTrace();
		}
		
		
		return "knowledge/knowledgeDetail";
	}
	
	
	
	
	@RequestMapping("knowledgeFix.know")
	public String showKnowledgeFixPage(Model model, Knowledge knowledge) {
		
		model.addAttribute("knowledge", knowledge);
			
		return "knowledge/knowledgeFix";
	}
	
	
	@RequestMapping("updateKnowledge.know")
	public String fixKnowledgeResult(Model model, Knowledge know) {
			try {
				int result = knowService.fixKnowledge(know);
				
				model.addAttribute("knowledge",know);
				
			} catch (fixKnowledgeException e) {
				e.printStackTrace();
			}
			return "knowledge/knowledgeDetail";

	}
	
	@RequestMapping("addKnowledgeReply.know")
	public String addKnowledgeReply(Model model, Reply reply) {
		
		try {
			int result = knowService.addKnowledgeReply(reply);
			String message = "댓글 작성을 완료했습니다!";
			String key = "addReply";
			
			model.addAttribute("message", message);
			model.addAttribute("key", key);
			model.addAttribute("KnowNum", reply.getKnowledgeNum());
			return "knowledge/knowledgeResult";
			
		} catch (addKnowReplyException e) {
			e.printStackTrace();
			
			return "knowledge/knowledgeMain";
		}
	}
	
	@RequestMapping("removeKnowledge.know")
	public String removeKnowledge(Model model, String knowNum) {
			
		int result = knowService.removeKnowledge(knowNum);
		
		String message = "게시글이 삭제되었습니다!";
		String key = "removeReply";
		
		model.addAttribute("message", message);
		model.addAttribute("key", key);
		return "knowledge/knowledgeResult";
	}
}
