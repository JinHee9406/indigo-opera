package com.indigo.opera.main.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.indigo.opera.attendance.model.service.AttendService;
import com.indigo.opera.attendance.model.vo.Attendance;
import com.indigo.opera.common.CommonUtils;
import com.indigo.opera.fileList.model.service.FileListService;
import com.indigo.opera.fileList.model.vo.FileList;
import com.indigo.opera.main.model.service.MainPageService;
import com.indigo.opera.main.model.vo.MainApp;
import com.indigo.opera.main.model.vo.MainKnow;
import com.indigo.opera.main.model.vo.MainNotice;
import com.indigo.opera.main.model.vo.MainRes;
import com.indigo.opera.member.model.vo.Member;
import com.indigo.opera.notice.model.vo.Board;
import com.indigo.opera.shareworks.model.service.ShareWorkService;
import com.indigo.opera.shareworks.model.vo.Work;


@Controller
public class mainPageController {
	@Autowired
	private AttendService as;
	
	@Autowired
	private MainPageService mainService;
	
	@Autowired
	private FileListService fileService;
	
	@Autowired
	private ShareWorkService shareWorkService;
	
	@RequestMapping("goMain.main")
	public String goMainPage(HttpServletRequest request) {
		
		int check = 0;
		Member login = (Member) request.getSession().getAttribute("loginUser");
		ArrayList<Object> alist = as.selectAttendanceList(login.getEmpNo());
		if(!alist.isEmpty()) {
			Attendance a = (Attendance)alist.get(alist.size() - 1);
			SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd");
			Date d = new Date();
			String checkDate = f.format(d);
			String myDate = f.format(a.getWorkTime());
			int cint = Integer.parseInt(checkDate);
			int mint = Integer.parseInt(myDate);
			
			if(cint != mint) {
				check = 1;
				request.getSession().setAttribute("attendanceChecklist", null);
			}
			if(alist.isEmpty()) {
				check = 1;
				request.getSession().setAttribute("attendanceChecklist", null);
			}
			if(check == 0) {
				request.getSession().setAttribute("attendanceChecklist", alist);
			}
			
		}
		
		request.getSession().setAttribute("attendanceCheck", check);
		return "main/mainPage";
	}
	
	@RequestMapping("insertProfileImg.main")
	public String addMeetingRoom(Model model, HttpServletRequest request, MultipartFile photo, String userEmpNo) {
		 
		String root = request.getSession().getServletContext().getRealPath("resources");
		String filePath = root + "\\uploadFiles";
		
		String root2 = request.getSession().getServletContext().getRealPath("messengerView");
		//메신저 폴더 저장용 경로
		String filePath2 = root2 + "\\static\\resources\\uploadFiles";
		
		String originFileName = photo.getOriginalFilename();
		String ext = originFileName.substring(originFileName.lastIndexOf("."));
		String changeName = CommonUtils.getRandomString();
		
		System.out.println("변환파일명 " + changeName);
		
		try {
			File file = new File(filePath + "\\" + changeName + ext);
			
			photo.transferTo(file);

			File copyFile = new File(filePath2 + "\\" + changeName + ext);
			 
			
			FileInputStream fis = new FileInputStream(file); //읽을파일
			
	        FileOutputStream fos = new FileOutputStream(copyFile); //복사할파일
			
	        int fileByte = 0; 
            // fis.read()가 -1 이면 파일을 다 읽은것
            while((fileByte = fis.read()) != -1) {
                fos.write(fileByte);
            }
            //자원사용종료
            fis.close();
            fos.close();
			
			FileList imgFile = new FileList();
			imgFile.setFilePath("\\uploadFiles");
			imgFile.setOriginName(originFileName);
			imgFile.setChangeName(changeName + ext);
			imgFile.setFileSize(String.valueOf(photo.getSize()));
			
			int fileResult = fileService.addFile(imgFile);
			//room.setRoomFile(String.valueOf(fileResult));
			Member param = new Member();
			param.setEmpNo(userEmpNo);
			param.setFileCode(String.valueOf(fileResult));
			
			int result = mainService.updateUserFile(param);
		} catch (Exception e) {
			new File(filePath + "\\" + changeName + ext).delete();
			
			String message = "파일 추가 오류";
			System.out.println(message);
			model.addAttribute("message", message);
			return "main/mainPage";
			
		}

		return "main/mainPage";
	}
	
	@RequestMapping("loadKnowledges.main")
	public ModelAndView loadKnowledges(ModelAndView mv) {
		
		ArrayList<MainKnow> simples = mainService.loadKnowledge();

		mv.addObject("Knows", simples);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("loadWorks.main")
	public ModelAndView loadWorks(ModelAndView mv, String loginUser) {
		
		ArrayList<Work> resultList = shareWorkService.selectWorkList(loginUser);

		mv.addObject("Works", resultList);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("loadNotice.main")
	public ModelAndView loadNotice(ModelAndView mv) {
		
		ArrayList<Board> resultList = mainService.loadNoticeList();

		ArrayList<MainNotice> resultNotice = new ArrayList<MainNotice>();
		
		for(int i = 0; i < resultList.size(); i++) {
			
			MainNotice elements = new MainNotice();
			
			Date from = resultList.get(i).getBoardDate();

			SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd");

			String to = transFormat.format(from);
			
			elements.setBoardNo(resultList.get(i).getBoardNo());
			elements.setBoardTitle(resultList.get(i).getBoardTitle());
			elements.setBoardDate(to);
			
			resultNotice.add(elements);
		}
		
		mv.addObject("Notices", resultNotice);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("loadRes.main")
	public ModelAndView loadRes(ModelAndView mv, String loginUser, String today) {
		
		String inToday = today.substring(2,today.length());
		
		HashMap<String, String> param = new HashMap<String, String>();
		param.put("empNo", loginUser);
		param.put("resDate", inToday);
		
		ArrayList<MainRes> resultList = mainService.selectResList(param);

		mv.addObject("Reservation", resultList);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("loadApp.main")
	public ModelAndView loadApp(ModelAndView mv, String loginUser) {
				
		ArrayList<MainApp> resultList = mainService.selectAppList(loginUser);

		mv.addObject("Approval", resultList);
		mv.setViewName("jsonView");
		return mv;
	}
}
