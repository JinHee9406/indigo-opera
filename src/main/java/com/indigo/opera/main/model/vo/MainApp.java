package com.indigo.opera.main.model.vo;

public class MainApp {
	private String loNo;
	private String draftNo;
	private String loStatus;
	private String draftStatus;
	
	public MainApp() {}

	public MainApp(String loNo, String draftNo, String loStatus, String draftStatus) {
		super();
		this.loNo = loNo;
		this.draftNo = draftNo;
		this.loStatus = loStatus;
		this.draftStatus = draftStatus;
	}

	public String getLoNo() {
		return loNo;
	}

	public void setLoNo(String loNo) {
		this.loNo = loNo;
	}

	public String getDraftNo() {
		return draftNo;
	}

	public void setDraftNo(String draftNo) {
		this.draftNo = draftNo;
	}

	public String getLoStatus() {
		return loStatus;
	}

	public void setLoStatus(String loStatus) {
		this.loStatus = loStatus;
	}

	public String getDraftStatus() {
		return draftStatus;
	}

	public void setDraftStatus(String draftStatus) {
		this.draftStatus = draftStatus;
	}

	
}
