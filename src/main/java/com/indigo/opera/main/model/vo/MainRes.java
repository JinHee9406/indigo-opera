package com.indigo.opera.main.model.vo;

public class MainRes {
	private String resNo;
	private String sDate;
	private String eDate;
	private String room;
	private String empNo;
	private String roomName;
	
	public MainRes() {}

	public MainRes(String sDate, String eDate, String room, String empNo) {
		super();
		this.sDate = sDate;
		this.eDate = eDate;
		this.room = room;
		this.empNo = empNo;
	}

	public String getsDate() {
		return sDate;
	}

	public void setsDate(String sDate) {
		this.sDate = sDate;
	}

	public String geteDate() {
		return eDate;
	}

	public void seteDate(String eDate) {
		this.eDate = eDate;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	@Override
	public String toString() {
		return "MainRes [sDate=" + sDate + ", eDate=" + eDate + ", room=" + room + ", empNo=" + empNo + "]";
	}

	public String getResNo() {
		return resNo;
	}

	public void setResNo(String resNo) {
		this.resNo = resNo;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	
	 
}
