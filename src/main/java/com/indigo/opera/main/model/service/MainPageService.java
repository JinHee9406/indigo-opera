package com.indigo.opera.main.model.service;

import java.util.ArrayList;
import java.util.HashMap;

import com.indigo.opera.main.model.exception.mainException;
import com.indigo.opera.main.model.vo.MainApp;
import com.indigo.opera.main.model.vo.MainKnow;
import com.indigo.opera.main.model.vo.MainRes;
import com.indigo.opera.member.model.vo.Member;
import com.indigo.opera.notice.model.vo.Board;

public interface MainPageService {

	int updateUserFile(Member param) throws mainException;

	ArrayList<MainKnow> loadKnowledge();

	ArrayList<MainRes> selectResList(HashMap<String, String> param);

	ArrayList<Board> loadNoticeList();

	ArrayList<MainApp> selectAppList(String loginUser);

}
