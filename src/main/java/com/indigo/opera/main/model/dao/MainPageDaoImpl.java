package com.indigo.opera.main.model.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.indigo.opera.knowledge.model.exception.knowledgeListException;
import com.indigo.opera.main.model.exception.mainException;
import com.indigo.opera.main.model.vo.MainApp;
import com.indigo.opera.main.model.vo.MainKnow;
import com.indigo.opera.main.model.vo.MainRes;
import com.indigo.opera.member.model.vo.Member;
import com.indigo.opera.notice.model.vo.Board;

@Repository
public class MainPageDaoImpl implements MainPageDao{

	@Override
	public int updateUserFile(SqlSessionTemplate sqlSession, Member param) throws mainException {
		
		int result = sqlSession.update("Member.updateFileUser",param);
		
		if(result == 0) {
			throw new mainException("프로필 변경실패");
		}
		return result;	
	}

	@Override
	public ArrayList<MainKnow> loadKnowledge(SqlSessionTemplate sqlSession) {
		
		 ArrayList<MainKnow> result = (ArrayList) sqlSession.selectList("Knowledge.mainKnowledgeLoad");
		
		return result;
	}

	@Override
	public ArrayList<MainRes> selectResList(SqlSessionTemplate sqlSession, HashMap<String, String> param) {
		 
		ArrayList<MainRes> result = (ArrayList) sqlSession.selectList("Reservation.selectMainRes", param);
		
		return result;
	}

	@Override
	public ArrayList<Board> loadNoticeList(SqlSessionTemplate sqlSession) {
		ArrayList<Board> result = (ArrayList) sqlSession.selectList("Notice.selectAllList");
		return result;
	}

	@Override
	public ArrayList<MainApp> selectAppList(SqlSessionTemplate sqlSession, String loginUser) {
		ArrayList<MainApp> result = (ArrayList) sqlSession.selectList("Approval.selectAppList", loginUser);
		return result;
	}

}
