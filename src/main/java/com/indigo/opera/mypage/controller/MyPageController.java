package com.indigo.opera.mypage.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MyPageController {
	@RequestMapping("myPageMain.my")
	public String showMyinfoPage() {
		return "mypage/myInfo";
	}
	@RequestMapping("manageAlert.my")
	public String showManageAlertPage() {
		return "mypage/manageAlert";
	}
	@RequestMapping("manageLine.my")
	public String showManageLinePage() {
		return "mypage/manageLine";
	}
	@RequestMapping("manageReception.my")
	public String showManageReceptionPage() {
		return "mypage/manageReception";
	}
	@RequestMapping("bookMark.my")
	public String showBookMarkPage() {
		return "mypage/bookMark";
	}
}
