package com.indigo.opera.notice.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import com.indigo.opera.common.CommonUtils;
import com.indigo.opera.common.Pagination;
import com.indigo.opera.common.Pagination3;
import com.indigo.opera.fileList.model.service.FileListService;
import com.indigo.opera.fileList.model.vo.FileList;
import com.indigo.opera.knowledge.model.vo.PageInfo;
import com.indigo.opera.member.model.vo.Member;
import com.indigo.opera.notice.model.service.NoticeService;
import com.indigo.opera.notice.model.vo.Board;
import com.indigo.opera.notice.model.vo.Notice;
import com.indigo.opera.notice.model.vo.Reply;

@Controller
public class NoticeController {
   @Autowired
   private NoticeService ns;
   
   @Autowired
   private FileListService fileService;
   

   @RequestMapping("allNotice.not")
   public String showNoticeMainPage(Model model, String pageNo) {

      int currentPage;
      if (pageNo == null) {
         currentPage = 1;
      } else {
         currentPage = Integer.parseInt(pageNo);
      }
      ArrayList<Board> notice;

      int listCount = ns.getListSize("공지");

      PageInfo pi = Pagination.getPageInfo(currentPage, listCount);

      notice = ns.showAllNoticeList(pi);

      System.out.println(listCount);
      System.out.println(notice);

      if (notice.size() > 0) {
         model.addAttribute("notice", notice);
         model.addAttribute("pi", pi);
         model.addAttribute("Typelist", "not");

      } else {
         model.addAttribute("message", "현재 공지사항이 없습니다.");
      }

      return "notice/allNotice";

   }

   @RequestMapping("deptNotice.not")
   public String showDeptNoticeMainPage(Model model, String pageNo, String loginUser) {

      System.out.println("empdept : " + loginUser);

      int currentPage;
      if (pageNo == null) {
         currentPage = 1;
      } else {
         currentPage = Integer.parseInt(pageNo);
      }
      ArrayList<Board> notice;

      //int listCount = ns.getListSize("부서");
      int listCount = ns.getDeptListSize("부서",loginUser);

      PageInfo pi = Pagination.getPageInfo(currentPage, listCount);

      notice = ns.showDeptNoticeList(pi, loginUser);

      System.out.println(listCount);
      System.out.println(notice);
      
      String com = "dept";

      if (notice.size() > 0) {
         model.addAttribute("notice", notice);
         model.addAttribute("pi", pi);
         model.addAttribute("Typelist", com);

      } else {
         model.addAttribute("message", "현재 공지사항이 없습니다.");
      }

      return "notice/departmentNotice";
   }

   @RequestMapping("commonNotice.not")
   public String showCommonNoticeMainPage(Model model, String pageNo) {
      
      int currentPage;
      if (pageNo == null) {
         currentPage = 1;
      } else {
         currentPage = Integer.parseInt(pageNo);
      }
      ArrayList<Board> notice;

      int listCount = ns.getListSize("일반");

      PageInfo pi = Pagination.getPageInfo(currentPage, listCount);

      notice = ns.showCommonNoticeList(pi);

      System.out.println(listCount);
      System.out.println(notice);

      String com = "com";
      
      
      

      if (notice.size() > 0) {
         model.addAttribute("notice", notice);
         model.addAttribute("pi", pi);
         model.addAttribute("Typelist", com);

      } else {
         model.addAttribute("message", "현재 공지사항이 없습니다.");
      }

      return "notice/commonNotice";
   }

   @RequestMapping("allDetail.not")
   public String showAllDetailPage(String boardNo, Model model,String pageNo) {
      Board notice;

      notice = ns.showCommonDetail(boardNo);
      
      ArrayList<Object> fileList = null;
        if(notice.getFileCode() != null) {
           fileList = ns.selectFileList(notice.getFileCode());
        }

      if (notice != null) {
         model.addAttribute("notice", notice);
         model.addAttribute("Typelist", "not");
         model.addAttribute("fileList",fileList);
      } else {
         model.addAttribute("message", "현재 공지사항이 없습니다.");
      }
      
      int result2 = ns.increaseBoardCnt(boardNo);

      return "notice/noticeDetail";
   }

   @RequestMapping("deptDetail.not")
   public String showDeptDetailPage(String boardNo, Model model,String pageNo) {
      
      Board notice;
      ArrayList<Reply> reply;
      
      int currentPage;
      if (pageNo == null) {
         currentPage = 1;
      } else {
         currentPage = Integer.parseInt(pageNo);
      }

      int listCount = ns.getReplyListSize(boardNo);

      PageInfo pi = Pagination3.getPageInfo(currentPage, listCount);
      
      reply = ns.showReply(boardNo,pi);

      notice = ns.showCommonDetail(boardNo);
      
      ArrayList<Object> fileList = null;
        if(notice.getFileCode() != null) {
           fileList = ns.selectFileList(notice.getFileCode());
        }

      if (notice != null) {
         model.addAttribute("notice", notice);
         model.addAttribute("Typelist", "dept");
         model.addAttribute("fileList",fileList);
         model.addAttribute("reply",reply);
         model.addAttribute("pi", pi);
      } else {
         model.addAttribute("message", "현재 공지사항이 없습니다.");
      }
      
      int result2 = ns.increaseBoardCnt(boardNo);
      
      return "notice/departmentDetail";
   }

   @RequestMapping("commonDetail.not")
   public String showCommonDetailPage(String boardNo, Model model,String pageNo) {

      Board notice;
      ArrayList<Reply> reply;
      
      int currentPage;
      if (pageNo == null) {
         currentPage = 1;
      } else {
         currentPage = Integer.parseInt(pageNo);
      }

      int listCount = ns.getReplyListSize(boardNo);

      PageInfo pi = Pagination3.getPageInfo(currentPage, listCount);
      
      reply = ns.showReply(boardNo,pi);

      notice = ns.showCommonDetail(boardNo);
      
      ArrayList<Object> fileList = null;
        if(notice.getFileCode() != null) {
           fileList = ns.selectFileList(notice.getFileCode());
        }

      if (notice != null) {
         model.addAttribute("notice", notice);
         model.addAttribute("Typelist", "com");
         model.addAttribute("fileList",fileList);
         model.addAttribute("reply",reply);
         model.addAttribute("pi", pi);
      } else {
         model.addAttribute("message", "현재 공지사항이 없습니다.");
      }
      
      int result2 = ns.increaseBoardCnt(boardNo);

      return "notice/commonDetail";
   }

   @RequestMapping("commonWrite.not")
   public String showCommonWritePage(Model model) {

      model.addAttribute("Typelist", "com");
      
      return "notice/commonWrite";
   }
   
   @RequestMapping("deptWrite.not")
   public String showDeptWritePage(Model model) {

      model.addAttribute("Typelist", "dept");
      
      return "notice/departmentWrite";
   }
   
   @RequestMapping("allWrite.not")
   public String showNoticeWritePage(Model model) {

      model.addAttribute("Typelist", "not");
      
      return "notice/allWrite";
   }

   @RequestMapping("insertCommon.not")
   public String insertCommonPage(HttpServletRequest request, MultipartFile[] uploadfiles, Board board,Model model) {
      
      Member m = (Member) request.getSession().getAttribute("loginUser");
      
      System.out.println(m);
      
      int j = 0 ;
      
      
      
      String fileCode = "";
      
      int count = 0;
      
      if(uploadfiles == null) {
         board.setFileCode(fileCode);
      }else {
         
      

      for(MultipartFile uploadfile : uploadfiles) {
         
         if(count != 0) {
            fileCode += ",";
         }
      
         String root = request.getSession().getServletContext().getRealPath("resources");
   
         String filePath = root + "\\uploadFiles";
         
         String originFileName = uploadfile.getOriginalFilename();
         String ext = originFileName.substring(originFileName.lastIndexOf("."));
         String changeName = CommonUtils.getRandomString();

         fileCode += changeName;
         
         File saveFile = new File(filePath,changeName+ext);
         
         try {
            uploadfile.transferTo(saveFile);
            
            FileList upFile = new FileList();
            upFile.setFilePath("\\uploadFiles");
            upFile.setOriginName(originFileName);
            upFile.setChangeName(changeName + ext);
            upFile.setFileSize(String.valueOf(uploadfile.getSize()));
            
            int fileResult = fileService.addFile(upFile);
            if(j > 0) {
               board.setFileCode(board.getFileCode()+","+String.valueOf(fileResult));
            }
            if(j == 0) {
               board.setFileCode(String.valueOf(fileResult));
               j = 1;
            }
            /* asd */
            
            
            
         } catch (IllegalStateException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
         } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
         }
         try {
            uploadfile.transferTo(saveFile);
         } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
         count++;
      }
      }
      board.setEmpNo(m.getEmpNo());
      board.setDeptCode(m.getEmpDept());
      board.setEmpName(m.getEmpName());
      board.setBoardType("일반");
      
      System.out.println("board :::::"+board);
      
      int result = ns.insertCommon(board);
      
      
      String str = request.getParameter("ir1");

      System.out.println(str);
      
      System.out.println("파일 변경이름 : "+fileCode);

      return showCommonNoticeMainPage(model,"1");
   }
   
   
   @RequestMapping("insertDept.not")
   public String insertDeptPage(HttpServletRequest request, MultipartFile[] uploadfiles, Board board,Model model) {
      
      Member m = (Member) request.getSession().getAttribute("loginUser");
      
      String empDept = request.getParameter("empDept");
      
      System.out.println(m);
      
      int j = 0 ;
      
      
      
      String fileCode = "";
      
      int count = 0;
      
      if(uploadfiles == null) {
         board.setFileCode(fileCode);
      }else {
         
      

      for(MultipartFile uploadfile : uploadfiles) {
         
         if(count != 0) {
            fileCode += ",";
         }
      
         String root = request.getSession().getServletContext().getRealPath("resources");
   
         String filePath = root + "\\uploadFiles";
         
         String originFileName = uploadfile.getOriginalFilename();
         String ext = originFileName.substring(originFileName.lastIndexOf("."));
         String changeName = CommonUtils.getRandomString();

         fileCode += changeName;
         
         File saveFile = new File(filePath,changeName+ext);
         
         try {
            uploadfile.transferTo(saveFile);
            
            FileList upFile = new FileList();
            upFile.setFilePath("\\uploadFiles");
            upFile.setOriginName(originFileName);
            upFile.setChangeName(changeName + ext);
            upFile.setFileSize(String.valueOf(uploadfile.getSize()));
            
            int fileResult = fileService.addFile(upFile);
            if(j > 0) {
               board.setFileCode(board.getFileCode()+","+String.valueOf(fileResult));
            }
            if(j == 0) {
               board.setFileCode(String.valueOf(fileResult));
               j = 1;
            }
            
            
            
         } catch (IllegalStateException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
         } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
         }
         try {
            uploadfile.transferTo(saveFile);
         } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
         count++;
      }
      }
      board.setEmpNo(m.getEmpNo());
      board.setDeptCode(m.getEmpDept());
      board.setEmpName(m.getEmpName());
      board.setDeptCode(empDept);
      board.setBoardType("부서");
      
      System.out.println("board :::::"+board);
      
      int result = ns.insertDept(board);
      
      
      String str = request.getParameter("ir1");

      System.out.println(str);
      
      System.out.println("파일 변경이름 : "+fileCode);

      return showDeptNoticeMainPage(model,"1",empDept);
   }
   
   
   @RequestMapping("insertReply.not")
   public String insertReply(HttpServletRequest request,Reply reply,Model model) {
      
      Member m = (Member) request.getSession().getAttribute("loginUser");
      String empNo = m.getEmpNo();
      String boardNo = request.getParameter("boardNo");
      reply.setEmpName(m.getEmpName());
      
      System.out.println(empNo + "z" + boardNo);
      
      System.out.println(reply);
      
      int result = ns.insertReply(reply,empNo,boardNo);
      
      int result2 = ns.increaseReplyCnt(boardNo);
      
      return showCommonDetailPage(boardNo,model,"1");
   }
   @RequestMapping("deleteReply.not")
   public String deleteReply(HttpServletRequest request,Model model) {
      String boardNo = request.getParameter("boardNo");
      String replyNo = request.getParameter("replyNo");
      
      int result = ns.deleteReply(replyNo);
      
      int result2 = ns.decreaseReplyCnt(boardNo);
      
      return showCommonDetailPage(boardNo,model,"1");
   }
   
   @RequestMapping("insertDeptReply.not")
   public String insertDeptReply(HttpServletRequest request,Reply reply,Model model) {
      
      Member m = (Member) request.getSession().getAttribute("loginUser");
      String empNo = m.getEmpNo();
      String boardNo = request.getParameter("boardNo");
      reply.setEmpName(m.getEmpName());
      
      System.out.println(empNo + "z" + boardNo);
      
      System.out.println(reply);
      
      int result = ns.insertReply(reply,empNo,boardNo);
      
      int result2 = ns.increaseReplyCnt(boardNo);
      
      return showDeptDetailPage(boardNo,model,"1");
   }
   
   @RequestMapping("deleteDeptReply.not")
   public String deleteDeptReply(HttpServletRequest request,Model model) {
      String boardNo = request.getParameter("boardNo");
      String replyNo = request.getParameter("replyNo");
      
      int result = ns.deleteReply(replyNo);
      
      int result2 = ns.decreaseReplyCnt(boardNo);
      
      return showDeptDetailPage(boardNo,model,"1");
   }
   
   @RequestMapping("insertAll.not")
   public String insertNoticePage(HttpServletRequest request, MultipartFile[] uploadfiles, Board board,Model model) {
      
      Member m = (Member) request.getSession().getAttribute("loginUser");
      
      System.out.println(m);
      
      int j = 0 ;
      
      
      
      String fileCode = "";
      
      int count = 0;
      
      if(uploadfiles == null) {
         board.setFileCode(fileCode);
      }else {
         
      

      for(MultipartFile uploadfile : uploadfiles) {
         
         if(count != 0) {
            fileCode += ",";
         }
      
         String root = request.getSession().getServletContext().getRealPath("resources");
   
         String filePath = root + "\\uploadFiles";
         
         String originFileName = uploadfile.getOriginalFilename();
         String ext = originFileName.substring(originFileName.lastIndexOf("."));
         String changeName = CommonUtils.getRandomString();

         fileCode += changeName;
         
         File saveFile = new File(filePath,changeName+ext);
         
         try {
            uploadfile.transferTo(saveFile);
            
            FileList upFile = new FileList();
            upFile.setFilePath("\\uploadFiles");
            upFile.setOriginName(originFileName);
            upFile.setChangeName(changeName + ext);
            upFile.setFileSize(String.valueOf(uploadfile.getSize()));
            
            int fileResult = fileService.addFile(upFile);
            if(j > 0) {
               board.setFileCode(board.getFileCode()+","+String.valueOf(fileResult));
            }
            if(j == 0) {
               board.setFileCode(String.valueOf(fileResult));
               j = 1;
            }
            
            
            
         } catch (IllegalStateException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
         } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
         }
         try {
            uploadfile.transferTo(saveFile);
         } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
         count++;
      }
      }
      board.setEmpNo(m.getEmpNo());
      board.setDeptCode(m.getEmpDept());
      board.setEmpName(m.getEmpName());
      board.setBoardType("공지");
      
      System.out.println("board :::::"+board);
      
      int result = ns.insertCommon(board);
      
      
      String str = request.getParameter("ir1");

      System.out.println(str);
      
      System.out.println("파일 변경이름 : "+fileCode);

      return showNoticeMainPage(model,"1");
   }

}