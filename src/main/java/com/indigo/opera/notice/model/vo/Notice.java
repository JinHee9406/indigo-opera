package com.indigo.opera.notice.model.vo;

import java.sql.Date;

public class Notice implements java.io.Serializable{
	private String noticeNo;
	private String noticeTitle;
	private String noticeContent;
	private Date noticeDate;
	private String fileCode;
	private String workNo;
	
	public Notice() {}
	
	

	public Notice(String noticeNo, String noticeTitle, String noticeContent, Date noticeDate, String fileCode,
			String workNo) {
		super();
		this.noticeNo = noticeNo;
		this.noticeTitle = noticeTitle;
		this.noticeContent = noticeContent;
		this.noticeDate = noticeDate;
		this.fileCode = fileCode;
		this.workNo = workNo;
	}



	public String getNoticeNo() {
		return noticeNo;
	}

	public void setNoticeNo(String noticeNo) {
		this.noticeNo = noticeNo;
	}

	public String getNoticeTitle() {
		return noticeTitle;
	}

	public void setNoticeTitle(String noticeTitle) {
		this.noticeTitle = noticeTitle;
	}

	public String getNoticeContent() {
		return noticeContent;
	}

	public void setNoticeContent(String noticeContent) {
		this.noticeContent = noticeContent;
	}

	public Date getNoticeDate() {
		return noticeDate;
	}

	public void setNoticeDate(Date noticeDate) {
		this.noticeDate = noticeDate;
	}

	public String getFileCode() {
		return fileCode;
	}

	public void setFileCode(String fileCode) {
		this.fileCode = fileCode;
	}

	public String getWorkNo() {
		return workNo;
	}

	public void setWorkNo(String workNo) {
		this.workNo = workNo;
	}



	@Override
	public String toString() {
		return "Notice [noticeNo=" + noticeNo + ", noticeTitle=" + noticeTitle + ", noticeContent=" + noticeContent
				+ ", noticeDate=" + noticeDate + ", fileCode=" + fileCode + ", workNo=" + workNo + "]";
	}
	
	
	
}
