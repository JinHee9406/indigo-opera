package com.indigo.opera.notice.model.vo;

import java.sql.Date;

public class Reply implements java.io.Serializable{
	private String replyNo;
	private String empNo;
	private Date replyDate;
	private String replyDetail;
	private String boardNo;
	private String replyStatus;
	private String empName;
	
	public Reply() {}

	public Reply(String replyNo, String empNo, Date replyDate, String replyDetail, String boardNo, String replyStatus,
			String empName) {
		super();
		this.replyNo = replyNo;
		this.empNo = empNo;
		this.replyDate = replyDate;
		this.replyDetail = replyDetail;
		this.boardNo = boardNo;
		this.replyStatus = replyStatus;
		this.empName = empName;
	}

	public String getReplyNo() {
		return replyNo;
	}

	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public Date getReplyDate() {
		return replyDate;
	}

	public void setReplyDate(Date replyDate) {
		this.replyDate = replyDate;
	}

	public String getReplyDetail() {
		return replyDetail;
	}

	public void setReplyDetail(String replyDetail) {
		this.replyDetail = replyDetail;
	}

	public String getBoardNo() {
		return boardNo;
	}

	public void setBoardNo(String boardNo) {
		this.boardNo = boardNo;
	}

	public String getReplyStatus() {
		return replyStatus;
	}

	public void setReplyStatus(String replyStatus) {
		this.replyStatus = replyStatus;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	@Override
	public String toString() {
		return "Reply [replyNo=" + replyNo + ", empNo=" + empNo + ", replyDate=" + replyDate + ", replyDetail="
				+ replyDetail + ", boardNo=" + boardNo + ", replyStatus=" + replyStatus + ", empName=" + empName + "]";
	}

	
	
}
