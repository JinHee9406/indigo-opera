package com.indigo.opera.notice.model.dao;

import java.util.ArrayList;

import org.mybatis.spring.SqlSessionTemplate;

import com.indigo.opera.knowledge.model.vo.PageInfo;
import com.indigo.opera.notice.model.vo.Board;
import com.indigo.opera.notice.model.vo.Notice;
import com.indigo.opera.notice.model.vo.Reply;

public interface NoticeDao {

   int getListSize(SqlSessionTemplate sqlSession,String boardType);
//gkdk

   ArrayList<Board> showAllNoticeList(SqlSessionTemplate sqlSession, PageInfo pi);

   ArrayList<Board> showCommonNoticeList(SqlSessionTemplate sqlSession, PageInfo pi);

   ArrayList<Board> showDeptNoticeList(SqlSessionTemplate sqlSession, PageInfo pi, String loginUser);

   Board showCommonDetail(SqlSessionTemplate sqlSession, String boardNo);

   int insertCommon(SqlSessionTemplate sqlSession, Board boar);

   ArrayList<Object> selectFileList(SqlSessionTemplate sqlSession, String fileCode);

   int insertReply(SqlSessionTemplate sqlSession, Reply reply, String empNo, String boardNo);

   int getReplyListSize(SqlSessionTemplate sqlSession, String boardNo);

   ArrayList<Reply> showReplyList(SqlSessionTemplate sqlSession, String boardNo, PageInfo pi);

   int deleteReply(SqlSessionTemplate sqlSession, String replyNo);

   int insertDept(SqlSessionTemplate sqlSession, Board board);

   int increaseReplyCnt(SqlSessionTemplate sqlSession, String boardNo);

   int decreaseReplyCnt(SqlSessionTemplate sqlSession, String boardNo);

   int increaseBoardCnt(SqlSessionTemplate sqlSession, String boardNo);

   int getDeptListSize(SqlSessionTemplate sqlSession, String boardType, String deptCode);
}