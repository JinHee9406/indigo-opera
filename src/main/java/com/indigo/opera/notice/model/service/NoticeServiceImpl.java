package com.indigo.opera.notice.model.service;

import java.util.ArrayList;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indigo.opera.knowledge.model.vo.PageInfo;
import com.indigo.opera.notice.model.dao.NoticeDao;
import com.indigo.opera.notice.model.vo.Board;
import com.indigo.opera.notice.model.vo.Notice;
import com.indigo.opera.notice.model.vo.Reply;
@Service
public class NoticeServiceImpl implements NoticeService{
   
   @Autowired
   private SqlSessionTemplate sqlSession;
   
   @Autowired
   private NoticeDao noticeDao;

   @Override
   public int getListSize(String boardType) {
      
      
      return noticeDao.getListSize(sqlSession,boardType);
   }

   @Override
   public ArrayList<Board> showAllNoticeList(PageInfo pi) {
      
      return noticeDao.showAllNoticeList(sqlSession,pi);
   }

   @Override
   public ArrayList<Board> showCommonNoticeList(PageInfo pi) {
      
      return noticeDao.showCommonNoticeList(sqlSession,pi);
   }

   @Override
   public ArrayList<Board> showDeptNoticeList(PageInfo pi, String loginUser) {
      
      return noticeDao.showDeptNoticeList(sqlSession,pi,loginUser);
   }

   @Override
   public Board showCommonDetail(String boardNo) {
      
      return noticeDao.showCommonDetail(sqlSession,boardNo);
   }

   @Override
   public int insertCommon(Board boar) {
      return noticeDao.insertCommon(sqlSession,boar);
   }

   @Override
   public ArrayList<Object> selectFileList(String fileCode) {
      ArrayList<Object> fileList = noticeDao.selectFileList(sqlSession, fileCode);
         return fileList;
   }

   @Override
   public int insertReply(Reply reply, String empNo, String boardNo) {
      return noticeDao.insertReply(sqlSession,reply,empNo,boardNo);
   }

   @Override
   public int getReplyListSize(String boardNo) {
      return noticeDao.getReplyListSize(sqlSession, boardNo);
   }

   @Override
   public ArrayList<Reply> showReply(String boardNo, PageInfo pi) {
      return noticeDao.showReplyList(sqlSession,boardNo,pi);
   }

   @Override
   public int deleteReply(String replyNo) {
      return noticeDao.deleteReply(sqlSession,replyNo);
      
   }

   @Override
   public int insertDept(Board board) {
      return noticeDao.insertDept(sqlSession,board);
   }

   @Override
   public int increaseReplyCnt(String boardNo) {
      return noticeDao.increaseReplyCnt(sqlSession,boardNo);
   }

   @Override
   public int decreaseReplyCnt(String boardNo) {
      return noticeDao.decreaseReplyCnt(sqlSession,boardNo);
   }

   @Override
   public int increaseBoardCnt(String boardNo) {
      return noticeDao.increaseBoardCnt(sqlSession,boardNo);
   }

   @Override
   public int getDeptListSize(String boardType, String deptCode) {
      return noticeDao.getDeptListSize(sqlSession,boardType,deptCode);
   }
   
   //gd

}