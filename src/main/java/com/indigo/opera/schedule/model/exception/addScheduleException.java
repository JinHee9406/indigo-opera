package com.indigo.opera.schedule.model.exception;

public class addScheduleException extends Exception{
	
	public addScheduleException (String msg) {
		super(msg);
	}

}
