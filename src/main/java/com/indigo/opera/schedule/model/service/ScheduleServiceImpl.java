package com.indigo.opera.schedule.model.service;

import java.util.ArrayList;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indigo.opera.reservation.model.vo.MeetRoom;
import com.indigo.opera.schedule.model.dao.ScheduleDao;
import com.indigo.opera.schedule.model.vo.DeptSchedule;
import com.indigo.opera.schedule.model.vo.PerSchedule;

@Service
public class ScheduleServiceImpl implements ScheduleService{
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private ScheduleDao schDao;

	@Override
	public int addPerSchedule(PerSchedule psche) {
		
		//int result = schDao.addPerSchedule(sqlSession, psche);
		
		return 0;
	}

	@Override
	public int addDeptSchedul(DeptSchedule dsche) {

		//int result = schDao.addDeptSchedule(sqlSession, dsche);
		
		return 0;
	}

	

}
