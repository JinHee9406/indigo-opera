package com.indigo.opera.fileList.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.indigo.opera.fileList.model.service.FileListService;
import com.indigo.opera.fileList.model.vo.FileList;
@Controller
public class FileController {
	
	@Autowired
	private FileListService fileService;
	
	@RequestMapping("loadRoomFile.file")
	public ModelAndView imgRoomLoad(String roomNumber, ModelAndView mv) {
		
		FileList file = fileService.roomLoad(roomNumber);

		mv.addObject("fileList", file);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("loadUserFile.file")
	public ModelAndView imgUserLoad(String userEmpNo, ModelAndView mv) {
		
		FileList file = fileService.userLoad(userEmpNo);

		mv.addObject("fileList", file);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("fileDownload.file")
	  public void projectFileDownload(HttpServletRequest request, HttpServletResponse response) throws IOException {
	      
	      String fileNo = request.getParameter("fileNo");
	      
	      FileList file = fileService.selectOneFile(fileNo);
	      
	      String root = request.getSession().getServletContext().getRealPath("resources");
	      
	      //폴더에서 파일을 읽어들일 때 사용할 스트림 생성
	      BufferedInputStream buf = null;
	            
	      //클라이언트로 내보낼 출력스트림 생성
	      ServletOutputStream downOut = null;
	            
	      downOut = response.getOutputStream();
	            
	      //스트림으로 전송할 파일 객체 생성
	      File downFile = new File(root + "\\" + (String) file.getFilePath() + "\\" + file.getChangeName());
	            
	      response.setContentType("text/plane; charset=UTF-8");
	            
	      //한글파일명에 대한 인코딩 처리
	      //강제적으로 다운로드 처리(버튼 누르면 바로 다운로드 진행하는 기능, 크롬은 강제다운로드만 진행함)
	      response.setHeader("Content-Disposition", "attachment; filename=\"" + new String(((String) file.getOriginName()).getBytes("UTF-8"), "ISO-8859-1") + "\"");
	      //attachment : 강제다운로드방식
	            
	      response.setContentLength((int)downFile.length());
	            
	      FileInputStream fin = new FileInputStream(downFile);
	            
	      buf = new BufferedInputStream(fin);
	            
	      int readBytes = 0;
	            
	      while((readBytes = buf.read()) != -1) {
	         downOut.write(readBytes);
	      }
	         
	      downOut.close();
	      buf.close();
	   }
}
