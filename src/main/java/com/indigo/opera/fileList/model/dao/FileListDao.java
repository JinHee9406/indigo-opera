package com.indigo.opera.fileList.model.dao;

import org.mybatis.spring.SqlSessionTemplate;

import com.indigo.opera.fileList.model.vo.FileList;

public interface FileListDao {

	int addFile(SqlSessionTemplate sqlSession, FileList imgFile);

	FileList roomLoad(SqlSessionTemplate sqlSession, String roomNumber);

	FileList userLoad(SqlSessionTemplate sqlSession, String userEmpNo);

	FileList selectOneFile(SqlSessionTemplate sqlSession, String fileNo);

}
