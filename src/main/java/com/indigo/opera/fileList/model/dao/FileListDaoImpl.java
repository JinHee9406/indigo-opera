package com.indigo.opera.fileList.model.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.indigo.opera.fileList.model.vo.FileList;

@Repository
public class FileListDaoImpl implements FileListDao{

	@Override
	public int addFile(SqlSessionTemplate sqlSession, FileList imgFile) {
		
		int result = sqlSession.insert("FileList.addFile",imgFile);
		
		if(result > 0) {
			result = sqlSession.selectOne("FileList.currval");
		}
		
		return result;	
	}

	@Override
	public FileList roomLoad(SqlSessionTemplate sqlSession, String roomNumber) {
		
		FileList result = sqlSession.selectOne("FileList.roomLoad", roomNumber);
		
		return result;
	}

	@Override
	public FileList userLoad(SqlSessionTemplate sqlSession, String userEmpNo) {
		
		FileList result = sqlSession.selectOne("FileList.userLoad", userEmpNo);
		
		return result;
	}

	@Override
	public FileList selectOneFile(SqlSessionTemplate sqlSession, String fileNo) {
		
		FileList result = sqlSession.selectOne("FileList.oneFile", fileNo);
		
		return result;
	}

}
