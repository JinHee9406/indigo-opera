package com.indigo.opera.shareworks.model.vo;

import java.sql.Date;

public class WorkNotice{
	private String noticeNo;
	private String noticeTitle;
	private String noticeDetail;
	private String noticeDate;
	private String fileCode;
	private String fileName;
	private String workNo;
	private String noticeUser;
	private String noticeUserName;
	private String userJobName;
	private int noticeCnt;
	
	public WorkNotice() {}

	public WorkNotice(String noticeNo, String noticeTitle, String noticeDetail, String noticeDate, String fileCode,
			String workNo, String noticeUser, String noticeUserName, String userJobName, int noticeCnt) {
		super();
		this.noticeNo = noticeNo;
		this.noticeTitle = noticeTitle;
		this.noticeDetail = noticeDetail;
		this.noticeDate = noticeDate;
		this.fileCode = fileCode;
		this.workNo = workNo;
		this.noticeUser = noticeUser;
		this.noticeUserName = noticeUserName;
		this.userJobName = userJobName;
		this.noticeCnt = noticeCnt;
	}

	public String getNoticeNo() {
		return noticeNo;
	}

	public void setNoticeNo(String noticeNo) {
		this.noticeNo = noticeNo;
	}

	public String getNoticeTitle() {
		return noticeTitle;
	}

	public void setNoticeTitle(String noticeTitle) {
		this.noticeTitle = noticeTitle;
	}

	public String getNoticeDetail() {
		return noticeDetail;
	}

	public void setNoticeDetail(String noticeDetail) {
		this.noticeDetail = noticeDetail;
	}

	public String getNoticeDate() {
		return noticeDate;
	}

	public void setNoticeDate(String noticeDate) {
		this.noticeDate = noticeDate;
	}

	public String getFileCode() {
		return fileCode;
	}

	public void setFileCode(String fileCode) {
		this.fileCode = fileCode;
	}

	public String getWorkNo() {
		return workNo;
	}

	public void setWorkNo(String workNo) {
		this.workNo = workNo;
	}

	public String getNoticeUser() {
		return noticeUser;
	}

	public void setNoticeUser(String noticeUser) {
		this.noticeUser = noticeUser;
	}

	public String getNoticeUserName() {
		return noticeUserName;
	}

	public void setNoticeUserName(String noticeUserName) {
		this.noticeUserName = noticeUserName;
	}

	public String getUserJobName() {
		return userJobName;
	}

	public void setUserJobName(String userJobName) {
		this.userJobName = userJobName;
	}

	public int getNoticeCnt() {
		return noticeCnt;
	}

	public void setNoticeCnt(int noticeCnt) {
		this.noticeCnt = noticeCnt;
	}

	@Override
	public String toString() {
		return "WorkNotice [noticeNo=" + noticeNo + ", noticeTitle=" + noticeTitle + ", noticeDetail=" + noticeDetail
				+ ", noticeDate=" + noticeDate + ", fileCode=" + fileCode + ", workNo=" + workNo + ", noticeUser="
				+ noticeUser + ", noticeUserName=" + noticeUserName + ", userJobName=" + userJobName + ", noticeCnt="
				+ noticeCnt + "]";
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	
	
	
}
