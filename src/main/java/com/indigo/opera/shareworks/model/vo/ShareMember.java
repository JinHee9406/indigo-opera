package com.indigo.opera.shareworks.model.vo;

public class ShareMember {
	private String shareMemberCode;
	private String shareMemberNo;
	private String shareWorkNo;
	private String shareMemberName;
	private String shareMemberJob;
	
	public ShareMember() {}


	public ShareMember(String shareMemberCode, String shareMemberNo, String shareWorkNo, String shareMemberName,
			String shareMemberJob) {
		super();
		this.shareMemberCode = shareMemberCode;
		this.shareMemberNo = shareMemberNo;
		this.shareWorkNo = shareWorkNo;
		this.shareMemberName = shareMemberName;
		this.shareMemberJob = shareMemberJob;
	}



	public String getShareMemberName() {
		return shareMemberName;
	}


	public void setShareMemberName(String shareMemberName) {
		this.shareMemberName = shareMemberName;
	}


	public String getShareMemberJob() {
		return shareMemberJob;
	}


	public void setShareMemberJob(String shareMemberJob) {
		this.shareMemberJob = shareMemberJob;
	}


	public String getShareMemberCode() {
		return shareMemberCode;
	}

	public void setShareMemberCode(String shareMemberCode) {
		this.shareMemberCode = shareMemberCode;
	}

	public String getShareMemberNo() {
		return shareMemberNo;
	}

	public void setShareMemberNo(String shareMemberNo) {
		this.shareMemberNo = shareMemberNo;
	}

	public String getShareWorkNo() {
		return shareWorkNo;
	}

	public void setShareWorkNo(String shareWorkNo) {
		this.shareWorkNo = shareWorkNo;
	}


	@Override
	public String toString() {
		return "ShareMember [shareMemberCode=" + shareMemberCode + ", shareMemberNo=" + shareMemberNo + ", shareWorkNo="
				+ shareWorkNo + ", shareMemberName=" + shareMemberName + ", shareMemberJob=" + shareMemberJob + "]";
	}

	
	
}
