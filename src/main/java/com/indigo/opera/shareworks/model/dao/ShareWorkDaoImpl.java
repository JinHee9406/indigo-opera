package com.indigo.opera.shareworks.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.indigo.opera.department.model.vo.Department;
import com.indigo.opera.member.model.vo.Member;
import com.indigo.opera.shareworks.model.vo.ShareMember;
import com.indigo.opera.shareworks.model.vo.Work;
import com.indigo.opera.shareworks.model.vo.WorkNotice;
import com.indigo.opera.shareworks.model.vo.WorkReport;

@Repository
public class ShareWorkDaoImpl implements ShareWorkDao{

	@Override
	public ArrayList<Member> memberList(SqlSessionTemplate sqlSession, String deptCode) {
		
		ArrayList<Member> resultList = (ArrayList) sqlSession.selectList("Member.memberList", deptCode);
		
		return resultList;
	}
	
	@Override
	public ArrayList<Member> memberList(SqlSessionTemplate sqlSession) {
		
		ArrayList<Member> resultList = (ArrayList) sqlSession.selectList("Member.allMemberList");
		
		return resultList;
	}

	@Override
	public ArrayList<Department> deptList(SqlSessionTemplate sqlSession) {
		
		ArrayList<Department> resultList = (ArrayList) sqlSession.selectList("Department.deptList");
		
		return resultList;
	}

	@Override
	public int addShareWork(SqlSessionTemplate sqlSession, Work work) {
		
		int result = sqlSession.insert("Share.addShareWork", work);
		
		String usersCode[] = work.getWorkUser().split(",");
		
		int result2 = 0;
		
		for(int i = 0; i < usersCode.length; i++) {
			
			ShareMember members = new ShareMember();
			members.setShareMemberNo(usersCode[i]);
			
			result2 = sqlSession.insert("Share.addShareUsers", members);
			
		}
		
		return result;
	}

	@Override
	public ArrayList<Work> selectWorkList(SqlSessionTemplate sqlSession, String loginUser) {

		ArrayList<Work> resultList = (ArrayList) sqlSession.selectList("Share.selectListWorks", loginUser);
		
		return resultList;
	}
	

	@Override
	public ArrayList<Work> selectMyList(SqlSessionTemplate sqlSession, String loginUser) {
		
		ArrayList<Work> resultList = (ArrayList) sqlSession.selectList("Share.selectMyWorks", loginUser);
		
		return resultList;
	}


	@Override
	public ArrayList<Work> selectShareList(SqlSessionTemplate sqlSession, String loginUser) {
		
		ArrayList<Work> resultList = (ArrayList) sqlSession.selectList("Share.selectShareWorks", loginUser);
		
		return resultList;
	}

	@Override
	public Work selectWorkOne(SqlSessionTemplate sqlSession, String workNum) {
	
		Work result = sqlSession.selectOne("Share.selectOneWork", workNum);
		
		return result;
	}

	@Override
	public ArrayList<ShareMember> selectListMember(SqlSessionTemplate sqlSession, String workNum) {
		
		ArrayList<ShareMember> memberList = (ArrayList) sqlSession.selectList("Share.selectMemberList", workNum);
		
		return memberList;
	}

	@Override
	public Member loadJoindMember(SqlSessionTemplate sqlSession, String memberNo) {
		
		Member result = sqlSession.selectOne("Member.selectMemberByNo", memberNo);
		
		return result;
	}

	@Override
	public int addWorkNotice(SqlSessionTemplate sqlSession, WorkNotice workNotice) {
		
		int result = sqlSession.insert("Share.addWorkNotice", workNotice);
		
		return result;
	}

	@Override
	public ArrayList<WorkNotice> loadShareNotice(SqlSessionTemplate sqlSession, String workNo) {
		
		ArrayList<WorkNotice> resultList = (ArrayList) sqlSession.selectList("Share.loadNotice", workNo);
		
		return resultList;
	}

	@Override
	public WorkNotice noticeDetail(SqlSessionTemplate sqlSession, String noticeNo) {
		WorkNotice result = sqlSession.selectOne("Share.noticeDetail", noticeNo);
	
		return result;
	}

	@Override
	public int removeNotice(SqlSessionTemplate sqlSession, String noticeNum) {

		int result = sqlSession.delete("Share.removeNotice", noticeNum);
		
		return result;
	}

	@Override
	public int updateCnt(SqlSessionTemplate sqlSession, WorkNotice notice) {

		int result = sqlSession.update("Share.updateCnt", notice);
		
		return result;
	}

	@Override
	public int updateNotice(SqlSessionTemplate sqlSession, WorkNotice notice) {

		int result = sqlSession.update("Share.updateNotice", notice);
		
		return result;
	}

	@Override
	public int addWorkReport(SqlSessionTemplate sqlSession, WorkReport report) {

		int result = sqlSession.insert("Share.addWorkReport", report);
		
		return result;
	}

	@Override
	public ArrayList<WorkReport> workReportList(SqlSessionTemplate sqlSession,HashMap<String, String> param) {

		ArrayList<WorkReport> resultList = (ArrayList) sqlSession.selectList("Share.workReportList", param);
		
		return resultList;
	}

	@Override
	public int updateShareWork(SqlSessionTemplate sqlSession, Work work) {
		
		int result = sqlSession.update("Share.updateShareWork", work);
		
		String usersCode[] = work.getWorkUser().split(",");
		
		String workNo = work.getWorkNo();
		
		int resetUsers = sqlSession.delete("Share.resetShareUsers", workNo);
		
		int result2 = 0;
		
		for(int i = 0; i < usersCode.length; i++) {
			
			ShareMember members = new ShareMember();
			members.setShareWorkNo(workNo);
			members.setShareMemberNo(usersCode[i]);
			
			result2 = sqlSession.insert("Share.updateShareUsers", members);
			
		}
		
		return result;
	}



}
