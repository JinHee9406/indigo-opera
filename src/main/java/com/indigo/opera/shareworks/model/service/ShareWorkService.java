package com.indigo.opera.shareworks.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.indigo.opera.department.model.vo.Department;
import com.indigo.opera.member.model.vo.Member;
import com.indigo.opera.shareworks.model.vo.ShareMember;
import com.indigo.opera.shareworks.model.vo.Work;
import com.indigo.opera.shareworks.model.vo.WorkNotice;
import com.indigo.opera.shareworks.model.vo.WorkReport;

public interface ShareWorkService {

	ArrayList<Member> memberList(String deptCode);

	ArrayList<Member> memberList();
	
	ArrayList<Department> deptList();

	int addShareWork(Work work);

	ArrayList<Work> selectWorkList(String loginUser);
	
	ArrayList<Work> selectMyList(String loginUser);
	
	ArrayList<Work> selectShareList(String loginUser);

	Work selectWorkOne(String workNum);

	ArrayList<ShareMember> selectListMember(String workNum);

	Member loadJoindMember(String memberNo);

	int addWorkNotice(WorkNotice workNotice);

	ArrayList<WorkNotice> loadShareNotice(String workNo);

	WorkNotice noticeDetail(String noticeNo);

	int removeNotice(String noticeNum);

	int updateCnt(WorkNotice notice);

	int updateNotice(WorkNotice notice);

	int addWorkReport(WorkReport report);

	ArrayList<WorkReport> workReportList(HashMap<String, String> param);

	int updateShareWork(Work work);



}
