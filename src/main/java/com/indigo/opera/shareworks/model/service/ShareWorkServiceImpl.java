package com.indigo.opera.shareworks.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indigo.opera.department.model.vo.Department;
import com.indigo.opera.member.model.vo.Member;
import com.indigo.opera.shareworks.model.dao.ShareWorkDao;
import com.indigo.opera.shareworks.model.vo.ShareMember;
import com.indigo.opera.shareworks.model.vo.Work;
import com.indigo.opera.shareworks.model.vo.WorkNotice;
import com.indigo.opera.shareworks.model.vo.WorkReport;

@Service
public class ShareWorkServiceImpl implements ShareWorkService{
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private ShareWorkDao shareWorkDao;
	
	
	@Override
	public ArrayList<Member> memberList(String deptCode) {
		
		ArrayList<Member> resultList = shareWorkDao.memberList(sqlSession, deptCode);
		
		return resultList;
	}


	@Override
	public ArrayList<Member> memberList() {
		
		ArrayList<Member> resultList = shareWorkDao.memberList(sqlSession);
		
		return resultList;
	}
	
	@Override
	public ArrayList<Department> deptList() {
		
		ArrayList<Department> resultList = shareWorkDao.deptList(sqlSession);
		
		return resultList;
	}


	@Override
	public int addShareWork(Work work) {
		
		int result = shareWorkDao.addShareWork(sqlSession, work);
		
		return result;
	}


	@Override
	public ArrayList<Work> selectWorkList(String loginUser) {

		ArrayList<Work> resultList = shareWorkDao.selectWorkList(sqlSession, loginUser);
		
		return resultList;
	}


	@Override
	public ArrayList<Work> selectMyList(String loginUser) {
		ArrayList<Work> resultList = shareWorkDao.selectMyList(sqlSession, loginUser);
		
		return resultList;
	}
	

	@Override
	public ArrayList<Work> selectShareList(String loginUser) {
		
		ArrayList<Work> resultList = shareWorkDao.selectShareList(sqlSession, loginUser);
		
		return resultList;
	}




	@Override
	public Work selectWorkOne(String workNum) {
	
		Work result = shareWorkDao.selectWorkOne(sqlSession, workNum);
		
		return result;
	}


	@Override
	public ArrayList<ShareMember> selectListMember(String workNum) {
		ArrayList<ShareMember> memberList = shareWorkDao.selectListMember(sqlSession, workNum);
		return memberList;
	}


	@Override
	public Member loadJoindMember(String memberNo) {
		
		Member result = shareWorkDao.loadJoindMember(sqlSession, memberNo);
		
		return result;
	}


	@Override
	public int addWorkNotice(WorkNotice workNotice) {

		int result = shareWorkDao.addWorkNotice(sqlSession, workNotice);
		
		return result;
	}


	@Override
	public ArrayList<WorkNotice> loadShareNotice(String workNo) {
		ArrayList<WorkNotice> resultList = shareWorkDao.loadShareNotice(sqlSession, workNo);
		
		return resultList;
	}


	@Override
	public WorkNotice noticeDetail(String noticeNo) {
		
		WorkNotice result = shareWorkDao.noticeDetail(sqlSession, noticeNo);
		
		return result;
		
	}


	@Override
	public int removeNotice(String noticeNum) {
		int result = shareWorkDao.removeNotice(sqlSession, noticeNum);
		
		return result;
	}


	@Override
	public int updateCnt(WorkNotice notice) {

		int result = shareWorkDao.updateCnt(sqlSession, notice);
		
		return result;
	}


	@Override
	public int updateNotice(WorkNotice notice) {

		int result = shareWorkDao.updateNotice(sqlSession, notice);

		return result;
	}


	@Override
	public int addWorkReport(WorkReport report) {

		int result = shareWorkDao.addWorkReport(sqlSession, report);
		
		return result;
	}


	@Override
	public ArrayList<WorkReport> workReportList(HashMap<String, String> param) {
	
		ArrayList<WorkReport> resultList = shareWorkDao.workReportList(sqlSession, param);
		
		return resultList;
	}


	@Override
	public int updateShareWork(Work work) {
		
		int result = shareWorkDao.updateShareWork(sqlSession, work);
		
		return result;
	}




}
