package com.indigo.opera.shareworks.model.vo;

public class Work {
	
	private int postNum;
	private String workNo;
	private String empNo;
	private String workName;
	private String workSDate;
	private String workEDate;
	private int workProg;
	private String workDetail;
	private String workUser;
	private String empUserName;
	private String empUserJob;
	private int userNum;
	
	public Work() {}


	public Work(String workNo, String empNo, String workName, String workSDate, String workEDate, int workProg,
			String workDetail, String workUser, String empUserName, int userNum) {
		super();
		this.workNo = workNo;
		this.empNo = empNo;
		this.workName = workName;
		this.workSDate = workSDate;
		this.workEDate = workEDate;
		this.workProg = workProg;
		this.workDetail = workDetail;
		this.workUser = workUser;
		this.empUserName = empUserName;
		this.userNum = userNum;
	}


	@Override
	public String toString() {
		return "Work [workNo=" + workNo + ", empNo=" + empNo + ", workName=" + workName + ", workSDate=" + workSDate
				+ ", workEDate=" + workEDate + ", workProg=" + workProg + ", workDetail=" + workDetail + ", workUser="
				+ workUser + ", empUserName=" + empUserName + ", userNum=" + userNum + "]";
	}


	public String getWorkNo() {
		return workNo;
	}

	public void setWorkNo(String workNo) {
		this.workNo = workNo;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getWorkName() {
		return workName;
	}

	public void setWorkName(String workName) {
		this.workName = workName;
	}

	public String getWorkSDate() {
		return workSDate;
	}

	public void setWorkSDate(String workSDate) {
		this.workSDate = workSDate;
	}

	public String getWorkEDate() {
		return workEDate;
	}

	public void setWorkEDate(String workEDate) {
		this.workEDate = workEDate;
	}

	public int getWorkProg() {
		return workProg;
	}

	public void setWorkProg(int workProg) {
		this.workProg = workProg;
	}

	public String getWorkDetail() {
		return workDetail;
	}

	public void setWorkDetail(String workDetail) {
		this.workDetail = workDetail;
	}

	public String getWorkUser() {
		return workUser;
	}

	public void setWorkUser(String workUser) {
		this.workUser = workUser;
	}


	public String getEmpUserName() {
		return empUserName;
	}


	public void setEmpUserName(String empUserName) {
		this.empUserName = empUserName;
	}


	public int getUserNum() {
		return userNum;
	}


	public void setUserNum(int userNum) {
		this.userNum = userNum;
	}


	public int getPostNum() {
		return postNum;
	}


	public void setPostNum(int postNum) {
		this.postNum = postNum;
	}


	public String getEmpUserJob() {
		return empUserJob;
	}


	public void setEmpUserJob(String empUserJob) {
		this.empUserJob = empUserJob;
	}
	
}
