package com.indigo.opera.shareworks.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.indigo.opera.common.CommonUtils;
import com.indigo.opera.department.model.vo.Department;
import com.indigo.opera.fileList.model.service.FileListService;
import com.indigo.opera.fileList.model.vo.FileList;
import com.indigo.opera.member.model.vo.Member;
import com.indigo.opera.shareworks.model.service.ShareWorkService;
import com.indigo.opera.shareworks.model.vo.ShareMember;
import com.indigo.opera.shareworks.model.vo.Work;
import com.indigo.opera.shareworks.model.vo.WorkNotice;
import com.indigo.opera.shareworks.model.vo.WorkReport;

@Controller
public class ShareworksController {

	@Autowired
	private ShareWorkService shareWorkService;
	
	@Autowired
	private FileListService fileService;

	@RequestMapping("shareworksMain.sha")
	public String showShareWorkPage(Model model, String loginUser) {

		ArrayList<Work> resultList = shareWorkService.selectWorkList(loginUser);
		  
		for(int i = resultList.size()-1; i >= 0; i--) {
			resultList.get(i).setPostNum(i+1);
		}
		
		model.addAttribute("workList", resultList);

		return "shareworks/shareworksMain";
	}
	
	@RequestMapping("shareworksMine.sha")
	public String showMyWorkPage(Model model, String loginUser) {

		ArrayList<Work> resultList = shareWorkService.selectMyList(loginUser);
		  
		for(int i = resultList.size()-1; i >= 0; i--) {
			resultList.get(i).setPostNum(i+1);
		}
		
		model.addAttribute("workList", resultList);

		return "shareworks/shareworksMain";
	}
	
	@RequestMapping("shareworksShared.sha")
	public String showSharedWorkPage(Model model, String loginUser) {

		ArrayList<Work> resultList = shareWorkService.selectShareList(loginUser);
		  
		for(int i = resultList.size()-1; i >= 0; i--) {
			resultList.get(i).setPostNum(i+1);
		}
		
		model.addAttribute("workList", resultList);

		return "shareworks/shareworksMain";
	}
	

	@RequestMapping("shareworksDetail.sha")
	public String shareWorkDetailPage(Model model, String workNum) {
		Work result = shareWorkService.selectWorkOne(workNum);
		
		ArrayList<ShareMember> memberList= shareWorkService.selectListMember(workNum);
		
		String memberListStr = "";
		String memberNoListStr = "";
		
		for(int i = 0; i < memberList.size(); i++) {		
			
			if (i == memberList.size()-1) {
				memberListStr += (memberList.get(i).getShareMemberName() +  " " + memberList.get(i).getShareMemberJob());
				memberNoListStr += (memberList.get(i).getShareMemberNo());
			}
			else {
				memberListStr += (memberList.get(i).getShareMemberName() +  " " + memberList.get(i).getShareMemberJob()+",");
				memberNoListStr += (memberList.get(i).getShareMemberNo()+",");
			}
			
		}
		
		result.setWorkUser(memberListStr);

		model.addAttribute("workDetail", result);
		model.addAttribute("memberListNo", memberNoListStr);
		return "shareworks/shareworksDetail";
	}
	
	@RequestMapping("shareworksAdd.sha")
	public String shareWorkAddPage(Model model) {
		
		return "shareworks/shareworksAdd";
	}

	@RequestMapping("workShareStart.sha")
	public String workShare(Model model, Work work) {

		String shareMembers[] = work.getWorkUser().split(",");
		
		work.setUserNum(shareMembers.length);

		int result = shareWorkService.addShareWork(work);

		String message = "업무를 공유했습니다!";
		String key = "addShareWork";

		model.addAttribute("message", message);
		model.addAttribute("key", key);
		return "shareworks/shareResult";
	}

	
	@RequestMapping("joindMemberList.sha")
	public ModelAndView loadJoindMembers(ModelAndView mv, String memberList) {

		String memberNoList[] = memberList.split(",");
		
		ArrayList<Member> members = new ArrayList<Member>();
		
		for(int i = 0; i < memberNoList.length; i++) {
			
			Member result = shareWorkService.loadJoindMember(memberNoList[i]);
			
			members.add(result);
		}
		
		mv.addObject("memList", members);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("updateWork.sha")
	public String updateWork(Model model, Work work) {
		
		String shareMembers[] = work.getWorkUser().split(",");

		work.setUserNum(shareMembers.length);

		System.out.println(work);
		
		int result = shareWorkService.updateShareWork(work);
		
		String message = "업무를 수정했습니다.";
		String key = "updateWork";

		model.addAttribute("message", message);
		model.addAttribute("key", key);
		model.addAttribute("workNum", work.getWorkNo());
		return "shareworks/shareResult";
	}
	
	
	@RequestMapping("showWorkNotice.sha")
	public String showWorkNoticePage(Model model, String workNo, String workTitle) {
		
		model.addAttribute("getWorkNo", workNo);
		model.addAttribute("workName", workTitle);
		
		return "shareworks/shareNoticeAdd";
	}
	
	@RequestMapping("shareWorkNotice.sha")
	public String addWorkNotice(Model model, HttpServletRequest request, MultipartFile fileCode) {
		
		WorkNotice workNotice = new WorkNotice();
		
		String title = request.getParameter("noticeTitle");
		String detail = request.getParameter("noticeDetail");
		String workNo = request.getParameter("workNo");
		String user = request.getParameter("noticeUser");
		
		workNotice.setNoticeTitle(title);
		workNotice.setNoticeDetail(detail);
		workNotice.setWorkNo(workNo);
		workNotice.setNoticeUser(user);
		
		String root = request.getSession().getServletContext().getRealPath("resources");
		String filePath = root + "\\uploadFiles";
		
		String originFileName = fileCode.getOriginalFilename();
		String ext = originFileName.substring(originFileName.lastIndexOf("."));
		String changeName = CommonUtils.getRandomString();
		
		try {
			File file = new File(filePath + "\\" + changeName + ext);
			
			fileCode.transferTo(file);
			
			FileList upFile = new FileList();
			upFile.setFilePath("\\uploadFiles");
			upFile.setOriginName(originFileName);
			upFile.setChangeName(changeName + ext);
			upFile.setFileSize(String.valueOf(fileCode.getSize()));
			
			int fileResult = fileService.addFile(upFile);
			workNotice.setFileCode(String.valueOf(fileResult));
			
			int result= shareWorkService.addWorkNotice(workNotice);
			
		} catch (Exception e) {
			new File(filePath + "\\" + changeName + ext).delete();
			
			String message = "파일 추가 오류";
			System.out.println(message);
			model.addAttribute("message", message);
			return "main/mainPage";
			
		}
		
		String message = "업무공지사항 작성을 완료했습니다.";
		String key = "addShareNotice";
	
		model.addAttribute("message", message);
		model.addAttribute("key", key);
		model.addAttribute("workNum", workNotice.getWorkNo());
		return "shareworks/shareResult";
	}
	
	@RequestMapping("loadShareNotice.sha")
	public ModelAndView loadShareNotice(ModelAndView mv, String workNo) {
		
		ArrayList<WorkNotice> notices = shareWorkService.loadShareNotice(workNo);
		
		mv.addObject("noticeList", notices);
		mv.setViewName("jsonView");
		return mv;
	}

	
	@RequestMapping("deptList.sha")
	public ModelAndView showDeptList(ModelAndView mv) {

		ArrayList<Department> deptList = shareWorkService.deptList();

		mv.addObject("deptList", deptList);
		mv.setViewName("jsonView");
		return mv;
	}

	@RequestMapping("memberList.sha")
	public ModelAndView showMemberList(ModelAndView mv, String deptCode) {

		ArrayList<Member> memberList = null;
		if (deptCode.equals("all")) {
			memberList = shareWorkService.memberList();
		} else {
			memberList = shareWorkService.memberList(deptCode);
		}

		mv.addObject("memberList", memberList);
		mv.setViewName("jsonView");
		return mv;

	}
	
	@RequestMapping("workShareDetail.sha")
	public String showNoticeDetail(Model model, String noticeNo) {
		
		WorkNotice notice = shareWorkService.noticeDetail(noticeNo);
		
		notice.setNoticeCnt(notice.getNoticeCnt() + 1);
		
		int updateCnt = shareWorkService.updateCnt(notice);
		
		model.addAttribute("noticeDetail", notice);
		return "shareworks/shareNoticeDetail";
	}
	
	@RequestMapping("removeNotice.sha")
	public String removeNotice(Model model, String noticeNum, String workNum) {
		
		int result = shareWorkService.removeNotice(noticeNum);
		
		String message = "공지사항을 삭제했습니다";
		String key = "removeNotice";

		model.addAttribute("message", message);
		model.addAttribute("key", key);
		model.addAttribute("workNo", workNum);
		return "shareworks/shareResult";
	}
	
	@RequestMapping("noticeFix.sha")
	public String showFixNotice(Model model, WorkNotice notice) {
		
		model.addAttribute("shareNotice", notice);
		return "shareworks/shareNoticeFix";
	}
	
	@RequestMapping("updateNotice.sha")
	public String updateNotice(Model model, WorkNotice notice) {
		
		int result = shareWorkService.updateNotice(notice);
		
		model.addAttribute("noticeDetail", notice);
		return "shareworks/shareNoticeDetail";
	}
	
	
	
	@RequestMapping("addWorkReport.sha")
	public String addWorkReport(Model model, HttpServletRequest request, MultipartFile reportFile) {
		
		WorkReport report = new WorkReport();
		
		String workNo = request.getParameter("workNo");
		String empNo = request.getParameter("empNo");
		String reportType = request.getParameter("reportType");
		String reportDetail = request.getParameter("reportDetail");
		
		report.setWorkNo(workNo);
		report.setEmpNo(empNo);
		report.setReportType(reportType);
		report.setReportDetail(reportDetail);
		
		String root = request.getSession().getServletContext().getRealPath("resources");
		String filePath = root + "\\uploadFiles";
		
		String originFileName = reportFile.getOriginalFilename();
		String ext = originFileName.substring(originFileName.lastIndexOf("."));
		String changeName = CommonUtils.getRandomString();
		
		try {
			File file = new File(filePath + "\\" + changeName + ext);
			
			reportFile.transferTo(file);
			
			FileList upFile = new FileList();
			upFile.setFilePath("\\uploadFiles");
			upFile.setOriginName(originFileName);
			upFile.setChangeName(changeName + ext);
			upFile.setFileSize(String.valueOf(reportFile.getSize()));
			
			int fileResult = fileService.addFile(upFile);
			report.setReportFile(String.valueOf(fileResult));
			
			int result = shareWorkService.addWorkReport(report);
			
		} catch (Exception e) {
			new File(filePath + "\\" + changeName + ext).delete();
			
			String message = "파일 추가 오류";
			System.out.println(message);
			model.addAttribute("workNo", report.getWorkNo());
			model.addAttribute("message", message);
			return "shareworks/shareResult";
		}
		
		String message = "업무보고를 작성했습니다!";
		String key = "addShareWorkReport";

		model.addAttribute("workNo", report.getWorkNo());
		model.addAttribute("message", message);
		model.addAttribute("key", key);
		return "shareworks/shareResult";
	}
	
	
	
	
	@RequestMapping("loadShareReport.sha")
	public ModelAndView shareReportList(ModelAndView mv, String workNo, String date) {
		
		HashMap<String, String> param = new HashMap<String, String>();
		param.put("workNo", workNo);
		param.put("date", date);
		
		ArrayList<WorkReport> resultList = shareWorkService.workReportList(param);
		
		mv.addObject("reportList", resultList);
		mv.setViewName("jsonView");
		return mv;
	}
}
