package com.indigo.opera.department.model.vo;

public class Department {
	private String deptCode;
	private String deptName;
	private String deptType;
	
	public Department() {}

	public Department(String deptCode, String deptName, String deptType) {
		super();
		this.deptCode = deptCode;
		this.deptName = deptName;
		this.deptType = deptType;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDeptType() {
		return deptType;
	}

	public void setDeptType(String deptType) {
		this.deptType = deptType;
	}

	@Override
	public String toString() {
		return "Department [deptCode=" + deptCode + ", deptName=" + deptName + ", deptType=" + deptType + "]";
	}
	
	
}
