package com.indigo.opera.attendance.model.dao;

import java.util.ArrayList;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.indigo.opera.attendance.model.vo.Attendance;
import com.indigo.opera.attendance.model.vo.DeptAttend;

@Repository
public class AttendDaoImpl implements AttendDao {

	@Override
	public int insertAttend(SqlSessionTemplate sqlSession, String empNo) {

		return sqlSession.insert("Attendance.insertAttend", empNo);
	}

	@Override
	public int updateAtten(SqlSessionTemplate sqlSession, String empNo) {
		return sqlSession.update("Attendance.updateAttend", empNo);
	}

	@Override
	public ArrayList<Attendance> showAttendList(SqlSessionTemplate sqlSession, Attendance att) {

		ArrayList<Attendance> list = (ArrayList) sqlSession.selectList("Attendance.showAttendList", att);

		return list;
	}

	@Override
	public ArrayList<DeptAttend> showDeptEmp(SqlSessionTemplate sqlSession, String deptCode) {
		// TODO Auto-generated method stub
		return (ArrayList) sqlSession.selectList("Attendance.showDeptAttendList", deptCode);
	}

	@Override
	public int showAttCount(SqlSessionTemplate sqlSession, String date, DeptAttend d) {

		d.setDate(date);

		return sqlSession.selectOne("Attendance.showAttCount", d);
	}

	@Override
	public int showLateCount(SqlSessionTemplate sqlSession, String date, DeptAttend d) {
		d.setDate(date);

		return sqlSession.selectOne("Attendance.showLateCount", d);
	}

	@Override
	public int showAbsCount(SqlSessionTemplate sqlSession, String date, DeptAttend d) {
		d.setDate(date);

		return sqlSession.selectOne("Attendance.showAbsCount", d);
	}

	@Override
	public ArrayList<Object> selectAttendanceList(SqlSessionTemplate sqlSession, String empNo) {
		ArrayList<Object> alist = (ArrayList<Object>)sqlSession.selectList("Attendance.selectAttendanceList", empNo);
		System.out.println("alist : " + alist);
		return alist;
	}

}
