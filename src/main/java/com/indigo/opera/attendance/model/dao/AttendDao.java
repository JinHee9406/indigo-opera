package com.indigo.opera.attendance.model.dao;

import java.util.ArrayList;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.indigo.opera.attendance.model.vo.Attendance;
import com.indigo.opera.attendance.model.vo.DeptAttend;


public interface AttendDao {

	int insertAttend(SqlSessionTemplate sqlSession, String empNo);

	int updateAtten(SqlSessionTemplate sqlSession, String empNo);

	ArrayList<Attendance> showAttendList(SqlSessionTemplate sqlSession, Attendance att);

	ArrayList<DeptAttend> showDeptEmp(SqlSessionTemplate sqlSession, String deptCode);

	int showAttCount(SqlSessionTemplate sqlSession, String date, DeptAttend d);

	int showLateCount(SqlSessionTemplate sqlSession, String date, DeptAttend d);

	int showAbsCount(SqlSessionTemplate sqlSession, String date, DeptAttend d);

	ArrayList<Object> selectAttendanceList(SqlSessionTemplate sqlSession, String empNo);

	



}
