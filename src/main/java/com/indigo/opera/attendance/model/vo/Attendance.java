package com.indigo.opera.attendance.model.vo;

import java.sql.Timestamp;

public class Attendance {
	private String attendNo;
	private String empNo;
	private Timestamp workTime;
	private Timestamp homeTime;
	private String attendStatus;
	private String empName;
	
	public Attendance() {
		// TODO Auto-generated constructor stub
	}

	public Attendance(String attendNo, String empNo, Timestamp workTime, Timestamp homeTime, String attendStatus,
			String empName) {
		super();
		this.attendNo = attendNo;
		this.empNo = empNo;
		this.workTime = workTime;
		this.homeTime = homeTime;
		this.attendStatus = attendStatus;
		this.empName = empName;
	}

	public String getAttendNo() {
		return attendNo;
	}

	public void setAttendNo(String attendNo) {
		this.attendNo = attendNo;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public Timestamp getWorkTime() {
		return workTime;
	}

	public void setWorkTime(Timestamp workTime) {
		this.workTime = workTime;
	}

	public Timestamp getHomeTime() {
		return homeTime;
	}

	public void setHomeTime(Timestamp homeTime) {
		this.homeTime = homeTime;
	}

	public String getAttendStatus() {
		return attendStatus;
	}

	public void setAttendStatus(String attendStatus) {
		this.attendStatus = attendStatus;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	@Override
	public String toString() {
		return "Attendance [attendNo=" + attendNo + ", empNo=" + empNo + ", workTime=" + workTime + ", homeTime="
				+ homeTime + ", attendStatus=" + attendStatus + ", empName=" + empName + "]";
	}
	
}
