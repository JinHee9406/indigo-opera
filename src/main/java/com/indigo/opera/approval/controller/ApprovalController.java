package com.indigo.opera.approval.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.indigo.opera.approval.model.service.ApprovalService;
import com.indigo.opera.approval.model.vo.CashDisbursement;
import com.indigo.opera.approval.model.vo.CheckClient;
import com.indigo.opera.approval.model.vo.CheckDeptPi;
import com.indigo.opera.approval.model.vo.Client;
import com.indigo.opera.approval.model.vo.DraftList;
import com.indigo.opera.approval.model.vo.PayLine;
import com.indigo.opera.approval.model.vo.PayLineList;
import com.indigo.opera.approval.model.vo.PayLog;
import com.indigo.opera.approval.model.vo.PayOpinion;
import com.indigo.opera.approval.model.vo.ProVision;
import com.indigo.opera.common.CommonUtils;
import com.indigo.opera.common.Pagination;
import com.indigo.opera.common.Pagination2;
import com.indigo.opera.fileList.model.service.FileListService;
import com.indigo.opera.fileList.model.vo.FileList;
import com.indigo.opera.knowledge.model.exception.addKnowledgeException;
import com.indigo.opera.knowledge.model.vo.PageInfo;
import com.indigo.opera.member.model.vo.Member;

@Controller
public class ApprovalController {
	
	@Autowired
	private ApprovalService approvalService;
	
	@Autowired
	private FileListService fileService;
	
	@RequestMapping("approvalInProgress.app")
	public String approvalInProgress(HttpServletRequest request) {
		int pageNum = 1;
		if(request.getParameter("pageNum") != null) {
			pageNum = Integer.parseInt(request.getParameter("pageNum"));
		}
		String status = "전체";
		if(request.getParameter("status") != null) {
			status = request.getParameter("status");
			System.out.println("requestStatus : " + status);
		} 
		String status2 = "";
		if(request.getParameter("status2") != null) {
			status2 = request.getParameter("status2");
			System.out.println("requestStatus2 : " + status2);
		}
		Member m = (Member)request.getSession().getAttribute("loginUser");
		m.setEmpAff(status2);
		int result = approvalService.selectCountApprovalList(m, status);
		PageInfo pi = Pagination.getPageInfo(pageNum, result);
		ArrayList<Object> approvalList = approvalService.selectApprovalList(m, status, pageNum);

		request.setAttribute("approvalList", approvalList);
		System.out.println("approvalList : " + approvalList);
		int ing = approvalService.selectCountApprovalList(m, "진행");
		int check = approvalService.selectCountApprovalList(m, "요청");
		request.getSession().setAttribute("ing", ing);
		request.getSession().setAttribute("check", check);
		request.setAttribute("pi", pi);
		request.setAttribute("status", status);
		return "approval/approvalInProgress";
	}
	
	@RequestMapping("selectDocuList.app")
	public String selectDocuList() {
		return "approval/approvalDocuList";
	}
	
	@RequestMapping("selectApprovalDocu.app")
	public String selectApprovalDocu(HttpServletRequest request) {
		ArrayList<Object> deptList = approvalService.selectDeptList();
		ArrayList<Object> empList = approvalService.selectEmpList();
		request.getSession().setAttribute("deptList", deptList);
		request.getSession().setAttribute("empList", empList);
		Member m = (Member)request.getSession().getAttribute("loginUser");
		String empDept = m.getEmpDept();
		
		String empDeptName = approvalService.selectDeptName(empDept);
		request.getSession().setAttribute("empDeptName", empDeptName);
		return "approval/doculist/cashDisbursement";
	}
	
	@RequestMapping("insertCashDis.app")
	public String insertCashDis(HttpServletRequest request, CashDisbursement cdm, List<MultipartFile> fileCode) {
		System.out.println("fileCode : " + fileCode);
		int j = 0 ;
		for(MultipartFile file : fileCode) {
			
			String root = request.getSession().getServletContext().getRealPath("resources");
			String filePath = root + "\\uploadFiles";
			String originFileName = file.getOriginalFilename();
			String ext = originFileName.substring(originFileName.lastIndexOf("."));
			String changeName = CommonUtils.getRandomString();
			
			try {
				File files = new File(filePath + "\\" + changeName + ext);
				
				file.transferTo(files);
				
				FileList upFile = new FileList();
				upFile.setFilePath("\\uploadFiles");
				upFile.setOriginName(originFileName);
				upFile.setChangeName(changeName + ext);
				upFile.setFileSize(String.valueOf(file.getSize()));
				
				int fileResult = fileService.addFile(upFile);
				if(j > 0) {
					cdm.setFilecode(cdm.getFilecode()+","+String.valueOf(fileResult));
				}
				if(j == 0) {
					cdm.setFilecode(String.valueOf(fileResult));
					j = 1;
				}
				
			} catch (Exception e) {
				new File(filePath + "\\" + changeName + ext).delete();
				
				String message = "파일 추가 오류";
				System.out.println(message);
				return "main/mainPage";
				
			}
		}
		System.out.println("cdm getFileCode : " + cdm.getFilecode());
			
		System.out.println("cdm : " + cdm);
		int insetCashResult = approvalService.insertCashDis(cdm);
		String faketotals = request.getParameter("totals");
		Member m = (Member)request.getSession().getAttribute("loginUser");
		if(insetCashResult > 0) {
			String[] totals = faketotals.split(",");
			
			ArrayList<PayLog> paylogList = new ArrayList<PayLog>();
			PayLog my = new PayLog();
			my.setLoSort("기안");
			my.setEmpName(m.getEmpName());
			paylogList.add(my);
			for(int i = 0; i < totals.length; i++) {
				PayLog p = new PayLog();
				if(request.getParameter("approvers").contains(totals[i].substring(0, 3))) {
					p.setLoSort("결재");
				} else {
					p.setLoSort("협조");
				}
				p.setEmpName(totals[i].substring(0, 3));
				paylogList.add(p);
			}
			
			int insertPayLogResult = approvalService.insertPayLog(paylogList);
			
			return approvalInProgress(request);
		} else {
			return "";
		}
	}
	
	@RequestMapping("selectApprovalOne.app")
	public String selectApprovalOne(String oneDocu, HttpServletRequest request) {
		String docu = oneDocu;
		if(oneDocu == null) {
			docu = (String)request.getSession().getAttribute("oneDocu");
		}
		System.out.println(oneDocu + "   :   " + docu );
		request.getSession().setAttribute("oneDocu", docu);
		if(docu != null) {
			CashDisbursement cdm = approvalService.selectApprovalOne(docu);
			request.getSession().setAttribute("cdm", cdm);
			
			ArrayList<Object> plist = approvalService.selectApprovalOnePayLogList(docu);
			System.out.println("before plist : " + plist ) ;

			request.getSession().setAttribute("plist", plist);
			ArrayList<Object> oplist = approvalService.selectPayOpinionList(docu);
			request.getSession().setAttribute("payoplist", oplist);
			System.out.println("oplist : " + oplist);
			int ing = approvalService.selectCountApprovalList((Member)request.getSession().getAttribute("loginUser"), "진행");
			int check = approvalService.selectCountApprovalList((Member)request.getSession().getAttribute("loginUser"), "요청");
			System.out.println("CDM : " + cdm);
			ArrayList<Object> fileList = null;
			if(cdm.getFilecode() != null) {
				fileList = approvalService.selectFileList(cdm.getFilecode());
			}
			if(fileList != null) {
				request.getSession().setAttribute("fileList", fileList);
			} else {
				request.getSession().setAttribute("fileList", null);
			}
			DraftList dr = approvalService.selectDraft(docu);
			request.getSession().setAttribute("dr", dr);
			request.getSession().setAttribute("ing", ing);
			request.getSession().setAttribute("check", check);
		}
		
		return "approval/checkCashDisbursement";
	}
	
	@RequestMapping("updatePayLogOne.app")
	public String updatePayLogOne(PayLog p, HttpServletRequest request) {
		System.out.println("payLog : " + p);
		int result = approvalService.updatePayLogOne(p);
		return selectApprovalOne(p.getDrNo(), request);
	}
	
	@RequestMapping("insertApprovalReply.app")
	public String insertApprovalReply(PayOpinion p, HttpServletRequest request) {
		int result = approvalService.insertOpinion(p);
		request.getSession().setAttribute("oneDocu", p.getDrNo());
		return "redirect:/selectApprovalOne.app?oneDocu=" + p.getDrNo();
	}
	@RequestMapping("deletePayOpinion.app")
	public String deletePayOpinion(PayOpinion p, HttpServletRequest request) {
		int result = approvalService.deletePayOpinion(p);
		return "redirect:/selectApprovalOne.app?oneDocu=" + p.getDrNo();
	}
	
	@RequestMapping("updateApproval.app")
	public String updateApproval(CashDisbursement cdm) {
		int result = approvalService.updateApproval(cdm);
		return "redirect:/selectApprovalOne.app?oneDocu=" + cdm.getdNo();
	}
	
	
	@ResponseBody
	@RequestMapping("changeEmpList.app")
	public ArrayList<Object> changeEmpList(String dept, String pageNum, String empName) {
		CheckDeptPi cp = new CheckDeptPi(dept, Integer.parseInt(pageNum), empName);
		System.out.println("EMPNAME : " + cp.getEmpName());
		int result = approvalService.selectCountEmpList(cp);
		
		PageInfo pi = Pagination2.getPageInfo(Integer.parseInt(pageNum), result);
		
		ArrayList<Object> deptList = approvalService.selectDeptList();
		ArrayList<Object> empList = approvalService.selectEmpList(pi, cp);
		empList.add(pi);

		return empList;
	}
	@ResponseBody
	@RequestMapping("addPersonalApprovalList.app")
	public void addPersonalApprovalList(String title, String loSort, String empName, String empJob, HttpServletRequest request) {
		Member m = (Member)request.getSession().getAttribute("loginUser");
		PayLine p = new PayLine("", title, m.getEmpNo());
		
		int result = approvalService.insertPayLine(p);
		
		ArrayList<PayLineList> plList = new ArrayList<PayLineList>();
		
		String[] realLoSort = loSort.split(",");
		String[] realEmpName = empName.split(",");
		String[] realEmpJob = empJob.split(",");
		
		for(int i = 0; i < realLoSort.length; i++) {
			PayLineList pl = new PayLineList();
			pl.setLoSort(realLoSort[i]);
			pl.setEmpName(realEmpName[i]);
			pl.setEmpJob(realEmpJob[i]);
			
			plList.add(pl);
		}
		int result2 = approvalService.insertPayLineList(plList);
		
	}
	@ResponseBody
	@RequestMapping("selectPersonalApprovalList.app")
	public ArrayList<Object> selectPersonalApprovalList(String empNo){
		ArrayList<Object> paList = approvalService.selectPersonalApprovalList(empNo);
		return paList;
	}
	@ResponseBody
	@RequestMapping("selectPersonalApprovalOne.app")
	public ArrayList<Object> selectPersonalApprovalOne(String plNo){
		System.out.println("PLNO : " + plNo);
		ArrayList<Object> pllList = approvalService.selectPersonalApprovalOne(plNo);
		
		return pllList; 
	}
	@ResponseBody
	@RequestMapping("addProvision.app")
	public void addProvision(ProVision pv) {
		System.out.println("PV : " + pv);
		int result = approvalService.insertProVision(pv);
	}
	@ResponseBody
	@RequestMapping("selectProVisionList.app")
	public ArrayList<Object> selectProVisionList(ProVision pv){
		
		ArrayList<Object> pvlist = approvalService.selectProvisionList(pv);
		return pvlist;
	}
	
	@ResponseBody
	@RequestMapping("insertClient.app")
	public void insertClient(Client cl) {
		int result = approvalService.insertClient(cl);
	}
	
	@ResponseBody
	@RequestMapping("selectClientList.app")
	public ArrayList<Object> selectClientList(CheckClient ck){
		System.out.println("CK : " + ck);
		int result = approvalService.selectCountClientList(ck);
		System.out.println("RESULT CONTROLLER : " + result);
		PageInfo pi = Pagination2.getPageInfo(Integer.parseInt(ck.getPageNum()), result);	
		ArrayList<Object> clList = approvalService.selectClientList(ck, pi);
		clList.add(pi);
		System.out.println("CLLIST : " + clList);
		return clList;
	}
		
	@ResponseBody
	@RequestMapping("selectClient.app")
	public Client selectClient(String cliNo) {
		
		Client cl = approvalService.selectClientOne(cliNo);
		System.out.println("CL : " + cl);
		return cl;
		
	}
}
