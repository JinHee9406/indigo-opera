package com.indigo.opera.approval.model.vo;

public class PayLine implements java.io.Serializable{
	private String plNo;
	private String plName;
	private String empNo;
	private String empName;
	
	public PayLine() {}
	
	public PayLine(String plNo, String plName, String empNo) {
		super();
		this.plNo = plNo;
		this.plName = plName;
		this.empNo = empNo;
	}
	public PayLine(String plNo, String plName, String empNo, String empName) {
		super();
		this.plNo = plNo;
		this.plName = plName;
		this.empNo = empNo;
		this.empName = empName;
	}

	public String getPlNo() {
		return plNo;
	}

	public void setPlNo(String plNo) {
		this.plNo = plNo;
	}

	public String getPlName() {
		return plName;
	}

	public void setPlName(String plName) {
		this.plName = plName;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	@Override
	public String toString() {
		return "PayLine [plNo=" + plNo + ", plName=" + plName + ", empNo=" + empNo + ", empName=" + empName + "]";
	}
	
	
}
