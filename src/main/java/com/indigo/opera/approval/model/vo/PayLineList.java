package com.indigo.opera.approval.model.vo;

public class PayLineList implements java.io.Serializable{
	private String pllNo;
	private String plNo;
	private String empName;
	private String empJob;
	private String empDeptName;
	private String loSort;
	
	public PayLineList() {}

	public PayLineList(String pllNo, String plNo, String empName, String empJob, String empDeptName, String loSort) {
		super();
		this.pllNo = pllNo;
		this.plNo = plNo;
		this.empName = empName;
		this.empJob = empJob;
		this.empDeptName = empDeptName;
		this.loSort = loSort;
	}

	public String getPllNo() {
		return pllNo;
	}

	public void setPllNo(String pllNo) {
		this.pllNo = pllNo;
	}

	public String getPlNo() {
		return plNo;
	}

	public void setPlNo(String plNo) {
		this.plNo = plNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpJob() {
		return empJob;
	}

	public void setEmpJob(String empJob) {
		this.empJob = empJob;
	}

	public String getEmpDeptName() {
		return empDeptName;
	}

	public void setEmpDeptName(String empDeptName) {
		this.empDeptName = empDeptName;
	}

	public String getLoSort() {
		return loSort;
	}

	public void setLoSort(String loSort) {
		this.loSort = loSort;
	}

	@Override
	public String toString() {
		return "PayLineList [pllNo=" + pllNo + ", plNo=" + plNo + ", empName=" + empName + ", empJob=" + empJob
				+ ", empDeptName=" + empDeptName + ", loSort=" + loSort + "]";
	}
	
	
}
