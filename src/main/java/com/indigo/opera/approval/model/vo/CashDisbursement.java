package com.indigo.opera.approval.model.vo;

import java.sql.Timestamp;


public class CashDisbursement implements java.io.Serializable{
	private String dNo;
	private String approvers;
	private String cooperators;
	private String receiver;
	private String expendform;
	private Timestamp date;
	private String empNo;
	private String title;
	private String paytype;
	private String paynumber;
	private String paydate;
	private String paycontent;
	private String payprice;
	private String paycompany;
	private String paybank;
	private String banknumber;
	private String bankholder;
	private String remark;
	private String share;
	private String deposit;
	private String attach;
	private String summary;
	private String details;
	private String filecode;
	
	public CashDisbursement() {}

	public CashDisbursement(String dNo, String approvers, String cooperators, String receiver, String expendform,
			Timestamp date, String empNo, String title, String paytype, String paynumber, String paydate,
			String paycontent, String payprice, String paycompany, String paybank, String banknumber, String bankholder,
			String paydept, String remark, String share, String deposit, String attach, String summary, String details,
			String filecode) {
		super();
		this.dNo = dNo;
		this.approvers = approvers;
		this.cooperators = cooperators;
		this.receiver = receiver;
		this.expendform = expendform;
		this.date = date;
		this.empNo = empNo;
		this.title = title;
		this.paytype = paytype;
		this.paynumber = paynumber;
		this.paydate = paydate;
		this.paycontent = paycontent;
		this.payprice = payprice;
		this.paycompany = paycompany;
		this.paybank = paybank;
		this.banknumber = banknumber;
		this.bankholder = bankholder;
		this.remark = remark;
		this.share = share;
		this.deposit = deposit;
		this.attach = attach;
		this.summary = summary;
		this.details = details;
		this.filecode = filecode;
	}

	public String getdNo() {
		return dNo;
	}

	public void setdNo(String dNo) {
		this.dNo = dNo;
	}

	public String getApprovers() {
		return approvers;
	}

	public void setApprovers(String approvers) {
		this.approvers = approvers;
	}

	public String getCooperators() {
		return cooperators;
	}

	public void setCooperators(String cooperators) {
		this.cooperators = cooperators;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getExpendform() {
		return expendform;
	}

	public void setExpendform(String expendform) {
		this.expendform = expendform;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPaytype() {
		return paytype;
	}

	public void setPaytype(String paytype) {
		this.paytype = paytype;
	}

	public String getPaynumber() {
		return paynumber;
	}

	public void setPaynumber(String paynumber) {
		this.paynumber = paynumber;
	}

	public String getPaydate() {
		return paydate;
	}

	public void setPaydate(String paydate) {
		this.paydate = paydate;
	}

	public String getPaycontent() {
		return paycontent;
	}

	public void setPaycontent(String paycontent) {
		this.paycontent = paycontent;
	}

	public String getPayprice() {
		return payprice;
	}

	public void setPayprice(String payprice) {
		this.payprice = payprice;
	}

	public String getPaycompany() {
		return paycompany;
	}

	public void setPaycompany(String paycompany) {
		this.paycompany = paycompany;
	}

	public String getPaybank() {
		return paybank;
	}

	public void setPaybank(String paybank) {
		this.paybank = paybank;
	}

	public String getBanknumber() {
		return banknumber;
	}

	public void setBanknumber(String banknumber) {
		this.banknumber = banknumber;
	}

	public String getBankholder() {
		return bankholder;
	}

	public void setBankholder(String bankholder) {
		this.bankholder = bankholder;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getShare() {
		return share;
	}

	public void setShare(String share) {
		this.share = share;
	}

	public String getDeposit() {
		return deposit;
	}

	public void setDeposit(String deposit) {
		this.deposit = deposit;
	}

	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getFilecode() {
		return filecode;
	}

	public void setFilecode(String filecode) {
		this.filecode = filecode;
	}

	@Override
	public String toString() {
		return "CashDisbursement [dNo=" + dNo + ", approvers=" + approvers + ", cooperators=" + cooperators
				+ ", receiver=" + receiver + ", expendform=" + expendform + ", date=" + date + ", empNo=" + empNo
				+ ", title=" + title + ", paytype=" + paytype + ", paynumber=" + paynumber + ", paydate=" + paydate
				+ ", paycontent=" + paycontent + ", payprice=" + payprice + ", paycompany=" + paycompany + ", paybank="
				+ paybank + ", banknumber=" + banknumber + ", bankholder=" + bankholder 
				+ ", remark=" + remark + ", share=" + share + ", deposit=" + deposit + ", attach=" + attach
				+ ", summary=" + summary + ", details=" + details + ", filecode=" + filecode + "]";
	}
	
	
	
}
