package com.indigo.opera.approval.model.vo;

public class CheckDeptPi implements java.io.Serializable{
	private String dept;
	private int pageNum;
	private String empName;
	
	public CheckDeptPi() {}

	public CheckDeptPi(String dept, int pageNum, String empName) {
		super();
		this.dept = dept;
		this.pageNum = pageNum;
		this.empName = empName;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	@Override
	public String toString() {
		return "CheckDeptPi [dept=" + dept + ", pageNum=" + pageNum + ", empName=" + empName + "]";
	}
	
}
