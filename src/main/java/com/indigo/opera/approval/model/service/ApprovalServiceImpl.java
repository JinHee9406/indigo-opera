package com.indigo.opera.approval.model.service;

import java.util.ArrayList;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indigo.opera.approval.model.dao.ApprovalDao;
import com.indigo.opera.approval.model.vo.CashDisbursement;
import com.indigo.opera.approval.model.vo.CheckClient;
import com.indigo.opera.approval.model.vo.CheckDeptPi;
import com.indigo.opera.approval.model.vo.Client;
import com.indigo.opera.approval.model.vo.DraftList;
import com.indigo.opera.approval.model.vo.PayLine;
import com.indigo.opera.approval.model.vo.PayLineList;
import com.indigo.opera.approval.model.vo.PayLog;
import com.indigo.opera.approval.model.vo.PayOpinion;
import com.indigo.opera.approval.model.vo.ProVision;
import com.indigo.opera.knowledge.model.vo.PageInfo;
import com.indigo.opera.member.model.vo.Member;

@Service
public class ApprovalServiceImpl implements ApprovalService{
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private ApprovalDao approvalDao;
	@Override
	public int insertCashDis(CashDisbursement cdm) {
		int result = approvalDao.insertCashDis(sqlSession, cdm);
		return result;
	}
	@Override
	public int insertPayLog(ArrayList<PayLog> paylogList) {
		int result = approvalDao.insertPayLog(sqlSession, paylogList);
		return result;
	}
	@Override
	public ArrayList<Object> selectApprovalList(Member m, String status, int pageNum) {
		ArrayList<Object> approvalList = approvalDao.selectApprovalList(sqlSession, m, status, pageNum);
		return approvalList;
	}
	@Override
	public CashDisbursement selectApprovalOne(String oneDocu) {
		CashDisbursement cdm = approvalDao.selectApprovalOne(oneDocu, sqlSession);
		return cdm;
	}
	@Override
	public ArrayList<Object> selectApprovalOnePayLogList(String oneDocu) {
		ArrayList<Object> plist = approvalDao.selectApprovalOnePayLogList(sqlSession, oneDocu);
		return plist;
	}
	@Override
	public int updatePayLogOne(PayLog p) {
		int result = approvalDao.updatePayLogOne(sqlSession, p);
		return result;
	}
	
	@Override
	public int selectCountApprovalList(Member m, String status) {
		int result = approvalDao.selectCountApprovalList(sqlSession, m, status);
		return result;
	}
	@Override
	public int insertOpinion(PayOpinion p) {
		int result = approvalDao.insertOpinion(sqlSession, p);
		return result;
	}
	@Override
	public ArrayList<Object> selectPayOpinionList(String oneDocu) {
		ArrayList<Object> oplist = approvalDao.selectPayOpinionList(sqlSession, oneDocu);
		return oplist;
	}
	@Override
	public int deletePayOpinion(PayOpinion p) {
		int result = approvalDao.deletePayOpinion(sqlSession, p);
		return result;
	}
	@Override
	public int updateApproval(CashDisbursement cdm) {
		int result = approvalDao.updateApproval(sqlSession, cdm);
		
		return result;
	}
	@Override
	public DraftList selectDraft(String docu) {
		DraftList dr = approvalDao.selectDraft(sqlSession, docu);
		return dr;
	}
	@Override
	public ArrayList<Object> selectDeptList() {
		ArrayList<Object> deptList = approvalDao.selectDeptList(sqlSession);
		return deptList;
	}
	@Override
	public ArrayList<Object> selectEmpList() {
		ArrayList<Object> empList = approvalDao.selectEmpList(sqlSession);
		return empList;
	}
	@Override
	public int selectCountEmpList(CheckDeptPi cp) {
		int result = approvalDao.selectCountEmpList(sqlSession, cp);
		return result;
	}
	@Override
	public ArrayList<Object> selectEmpList(PageInfo pi, CheckDeptPi cp) {
		ArrayList<Object> empList = approvalDao.selectEmpList(sqlSession, pi, cp);
		return empList;
	}
	@Override
	public int insertPayLine(PayLine p) {
		int result = approvalDao.insertPayLine(sqlSession, p);
		return result;
	}
	@Override
	public int insertPayLineList(ArrayList<PayLineList> plList) {
		int result = approvalDao.insertPayLineList(sqlSession, plList);
		return result;
	}
	@Override
	public ArrayList<Object> selectPersonalApprovalList(String empNo) {
		ArrayList<Object> paList = approvalDao.selectPersonalApprovalList(sqlSession, empNo);
		return paList;
	}
	@Override
	public ArrayList<Object> selectPersonalApprovalOne(String plNo) {
		ArrayList<Object> pllList = approvalDao.selectPersonalApprovalOne(sqlSession, plNo);
		return pllList;
	}
	@Override
	public int insertProVision(ProVision pv) {
		int result = approvalDao.insertProVision(sqlSession, pv);
		return result;
	}
	@Override
	public ArrayList<Object> selectProvisionList(ProVision pv) {
		ArrayList<Object> pvlist = approvalDao.selectProvisionList(sqlSession, pv);
		return pvlist;
	}
	@Override
	public int insertClient(Client cl) {
		int result = approvalDao.insertClient(sqlSession, cl);
		return result;
	}
	@Override
	public ArrayList<Object> selectClientList(CheckClient ck, PageInfo pi) {
		ArrayList<Object> plList = approvalDao.selectClientList(sqlSession, ck, pi);
		return plList;
	}
	@Override
	public Client selectClientOne(String cliNo) {
		Client cl = approvalDao.selectClientOne(sqlSession, cliNo);
		return cl;
	}
	@Override
	public ArrayList<Object> selectFileList(String filecode) {
		ArrayList<Object> fileList = approvalDao.selectFileList(sqlSession, filecode);
		return fileList;
	}
	@Override
	public String selectDeptName(String empDept) {
		String empDeptName = approvalDao.selectDeptName(sqlSession, empDept);
		return empDeptName;
	}
	@Override
	public int selectCountClientList(CheckClient ck) {
		int result = approvalDao.selectCountClientList(sqlSession, ck);
		return result;
	}
	
}
