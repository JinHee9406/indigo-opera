package com.indigo.opera.approval.model.vo;

public class CheckClient {
	private String cliNo;
	private String empNo;
	private String pageNum;
	private String search;
	private String searchType;
	
	public CheckClient() {}

	public CheckClient(String cliNo, String empNo, String pageNum, String search, String searchType) {
		super();
		this.cliNo = cliNo;
		this.empNo = empNo;
		this.pageNum = pageNum;
		this.search = search;
		this.searchType = searchType;
	}

	public String getCliNo() {
		return cliNo;
	}

	public void setCliNo(String cliNo) {
		this.cliNo = cliNo;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getPageNum() {
		return pageNum;
	}

	public void setPageNum(String pageNum) {
		this.pageNum = pageNum;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	@Override
	public String toString() {
		return "CheckClient [cliNo=" + cliNo + ", empNo=" + empNo + ", pageNum=" + pageNum + ", search=" + search
				+ ", searchType=" + searchType + "]";
	}
	
}
