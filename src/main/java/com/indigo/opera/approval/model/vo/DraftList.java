package com.indigo.opera.approval.model.vo;

import java.sql.Timestamp;

public class DraftList {
	private String drNo;
	private String drStatus;
	private String doName;
	private String drTitle;
	private Timestamp drDate;
	private String empName;
	private String myStatus;
	
	public DraftList() {}

	public DraftList(String drNo, String drStatus, String doName, String drTitle, Timestamp drDate, String empName,
			String myStatus) {
		super();
		this.drNo = drNo;
		this.drStatus = drStatus;
		this.doName = doName;
		this.drTitle = drTitle;
		this.drDate = drDate;
		this.empName = empName;
		this.myStatus = myStatus;
	}

	public String getDrNo() {
		return drNo;
	}

	public void setDrNo(String drNo) {
		this.drNo = drNo;
	}

	public String getDrStatus() {
		return drStatus;
	}

	public void setDrStatus(String drStatus) {
		this.drStatus = drStatus;
	}

	public String getDoName() {
		return doName;
	}

	public void setDoName(String doName) {
		this.doName = doName;
	}

	public String getDrTitle() {
		return drTitle;
	}

	public void setDrTitle(String drTitle) {
		this.drTitle = drTitle;
	}

	public Timestamp getDrDate() {
		return drDate;
	}

	public void setDrDate(Timestamp drDate) {
		this.drDate = drDate;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getMyStatus() {
		return myStatus;
	}

	public void setMyStatus(String myStatus) {
		this.myStatus = myStatus;
	}

	@Override
	public String toString() {
		return "DraftList [drNo=" + drNo + ", drStatus=" + drStatus + ", doName=" + doName + ", drTitle=" + drTitle
				+ ", drDate=" + drDate + ", empName=" + empName + ", myStatus=" + myStatus + "]";
	}
	

	
	
}
