package com.indigo.opera.approval.model.dao;

import java.util.ArrayList;

import org.mybatis.spring.SqlSessionTemplate;

import com.indigo.opera.approval.model.vo.CashDisbursement;
import com.indigo.opera.approval.model.vo.CheckClient;
import com.indigo.opera.approval.model.vo.CheckDeptPi;
import com.indigo.opera.approval.model.vo.Client;
import com.indigo.opera.approval.model.vo.DraftList;
import com.indigo.opera.approval.model.vo.PayLine;
import com.indigo.opera.approval.model.vo.PayLineList;
import com.indigo.opera.approval.model.vo.PayLog;
import com.indigo.opera.approval.model.vo.PayOpinion;
import com.indigo.opera.approval.model.vo.ProVision;
import com.indigo.opera.knowledge.model.vo.PageInfo;
import com.indigo.opera.member.model.vo.Member;

public interface ApprovalDao {

	int insertCashDis(SqlSessionTemplate sqlSession, CashDisbursement cdm);

	int insertPayLog(SqlSessionTemplate sqlSession, ArrayList<PayLog> paylogList);

	ArrayList<Object> selectApprovalList(SqlSessionTemplate sqlSession, Member m, String status, int pageNum);

	CashDisbursement selectApprovalOne(String oneDocu, SqlSessionTemplate sqlSession);

	ArrayList<Object> selectApprovalOnePayLogList(SqlSessionTemplate sqlSession, String oneDocu);

	int updatePayLogOne(SqlSessionTemplate sqlSession, PayLog p);

	int selectCountApprovalList(SqlSessionTemplate sqlSession, Member m, String status);

	int insertOpinion(SqlSessionTemplate sqlSession, PayOpinion p);

	ArrayList<Object> selectPayOpinionList(SqlSessionTemplate sqlSession, String oneDocu);

	int deletePayOpinion(SqlSessionTemplate sqlSession, PayOpinion p);

	int updateApproval(SqlSessionTemplate sqlSession, CashDisbursement cdm);

	DraftList selectDraft(SqlSessionTemplate sqlSession, String docu);

	ArrayList<Object> selectDeptList(SqlSessionTemplate sqlSession);

	ArrayList<Object> selectEmpList(SqlSessionTemplate sqlSession);

	int selectCountEmpList(SqlSessionTemplate sqlSession, CheckDeptPi cp);


	ArrayList<Object> selectEmpList(SqlSessionTemplate sqlSession, PageInfo pi, CheckDeptPi cp);

	int insertPayLine(SqlSessionTemplate sqlSession, PayLine p);

	int insertPayLineList(SqlSessionTemplate sqlSession, ArrayList<PayLineList> plList);

	ArrayList<Object> selectPersonalApprovalList(SqlSessionTemplate sqlSession, String empNo);

	ArrayList<Object> selectPersonalApprovalOne(SqlSessionTemplate sqlSession, String plNo);

	int insertProVision(SqlSessionTemplate sqlSession, ProVision pv);

	ArrayList<Object> selectProvisionList(SqlSessionTemplate sqlSession, ProVision pv);

	int insertClient(SqlSessionTemplate sqlSession, Client cl);

	ArrayList<Object> selectClientList(SqlSessionTemplate sqlSession, CheckClient ck, PageInfo pi);

	Client selectClientOne(SqlSessionTemplate sqlSession, String cliNo);

	ArrayList<Object> selectFileList(SqlSessionTemplate sqlSession, String filecode);

	String selectDeptName(SqlSessionTemplate sqlSession, String empDept);

	int selectCountClientList(SqlSessionTemplate sqlSession, CheckClient ck);



}
