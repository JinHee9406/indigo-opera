package com.indigo.opera.reservation.model.service;

import java.sql.Date;
import java.util.ArrayList;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indigo.opera.knowledge.model.vo.PageInfo;
import com.indigo.opera.member.model.vo.Member;
import com.indigo.opera.reservation.model.dao.ReservationDao;
import com.indigo.opera.reservation.model.exception.reservationException;
import com.indigo.opera.reservation.model.vo.MeetRoom;
import com.indigo.opera.reservation.model.vo.Reservation;

@Service
public class ReservationServiceImpl implements ReservationService{
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private ReservationDao resDao;
	
	@Override
	public int addMeetRoom(MeetRoom room) throws reservationException {
		
		int result = resDao.addMeetRoom(sqlSession, room);
		
		return result;
	}
	
	@Override
	public ArrayList<Reservation> userListReservation(Reservation load) {
		ArrayList<Reservation> result = resDao.userListReservation(sqlSession, load);
		
		return result;
	}

	@Override
	public ArrayList<MeetRoom> showRoomList() {
		
		ArrayList<MeetRoom> resultList = resDao.showRoomList(sqlSession);
		
		return resultList;
	}

	@Override
	public MeetRoom roomInfoLoad(String roomNumber) {
		
		MeetRoom roomInfo = resDao.roomInfoLoad(sqlSession, roomNumber);
		
		return roomInfo;
	}

	@Override
	public int putReservation(Reservation reservation) {
		
		int reservResult = resDao.putReservation(sqlSession, reservation);
		
		return reservResult;
	}

	@Override
	public ArrayList<Reservation> listReservation(String day) {
	
		ArrayList<Reservation> result = resDao.listReservation(sqlSession, day);
		
		return result;
	}

	@Override
	public ArrayList<Reservation> reslistPage(String loginUser, PageInfo pi) {
		ArrayList<Reservation> result = resDao.reslistPage(sqlSession, loginUser, pi);
		return result;
	}

	@Override
	public int getListSize(String loginUser) {
		
		int result = resDao.getListSize(sqlSession, loginUser);
		
		return result;
	}

	@Override
	public Reservation reservationDetail(String resNo) {
		Reservation result = resDao.reservationDetail(sqlSession, resNo);
		
		return result;
	}

	@Override
	public int removeReservation(String resNo) {
		int result = resDao.removeReservation(sqlSession, resNo);
		
		return result;
	}

	@Override
	public Member getUserData(String loginUser) {

		Member result = resDao.getUserData(sqlSession,loginUser);
		
		return result;
	}



}
