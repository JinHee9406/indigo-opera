package com.indigo.opera.reservation.model.dao;

import java.sql.Date;
import java.util.ArrayList;

import org.mybatis.spring.SqlSessionTemplate;

import com.indigo.opera.knowledge.model.vo.PageInfo;
import com.indigo.opera.member.model.vo.Member;
import com.indigo.opera.reservation.model.exception.reservationException;
import com.indigo.opera.reservation.model.vo.MeetRoom;
import com.indigo.opera.reservation.model.vo.Reservation;

public interface ReservationDao {

	int addMeetRoom(SqlSessionTemplate sqlSession, MeetRoom room) throws reservationException;

	ArrayList<MeetRoom> showRoomList(SqlSessionTemplate sqlSession);

	MeetRoom roomInfoLoad(SqlSessionTemplate sqlSession, String roomNumber);

	int putReservation(SqlSessionTemplate sqlSession, Reservation reservation);

	ArrayList<Reservation> listReservation(SqlSessionTemplate sqlSession, String day);

	ArrayList<Reservation> userListReservation(SqlSessionTemplate sqlSession, Reservation load);

	ArrayList<Reservation> reslistPage(SqlSessionTemplate sqlSession, String loginUser, PageInfo pi);

	int getListSize(SqlSessionTemplate sqlSession, String loginUser);

	Reservation reservationDetail(SqlSessionTemplate sqlSession, String resNo);

	int removeReservation(SqlSessionTemplate sqlSession, String resNo);

	Member getUserData(SqlSessionTemplate sqlSession, String loginUser);

}
