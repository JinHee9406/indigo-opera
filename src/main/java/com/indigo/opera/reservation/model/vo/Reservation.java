package com.indigo.opera.reservation.model.vo;

import java.sql.Date;

public class Reservation {
	private int listNum;
	private String resNo;
	private String empNo;
	private String empName;
	private String startDate;
	private String endDate;
	private String resDetail;
	private int personNum;
	private String roomNo;
	private String roomName;
	private String resDay;
	
	public Reservation() {}



	public Reservation(String resNo, String empNo, String empName, String startDate, String endDate, String resDetail,
			int personNum, String roomNo, String resDay) {
		super();
		this.resNo = resNo;
		this.empNo = empNo;
		this.empName = empName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.resDetail = resDetail;
		this.personNum = personNum;
		this.roomNo = roomNo;
		this.resDay = resDay;
	}



	@Override
	public String toString() {
		return "Reservation [resNo=" + resNo + ", empNo=" + empNo + ", empName=" + empName + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", resDetail=" + resDetail + ", personNum=" + personNum + ", roomNo="
				+ roomNo + ", resDay=" + resDay + "]";
	}



	public String getResNo() {
		return resNo;
	}

	public void setResNo(String resNo) {
		this.resNo = resNo;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}


	public String getResDetail() {
		return resDetail;
	}

	public void setResDetail(String resDetail) {
		this.resDetail = resDetail;
	}

	public int getPersonNum() {
		return personNum;
	}

	public void setPersonNum(int personNum) {
		this.personNum = personNum;
	}

	public String getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}
	

	public String getStartDate() {
		return startDate;
	}


	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}


	public String getEndDate() {
		return endDate;
	}


	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	public String getResDay() {
		return resDay;
	}

	public void setResDay(String resDay) {
		this.resDay = resDay;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}



	public int getListNum() {
		return listNum;
	}



	public void setListNum(int listNum) {
		this.listNum = listNum;
	}



	public String getRoomName() {
		return roomName;
	}



	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	
}
